<?php
/*
* Fichier de chargement des paramètres  
* Fichier crée le 04/12/2017
* Auteur : Denis Sanson alias Multixvers
*/
date_default_timezone_set('Europe/Paris');
setlocale (LC_TIME, 'fr_FR.utf8','fra');
define('AN',date('Y'));
define('MOIS',date('m'));

if(DEBUG == true){
    error_reporting(E_ALL);
    ini_set('display_errors',1);
    ini_set('ignore_repeated_errors ',1);
} else {
    error_reporting(E_ALL);
    ini_set('display_errors', '0');
}

define('TEMP_PRIV',SRC."temp/");
define('LOGS',SRC."logs/");

set_include_path(get_include_path() . PATH_SEPARATOR . TEMP_PRIV);
set_include_path(get_include_path() . PATH_SEPARATOR . LOGS);