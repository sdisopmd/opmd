<?php
header('Content-type: application/json');

// verif user et droit retour ( nom du groupe, nom et prenom utilisateur, et si autorisé ou non )
if( isset($_POST['auth']) && $_POST['auth'] == true){

	$tab = array("rep" => "no");
	$user = User::isLogin();
	$name = $user->getNomFormat();
	$id = $user->getGroupe();
	$groups = new Group($id);
	$groupe = $groups->getNom();
	
	if( $id < 3 && $id > 0 ){
		$tab = array("rep" => "ok", "name" => $name, "groupe" => $groupe);
	}
	
	echo json_encode($tab);

}

// exportation du fichier sql ( date heure nom )
if( isset($_POST['export']) && $_POST['export'] == true ){

	$tab = array("rep" => "no");
	$user = User::isLogin();
	$name = $user->getNomDB();
	$jour = date("d-m-Y");
	$heure = date("H\hi");
	$date = $jour."_".$heure;
	$dir = BACKUP.$name;
	$fichier = "opmd_".$date.".sql";

	if(!is_dir($dir)) {
		mkdir($dir);
	}

	$save = $dir."/".$fichier;

	$cmd = "mysqldump --host=".HOST." --user=".USER." --password=".PASS." ".BASE." > ".$save;
	system($cmd);
	
	$tab = array("rep" => "ok", "name" => $name, "jour" => $jour, "heure" => $heure, "fichier" => $fichier, "dossier" => $save );
	
	echo json_encode($tab);

}

// déplacement du fichier en vue du téléchargement ( date heure nom )
if( isset($_POST['move']) && $_POST['move'] == true && !empty($_POST['file']) ){

	$tab = array("rep" => "no");
	$user = User::isLogin();
	$name = $user->getNomDB();
	$dir = BACKUP.$name;

	$dos = WEB."/".ADMIN_BAKCUP.$name;
	$fichier = $_POST['file'];

	if(!is_dir($dos)) {
		mkdir($dos);
	}

	$save = $dir."/".$fichier;
	$copy = $dos."/".$fichier;

	copy($save,$copy);

	$upload = URL.ADMIN_BAKCUP.$name."/".$fichier;

	$tab = array("rep" => "ok", "url" => $upload );
	
	echo json_encode($tab);
	
}

// suppression du dossier temporaire
if( isset($_POST['delete']) && $_POST['delete'] == true && !empty($_POST['file']) ){

	$tab = array("rep" => "no");
	$user = User::isLogin();
	$name = $user->getNomDB();
	$fichier = $_POST['file'];
	$dos = WEB."/".ADMIN_BAKCUP.$name;

	unlink($dos."/".$fichier);
	rmdir($dos);
	$url = "http://".$_SERVER['SERVER_NAME']."/";

	$tab = array("rep" => "ok", "url" => $url );
	
	echo json_encode($tab);

}


?>

