<?php

/*
* Fonction utilitaires de gestion des formats de date
* Fichier crée le 06/04/2014
* Auteur : Denis Sanson alias Multixvers
*/

/*******************************
* D : date
* H : heure
* 2 : version courte
* 3 : version courte et fausse
*******************************/
function dateD($date) {
	return $date!=null ? date('d/m/Y', $date) : '';
}

function dateD2($date) {
	return $date!=null ? date('d/m', $date) : '';
}

function dateH($date) {
	return $date!=null ? date('Hi', $date) : '';
}
function dateH2($date) {
	return $date!=null ? date('H:i', $date) : '';
}

function dateDH($date) {
	return $date!=null ? date('d/m/Y Hi', $date) : '';
}

function dateD2H3($date) {
	return $date!=null ? date('d/m Hi', $date) : '';
}

function dateDH2($date) {
	return $date!=null ? date('d/m/Y H:i', $date) : '';
}

function dateD2H2($date) {
	return $date!=null ? date('d/m Hi', $date) : '';
}

function dateDH3($date) {
	return $date!=null ? date('Y-m-d H:i', $date) : '';
}

?>