<?php

/*
* Fonction des constantes globales des chemins des dossiers
* Fichier crée le 06/04/2014
* Auteur : Denis Sanson alias Multixvers
*/

/* VARIABLE GLOBALE DES CHEMINS DES DOSSIERS */

// niveau classes
define("CLASSES",SRC."classes".DS);
define("BACKUP",SRC."backup".DS);

// niveau Assets
define("ASSETS","../../assets");
define("JS",ASSETS."/js");
define("CSS",ASSETS."/css");
define("IMG","/assets/img");
define("VEHICULE",IMG."/vehicule/");


// niveau Admin
define("ADMIN","../admin");
define("ADMIN_JS",ADMIN."/assets/js");
define("ADMIN_CSS",ADMIN."/assets/css");
define("ADMIN_IMG",ADMIN."/assets/img");
define("ADMIN_TPL",ADMIN."/template");
define("ADMIN_FORMS",ADMIN."/formulaires");
define("ADMIN_PAGES",ADMIN."/pages");
define("ADMIN_REQ",ADMIN."/requetes");
define("ADMIN_MEMBRE",ADMIN."/membres");
define("ADMIN_USER",ADMIN_MEMBRE."/users");
define("ADMIN_BAKCUP","admin/backup/");

// niveau librairies libs
define('LIBS','../libs');
define("BOOTSTRAP",LIBS."/bootstrap");
define("BOOTSTRAP_CSS",BOOTSTRAP."/css");
define("BOOTSTRAP_JS",BOOTSTRAP."/js");
define("JQUERY",LIBS."/jquery");
define("MENU",LIBS."/menu");
define("COLOR",LIBS."/color");
define('GRITTER_JS',LIBS."/gritter/js");
define('GRITTER_CSS',LIBS."/gritter/css");
define('LOADER',LIBS."/loader");

// niveau PHP
define('PHPS','../php');
define("DEV",PHPS."/developpement");
define("FORMULAIRES",PHPS."/formulaires");
define("PAGES",PHPS."/pages");
define("REQUETES",PHPS."/requetes");
define("TEMPLATES",PHPS."/template");
define("INTERS",PHPS."/interventions");
define("PROJ",PHPS."/projecteur");

/* CONSTANTE GLOBALE DES URL */
define("ROOT","/");
define("URL","http://".$_SERVER['SERVER_NAME']."/");
define("URLS","https://".$_SERVER['SERVER_NAME']."/");

?>