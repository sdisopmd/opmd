<?php

/*
* Fichier de class Intervention
* Fichier crée le 02/04/2014
* Auteur : Denis Sanson alias Multixvers
*/

// Info l'Etat correspond 0:Aucun, 1: En cour, 2: Reconnaissance, 3: Differee et 4 : Terminer
// Le Status correspond à l'état du véhicule soit :
// 1:En inter (orange), 3: VL reco (cyan), 5: Indisponible (noir) et 10 : Cloture inter (vert) 

class Intervention extends Enregistrement {
	//attributs
	private $tmp_id;
	private $codis_lier;
	private $dateheure;
	private $fin;
	private $nom_appelant;
	private $prenom_appelant;
	private $numero_rue;
	private $adresse;
	private $commune;
	private $etage;
	private $appt;
	private $telephone_appelant;
	private $nature_id;
	private $precision_int;
	private $complement;
	private $message_radio;
	private $differee;
	private $_pes;
	private $_vehicules;
	private $_materiels;
	private $etat;
	private $status;
	private $libelle;
	
	//constructeur
	public function __construct($id) {
		parent::__construct(T_INTERVENTION, $id);

		if($this->row) {
			$this->codis_lier = $this->row['codis_lier'];
			$this->dateheure = $this->row['dateheure'];
			$this->fin = $this->row['fin'];
			$this->nom_appelant = $this->row['nom_appelant'];
			$this->prenom_appelant = $this->row['prenom_appelant'];
			$this->numero_rue = $this->row['numero_rue'];
			$this->adresse = addslashes($this->row['adresse']);
			$this->commune = $this->row['commune'];
			$this->etage = $this->row['etage'];
			$this->appt = $this->row['appt'];
			$this->telephone_appelant = $this->row['telephone_appelant'];
			$this->nature_id = $this->row['nature_id'];
			$this->precision_int = $this->row['precision_int'];
			$this->complement = addslashes($this->row['complement']);
			$this->message_radio = addslashes($this->row['message_radio']);
			$this->differee = $this->row['differee'];
			$this->etat = $this->row['etat'];
			$this->status = $this->row['status'];
			$this->libelle = "Intervention - ".$this->id;
		} else {
			$this->codis_lier = null;
			$this->tmp_id = $id;
			$this->etat = 0;
			$this->status = 10;
			$this->differee = 0;
			$this->libelle = "Intervention - ".$id;
		}
	}
	
	//accesseurs get
	public function getCodisLier() { return $this->codis_lier; }
	public function getDateHeure() { return $this->dateheure; }
	public function getFin() { return $this->fin; }
	public function getNomAppelant() { return $this->nom_appelant; }
	public function getPrenomAppelant() { return $this->prenom_appelant; }
	public function getNumeroRue() { return $this->numero_rue; }
	public function getAdresse() { return stripslashes($this->adresse); }
	public function getCommune() { return new Commune($this->commune); }
	public function getEtage() { return $this->etage; }
	public function getAppartement() { return $this->appt; }
	public function getTelephoneAppelant() { return $this->telephone_appelant; }
	public function getPrecision($raw = false) { return $raw ? $this->precision_int : new PrecisionIntervention($this->precision_int); }
	public function getNature($raw = false) { return $raw ? $this->nature_id : new NatureIntervention($this->nature_id); }
	public function getComplement() { return stripslashes($this->complement); }
	public function getMessageRadio() { return stripslashes($this->message_radio); }
	public function isDifferee() { return ($this->differee == 1) ? true : false; }
	public function getDifferee() { return $this->differee; }
	public function getEtat() { return $this->etat; }
	public function getStatus() { return new EtatVehicule($this->status); }
	public function getLibelle() { return $this->libelle; }

	//accesseurs set
	public function setCodisLier($codis_lier = null) { $this->codis_lier = $codis_lier; }
	public function setDateHeure($dateheure = null) { $this->dateheure = $dateheure; }
	public function setFin($fin = 0) { $this->fin = $fin; }
	public function setEtat($etat = null) { $this->etat = $etat; }
	public function setStatus($status = null) { $this->status = $status; }
	public function setNomAppelant($nom_appelant = null) { $this->nom_appelant = $nom_appelant; }
	public function setPrenomAppelant($prenom_appelant = null) { $this->prenom_appelant = $prenom_appelant; }
	public function setNumeroRue($numero_rue = null) { $this->numero_rue = $numero_rue; }
	public function setAdresse($adresse = null) { $this->adresse = addslashes($adresse); }
	public function setCommune($commune = null) { $this->commune = $commune; }
	public function setEtage($etage = null) { $this->etage = $etage; }
	public function setAppartement($appt = null) { $this->appt = $appt; }
	public function setTelephoneAppelant($telephone_appelant = null) { $this->telephone_appelant = $telephone_appelant; }
	public function setNature($nature_id = null) { $this->nature_id = $nature_id; }
	public function setPrecision($precision_int = null) { $this->precision_int = $precision_int; }
	public function setComplement($complement = null) { $this->complement = addslashes($complement); }
	public function setMessageRadio($message_radio = null) { $this->message_radio = addslashes($message_radio); }
	public function setPresenceExterne($pes = null) { $this->_pes = $pes; }
	public function setVehicules($vehicules = null) { $this->_vehicules = $vehicules; if(is_array($vehicules)) $this->differee = 0; }
	public function setMateriels($materiels = null) { $this->_materiels = $materiels; if(is_array($materiels)) $this->differee = 0; }
	public function setDifferee($differee = null) { $this->differee = $differee; }
	
	public function getPresencesExternes($raw = false) {
		$all = null;
		$query = "select id_presence_externe from ".T_INTERVENTION_PRESENCE_EXTERNE." where id_intervention=?";
		$base = $this->bdd;
		$result = Mysql::query($query,$this->id);
		while($row = $result->fetch()) {
			$all[] = $raw ? $row['id_presence_externe'] : new PresenceExterne($row['id_presence_externe']);
		}
		return $all;
	}
	
	public function getVehicules($raw = false) {
		$all = null;
		$query = "select id from ".T_VEHICULE." where intervention = ?";
		$result = Mysql::query($query,$this->id);
		while($row = $result->fetch()) {
			$all[] = $raw ? $row['id'] : new Vehicule($row['id']);
		}
		return $all;
	}
	
	public function getVehiculesVisibles($raw = false) { /* hack */
		$all = null;
		$query = "select v.id from ".T_VEHICULE." v join ".T_CENTRE." c on v.centre = c.id where v.intervention = ?";
		$result = Mysql::query($query,$this->id);
		while($row = $result->fetch()) {
			$all[] = $raw ? $row['id'] : new Vehicule($row['id']);
		}
		return $all;
	}
	
	public function getMateriels($raw = false) {
		$all = null;
    $all = Materiel::getAllMaterielsByInter($this->id,$raw);
		return $all;
	}
	
	public function getMaterielsVisibles($raw = false) { /* hack */
		$all = null;
    $mats = Materiel::getAllMaterielsByInter($this->id,true);
    $tabs = null;
    if($mats != null){
      foreach($mats as $m){
        $query = "select m.id from ".T_MATERIEL." m join ".T_CENTRE." c on m.centre = c.id where m.id =  ?";
        $result = Mysql::query($query,$m);
        $tabs[] = $result->fetchAll();
      }
    }
		
		if($tabs != null){
      foreach($tabs as $t) {
        $all[] = $raw ? $t : new Materiel($t);
      }
    }
		return $all;
	}
	
	public function getPersonnel($raw = false) {
		$all = null;
		$query = "select ap.personnel
								from ".T_AFFECTATION_POSTE." ap
								where ap.vehicule in (select v.id
														from ".T_VEHICULE." v
														where intervention = ?)
									and ap.personnel <> 0";
		$result = Mysql::query($query,$this->id);
		while($row = $result->fetch()) {
			$all[] = $raw ? $row['personnel'] : new Personnel($row['personnel']);
		}
		return $all;
	}

	public function getAllVehiculesGroupId() {
		$all = null;
		$query = "select group from ".T_VEHICULE." order by position asc";
		$result = Mysql::query($query);
		while($row = $result->fetch()) {
			$all[] =  $row['group'];
		}
		return $all;
	}
	
	public function getAllMateriels($raw = false) {
		$all = null;
		$all = Materiel::getAllMaterielsByInter($this->id,$raw);
		return $all;
	}
	
	public function getAllVehicules($raw = false) {
		$all = null;
		$query = "select id from ".T_VEHICULE." where intervention = ? order by id asc";
		$result = Mysql::query($query,$this->id);
		while($row = $result->fetch()) {
			$all[] =  $raw ? $row['m.id'] : new Vehicule($row['id']);
		}
		return $all;
	}
	
	public function getAllVehiculesByGroup($group = 0, $raw = false) {
		$all = null;
		$query = "select id from ".T_VEHICULE." order by position asc";
		$result = Mysql::query($query,$this->id);
		while($row = $result->fetch()) {
			$all[] = $raw ? $row['id'] : new Vehicule($row['id']);
		}
		return $all;
	}
	
	//accesseurs bonus
	public static function getInterventionsEnCours($raw = false) {
		$all = null;
		$query = "select id from ".T_INTERVENTION." where (etat = 1 or etat = 6) and status = 1 order by dateheure desc";
		Mysql::Connect();
		$result = Mysql::query($query);
		while($row = $result->fetch()) {
			$all[] = $raw ? $row['id'] : new Intervention($row['id']);
		}
		return $all;
	}
	
	public static function getInterventionsEnReconnaissance($raw = false) {
		$all = null;
		$query = "select id from ".T_INTERVENTION." where etat = 2  and status = 3 order by dateheure desc";
		Mysql::Connect();
		$result = Mysql::query($query);
		while($row = $result->fetch()) {
			$all[] = $raw ? $row['id'] : new Intervention($row['id']);
		}
		return $all;
	}

	public static function compteurInter() {
		Mysql::Connect();
		$all = null;
		$e1 = 0; $e2 = 0; $e3 = 0; $e4 = 0; $e5 = 0;
		$all['enCour'] = $e1;
		$all['diff'] = $e2;
		$all['vlreco'] = $e3;
		$all['annul'] = $e4;
		$all['term'] = $e5;
		$query = "select id, etat from ".T_INTERVENTION;
		$result = Mysql::query($query);
		while($row = $result->fetch()) {
			if( $row['etat'] == 1 ){
				$all['enCour'] = ++$e1;
			}
			if( $row['etat'] == 2 ){
				$all['vlreco'] = ++$e2;
			}
			if( $row['etat'] == 3 ){
				$all['diff'] = ++$e3;
			}
			if( $row['etat'] == 4 ){
				$all['annul'] = ++$e4;
			}
			if( $row['etat'] == 5 ){
				$all['term'] = ++$e5;
			}
		}
		$all['total'] = $all['enCour'] + $all['diff'] + $all['vlreco'];
		return $all;
	}
	
	public static function getInterventionsTerminees($nb_jours = 0, $raw = false) {
		$it = null;
		$query = "select id
				from ".T_INTERVENTION."
				where id not in (select intervention
								from ".T_VEHICULE."
								where etat <> 10)"
				.(($nb_jours > 0) ? " and dateheure > ".(time()-$nb_jours*86400) : "")
				." and differee = false
				order by dateheure desc";
		Mysql::Connect();
		$result = Mysql::query($query);
		while($row = $result->fetch()) {
			$it[] = $raw ? $row['id'] : new Intervention($row['id']);
		}
		return $it;
	}
	
	public static function getPlageInterventionsTerminees($debut = 0, $fin = 0) {
		$it = null;
		$query = "select id
				from ".T_INTERVENTION."
				where id not in (select intervention
								from ".T_VEHICULE."
								where etat <> 10)
				 and dateheure between ? and ?
				 and differee = false
				 order by dateheure asc";
		Mysql::Connect();
		$result = Mysql::query($query,$debut,$fin);
		while($row = $result->fetch()) {
			$it[] = new Intervention($row['id']);
		}
		return $it;
	}
	
	public function getCompteRendu() {
		return new CompteRendu($this->id);
	}
	
	public static function isInterventionExiste($id = -1) {
		$exists = false;
		if(!empty($id)) {
			$query = "select count(id) from ".T_INTERVENTION." where id=?";
			Mysql::Connect();
			$result = Mysql::query($query,$id);
			$row = $result->fetch();
			$exists = ($row['count(id)']==1) ? true : false;
		}
		return $exists;
	}
	
	//commit
	public function commit() {
		$dernieremodif = time();
		if($this->notExists()) {
			$query = "insert into ".T_INTERVENTION."(id, codis_lier, dateheure, fin, dernieremodif, nom_appelant, prenom_appelant, telephone_appelant, numero_rue, commune, adresse, etage, appt, complement, message_radio, differee, nature_id, precision_int, etat, status) values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
			$result = Mysql::query($query,$this->tmp_id,$this->codis_lier,$this->dateheure,$this->fin,$dernieremodif,$this->nom_appelant,$this->prenom_appelant,$this->telephone_appelant, $this->numero_rue, $this->commune, $this->adresse,$this->etage,$this->appt,$this->complement,$this->message_radio,$this->differee,$this->nature_id,$this->precision_int,$this->etat,$this->status);
			$this->id = $this->tmp_id; 
		} else {
				$query = "update ".T_INTERVENTION."
						set codis_lier = ?,
						    dateheure = ?,
							fin = ?,
							dernieremodif = ?,
							nom_appelant = ?,
							prenom_appelant = ?,
							numero_rue = ?,
							commune = ?,
							adresse = ?,
							etage = ?,
							appt = ?,
							telephone_appelant = ?,
							nature_id = ?,
							precision_int = ?,
							complement = ?,
							message_radio = ?,
							differee = ?,
							etat = ?,
							status = ?
						where id = ?";

			$result = Mysql::query($query,$this->codis_lier,$this->dateheure,$this->fin,$dernieremodif,$this->nom_appelant,$this->prenom_appelant,$this->numero_rue, $this->commune, $this->adresse,$this->etage,$this->appt,$this->telephone_appelant,$this->nature_id,$this->precision_int,$this->complement,$this->message_radio,$this->differee,$this->etat,$this->status,$this->id);
		}
		
		if($this->exists()) {
			//présences externes
			if(is_array($this->_pes)) {
					$query = "delete from ".T_INTERVENTION_PRESENCE_EXTERNE." where id_intervention = ?";
					$result = Mysql::query($query,$this->id);
				foreach($this->_pes as $pe) {
					if($pe > 0) {
						$quantite = ( $pe['quantite'] > 0 ) ? $pe['quantite'] : 0;
						$query = "insert into ".T_INTERVENTION_PRESENCE_EXTERNE."(id_intervention, id_presence_externe, quantite) values(?,?,?)";
						$result = Mysql::query($query,$this->id,$pe['id'],$quantite);
					}
				}
			}
			
			// différée
			if( $this->differee == 0 ){
				$query = "update ".T_INTERVENTION." set differee = 0, etat = ? where id = ?";
				$result = Mysql::query($query,$this->etat,$this->id);
			}
			
			if($this->differee == 1 ){
				
				$query = "update ".T_INTERVENTION." set differee = 1, etat = ? where id = ?";
				$result = Mysql::query($query,$this->etat,$this->id);
				
				$query = "update ".T_VEHICULE." set intervention = null, etat = 10 where intervention = ?";
				$result = Mysql::query($query,$this->id);
			
        Materiel::updateInter(null,$this->id);
			}
			
			//véhicules
			if(is_array($this->_vehicules)) {
				$query = "update ".T_VEHICULE." set intervention = null where intervention = ?";
				$result = Mysql::query($query,$this->id);
				foreach($this->_vehicules as $vehicule) {
					$query = "update ".T_VEHICULE." set intervention = ?, etat = if(etat=10,1,etat) where id = ?";
					$result = Mysql::query($query,$this->id,$vehicule);
				}
					$query = "update ".T_VEHICULE." set etat = 10 where intervention is null";
					$result = Mysql::query($query);
			}
			
			//matériels
			/*if(is_array($this->_materiels)) {
				$query = "update ".T_MATERIEL." set intervention = null where intervention = ?";
				$result = Mysql::query($query,$this->id);

				foreach($this->_materiels as $materiel) {
					$query = "update ".T_MATERIEL." set intervention = ?, etat = if(etat=10,1,etat) where id = ?";
					$result = Mysql::query($query,$this->id,$materiel);
				}
					$query = "update ".T_MATERIEL." set etat = 10 where intervention is null";
					$result = Mysql::query($query);
			}*/
      
      if($this->etat > 3){
        
      }
		}
	}
	
	//delete
	public function delete() {
		if($this->exists() && $this->getInterventionsLiees(true) == null) {

			//présences externes
			$query = "delete from ".T_INTERVENTION_PRESENCE_EXTERNE." where id_intervention=?";
			$result = Mysql::query($query,$this->id);
			
			//véhicules
			$query = "update ".T_VEHICULE." set intervention = null, etat = 10 where intervention = ?";
			$result = Mysql::query($query,$this->id);
				
			//matériel
			Materiel::removeInter($this->id);
			
			//fiche bilan
			$query = "delete from ".T_FICHE_BILAN." where id=?";
			$result = Mysql::query($query,$this->id);
			
			//intervention
			$query = "delete from {$this->table} where id=?";
			$result = Mysql::query($query,$this->id);

		}
	}
	
	// A voir pour le reconnaissance si on peutle liée au différée en conservant le status
	public function setReconnaissance() {
		if($this->exists()) {
		
			$query = "update ".T_INTERVENTION." set differee = 0, etat = 3 where id = ?";
			$result = Mysql::query($query,$this->id);

			$query = "update ".T_VEHICULE." set intervention = null, etat = 3 where intervention = ?";
			$result = Mysql::query($query,$this->id);

			Materiel::removeInter($this->id);

			$this->etat = 2;
		}
	}
	
	public static function listEtat(){
		return array("En création","En cours","Reconnaissance","Differee","Terminer","Annuler");
	}
	
	public function recupEtat($status){
		$list = array("En création","En cours","Reconnaissance","Differee","Terminer","Annuler");
		return $list[$status];
	}
	
	public function classEtat($status){
		$list = array("default","success","info","warning","danger","purple");
		return $list[$status];
	}
	
	// Mets à jour l'etat soit 0:Aucun, 1: En cour, 2: Reconnaissance, 3: Differee, 4 : Terminer, 5 : Annuler
	public function upStatus($etat) {	
		  
		if($etat == 4 ){
			$stop = time();
			$this->etat = $etat;
			$this->fin = $stop;
			if($this->exists()) {
				$query = "update ".T_INTERVENTION." set etat = ?, fin = ? where id = ?";
				$result = Mysql::query($query,$this->etat,$this->fin,$this->id);
			}
		} 
		
		if($etat == 5 ){
			$stop = time();
			$this->etat = $etat;
			$this->fin = $stop;
			if($this->exists()) {
				$query = "update ".T_INTERVENTION." set etat = ?, fin = ? where id = ?";
				$result = Mysql::query($query,$this->etat,$this->fin,$this->id);
			}
		} 
		
		if($etat < 4 ) {
			$this->etat = $etat;
			if($this->exists()) {
				$query = "update ".T_INTERVENTION." set etat = ? where id = ?";
				$result = Mysql::query($query,$this->etat,$this->id);
			}
		}
		
	}
	
	public static function listInterventions() {
		$query = "select * from ".T_INTERVENTION." order by dateheure desc";
		Mysql::Connect();
		$req = Mysql::query($query);
		$tabInter = array();
		while ($row = $req->fetch(PDO::FETCH_OBJ)) {
			array_push($tabInter,$row);
		}
		return $tabInter;
	}
	
	public static function getAllInterventions() {
		$query = "select * from ".T_INTERVENTION." order by id";
		Mysql::Connect();
		$req = Mysql::query($query);
		$tabInter = null;
		while ($row = $req->fetch()) {
			$tabInter[] = new Intervention($row['id']);
		}
		return $tabInter;
	}

	public static function getAllInterventionsDifferees() {
		$query = "select * from ".T_INTERVENTION." where differee = 1 order by id";
		Mysql::Connect();
		$req = Mysql::query($query);
		$tabInter = null;
		while ($row = $req->fetch()) {
			$tabInter[] = new Intervention($row['id']);
		}
		return $tabInter;
	}
	
	public static function getAllInterByEtat($etat) {
		$query = "select * from ".T_INTERVENTION." where etat = ? order by id";
		Mysql::Connect();
		$req = Mysql::query($query,$etat);
		$tabInter = null;
		while ($row = $req->fetch()) {
			$tabInter[] = new Intervention($row['id']);
		}
		return $tabInter;
	}
	
	public static function verifCodis($codis) {
		Mysql::Connect();
		$query = "select id from ".T_INTERVENTION." where id = ?";
		$req = Mysql::query($query,$codis);
		$row = $req->fetch(PDO::PARAM_STR);
		$reponse = ($row['id'] == $codis) ? "erreur" : "ok" ;
		if($reponse == "ok"){
			$query2 = "select codis_lier from ".T_INTERVENTION;
			$req2 = Mysql::query($query2);
			while ($row = $req2->fetch()) {
				$num = trim($row['codis_lier']);
				$tmp = explode(" ",$num);
				foreach($tmp as $t){
					if($t == $codis){
						return "erreur";
					}
				}
			}
		}
		return $reponse;
	}
	
	public static function validCodis($codis) {
		Mysql::Connect();
		$query = "select id from ".T_INTERVENTION." where id = ?";
		$req = Mysql::query($query,$codis);
		$row = $req->fetch(PDO::PARAM_STR);
		$reponse = ($row['id'] == $codis) ? "ok" : "erreur" ;
		return $reponse;
	}
	
	public static function searchCodis($codis) {
		Mysql::Connect();
		$query = "select id from ".T_INTERVENTION." where id = ?";
		$req = Mysql::query($query,$codis);
		$row = $req->fetch(PDO::PARAM_STR);
		$reponse = ($row['id'] == $codis) ? $codis : 0 ;
		return $reponse;
	}
}

?>