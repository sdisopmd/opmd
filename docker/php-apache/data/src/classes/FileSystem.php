<?php
/**
 * \brief : Permet de faire des opération sur le système de fichier
 * \author Denis Sanson
 * \copyright Multixvers
 * \version PHP 7.0+
 */
class FileSystem {
  
  /**
   * Permet de connaître de système d'exploitation que le serveur utilise
   * @return string Retourne true si Windows et false si Linux ou Mac
   */
  public static function isWindows(){
    $os = ( preg_match("/^[A-Z\:]{1}/",$_SERVER['DOCUMENT_ROOT']) == 1 ) ? true : false;
    return $os;
  }
  
  
  /**
   * Permet de reformaté un chemin système selon le typa de système d'exploitation que le serveur utilise
   * @param string $chemin Chemin complet du fichier ou dossier
   * @return string Retourne le chemin formaté selon le système d'exploitation
   */
  public function os($chemin){
    $os = ( preg_match("/^[A-Z\:]{1}/",$_SERVER['DOCUMENT_ROOT']) == 1 ) ? "windows" : "linux";
    if($os == "linux"){
			$rep = str_replace("\\","/",$chemin);
		} else {
			$rep = str_replace("/","\\",$chemin);
		}
    return $rep;
  }
  
  /**
   * Permet de créer une structure de dossier récurvive
   * @param string $dir Chemin complet des dossiers à créer
   */
  public function mkdir($dir){
    $dir = $this->os($dir);
    if(!file_exists($dir)){
      mkdir($this->os($dir), 0775, true);
    }
  }
  
  /**
   * Permet de supprimer un dossier
   * @param string $dir Chemin complet du dossier à supprimer
   */
  public function rmdir($dir){
    $dir = $this->os($dir);
    if(file_exists($dir)){
      chmod($this->os($dir), 0775);
      rmdir($this->os($dir));
    }
  }
  
  /**
   * Permet d'effectuer une copie de fichier sécurisé
   * @param string $src Chemin complet du fichier source
   * @param string $dest Chemin complet du fichier de destination
   */
  public function copy($src,$dest){
    $src = $this->os($src);
    $dest = $this->os($dest);
    if(file_exists($dest)){
      $this->unlink($dest);
    }
    copy($src, $dest);
  }
  
  /**
   * Permet de déplacer un fichier
   * @param string $src Chemin complet du fichier source
   * @param string $dest Chemin complet du fichier de destination
   */
  public function move($src,$dest){
    $src = $this->os($src);
    $dest = $this->os($dest);
    if(file_exists($dest)){
      $this->unlink($dest);
    }
    $this->copy($src, $dest);
    $this->unlink($src);
  }
  
  /**
   * Permet de supprimer un fichier avec vérification avant
   * @param string $file Chemin complet du fichier
   */
  public function unlink($file){
    $file = $this->os($file);
    if(file_exists($file)){
      chmod($file, 0777);
      unlink($file);
    }
  }
  
  /**
   * Permet de créer un fichier avec vérification avant
   * @param string $file Chemin complet du fichier
   */
  public function touch($file){
    $file = $this->os($file);
    if(!file_exists($file)){
      touch($file);
    }
  }
  
  /**
   * Récupère la liste des fichiers d'un dossier
   * @param string $dos chemin complet du dossier
   * @return array retourne un tableau qui liste les fichiers du dossier sans les "." ".."
   **/
	public function recupFichiers($dos){
    $dos = $this->os($dos);
    $tab = null;
    $files = scandir($dos);
    if($files){
      foreach($files as $f){
        if($f != "." && $f != ".."){
          $tab[] = $f;
        }
      }
    }
    return $tab;
  }
  
  /**
   * Permet de supprimer tout les dossier y compris les fichiers contenu
   * @param string $dir C'est le dossier cible a supprimer avec son contenu
   */
  public function removeRecursive($dir){
    $dir = $this->os($dir);
    $iterator = new DirectoryIterator($dir);
    foreach ($iterator as $fileinfo) {
      if($fileinfo != "." && $fileinfo != ".."){
        if($fileinfo->isDir()){
          $chem = $fileinfo->getPathname();
          self::removeRecursive($chem);
          $this->rmdir($fileinfo->getPathname());
        } else {
          $this->unlink($fileinfo->getPathname());
        }
      }
    }
  }
  
  /**
   * Permet de copier tout les dossier y compris les fichiers contenu
   * @param string $src C'est le dossier source a copier avec son contenu
   * @param string $dest C'est le dossier cible a coller avec son contenu
   */
  public function copyRecursive($src,$dest){
    $src = $this->os($src);
    $dest = $this->os($dest);
    if(!file_exists($dest)) $this->mkdir($dest);
    $iterator = new DirectoryIterator($src);
    foreach ($iterator as $fileinfo) {
      if($fileinfo != "." && $fileinfo != ".."){
        if($fileinfo->isDir() && !file_exists($dest)){
          $chem = $fileinfo->getPathname();
          $this->mkdir($fileinfo->getPathname());
          self::copyRecursive($chem,$dest);
        } else {
          $this->copy($fileinfo->getPathname(),$this->os($dest."/".$fileinfo->getFilename()));
        }
      }
    }
  }
  
  /**
   * Permet de copier tout les dossiers et sous dossiers y compris les fichiers contenu
   * @param string $src C'est le dossier source a copier avec son contenu
   * @param string $dest C'est le dossier cible a coller avec son contenu
   */
  public function copyMultiRecursiveDirectory($src,$dest){
    $src = $this->os($src);
    $dest = $this->os($dest);
    if(!file_exists($dest)) $this->mkdir($dest);
    $dir_iterator = new RecursiveDirectoryIterator($src, RecursiveDirectoryIterator::SKIP_DOTS);
    $iterator = new RecursiveIteratorIterator($dir_iterator, RecursiveIteratorIterator::SELF_FIRST);
    foreach($iterator as $element){
      if($element->isDir()){
        $this->mkdir($dest . DIRECTORY_SEPARATOR . $iterator->getSubPathName());
      } else{
        $this->copy($element, $dest . DIRECTORY_SEPARATOR . $iterator->getSubPathName());
      }
    }
  }
  
  /**
   * Permet de parcourir une arborecense de dossier
   * @param string $dir est le chemin absolut du système du dossier devant contenir les sous dossier
   * @return array Tableau contenant l'arborescence des dossiers
   */
  public function parcourtRecursive($dir){
    $dir = $this->os($dir);
    $iterator = new DirectoryIterator($dir);
    $html = null;
    foreach ($iterator as $fileinfo) {
      if($fileinfo != "." && $fileinfo != ".."){
        if($fileinfo->isDir()){
          $html[] =  $fileinfo->getPathname();
          $chem = $fileinfo->getPathname();
          $html[] =  self::parcourtRecursive($chem);
        } else {
          $html[] =  $fileinfo->getPathname();
        }
      }
    }
    return $html;
  }
  
  /**
   * Permet de formater un nom de fichier pour lui retiré tout caractères spéciaux et remplacer les tiret et espace par le symbole underscore
   * @param string $nom Un nom de fichier
   * @return string Retourne le nom du fichier sans caractères spéciaux et les espaces étant remplacé par _
   */
	public function formatNom($nom) { 
		$a = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'Ā', 'ā', 'Ă', 'ă', 'Ą', 'ą', 'Ć', 'ć', 'Ĉ', 'ĉ', 'Ċ', 'ċ', 'Č', 'č', 'Ď', 'ď', 'Đ', 'đ', 'Ē', 'ē', 'Ĕ', 'ĕ', 'Ė', 'ė', 'Ę', 'ę', 'Ě', 'ě', 'Ĝ', 'ĝ', 'Ğ', 'ğ', 'Ġ', 'ġ', 'Ģ', 'ģ', 'Ĥ', 'ĥ', 'Ħ', 'ħ', 'Ĩ', 'ĩ', 'Ī', 'ī', 'Ĭ', 'ĭ', 'Į', 'į', 'İ', 'ı', 'Ĳ', 'ĳ', 'Ĵ', 'ĵ', 'Ķ', 'ķ', 'Ĺ', 'ĺ', 'Ļ', 'ļ', 'Ľ', 'ľ', 'Ŀ', 'ŀ', 'Ł', 'ł', 'Ń', 'ń', 'Ņ', 'ņ', 'Ň', 'ň', 'ŉ', 'Ō', 'ō', 'Ŏ', 'ŏ', 'Ő', 'ő', 'Œ', 'œ', 'Ŕ', 'ŕ', 'Ŗ', 'ŗ', 'Ř', 'ř', 'Ś', 'ś', 'Ŝ', 'ŝ', 'Ş', 'ş', 'Š', 'š', 'Ţ', 'ţ', 'Ť', 'ť', 'Ŧ', 'ŧ', 'Ũ', 'ũ', 'Ū', 'ū', 'Ŭ', 'ŭ', 'Ů', 'ů', 'Ű', 'ű', 'Ų', 'ų', 'Ŵ', 'ŵ', 'Ŷ', 'ŷ', 'Ÿ', 'Ź', 'ź', 'Ż', 'ż', 'Ž', 'ž', 'ſ', 'ƒ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ǎ', 'ǎ', 'Ǐ', 'ǐ', 'Ǒ', 'ǒ', 'Ǔ', 'ǔ', 'Ǖ', 'ǖ', 'Ǘ', 'ǘ', 'Ǚ', 'ǚ', 'Ǜ', 'ǜ', 'Ǻ', 'ǻ', 'Ǽ', 'ǽ', 'Ǿ', 'ǿ'); 
		$b = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o'); 
		$txt = str_replace($a, $b, $nom); 
		$c = array('[', ']', '%', '#', '"', '{', '}', '(', ')', '|', '`', '~', '&', '^', '@', '$', '£', '¤', 'µ', '*', '+', '!', '§', ':', '/', '\\', ';', '?', ',', '<', '>', '=', '°', '¨'); 
		$d = array(''); 
		$txt = str_replace($c, $d, $txt);
		$txt = str_replace('_', '-', $txt);
		$txt = str_replace(' ', '-', $txt);
		return $txt;
	}
  
}

