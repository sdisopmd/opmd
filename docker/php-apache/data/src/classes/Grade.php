<?php

/*
* Fichier de class Grade
* Fichier crée le 02/04/2014
* Auteur : Denis Sanson alias Multixvers
*/

class Grade extends Enregistrement{
	//attributs
	private $libelle;
	private $abreviation;
	private $position;
	
	//constructeur
	public function __construct($id) {
		parent::__construct(T_GRADE, $id);
		if($this->row) {
			$this->libelle = $this->row['libelle'];
			$this->abreviation = $this->row['abreviation'];
			$this->position = $this->row['position'];
		}
	}
	
	//accesseurs get
	public function getLibelle() { return $this->libelle; }
	public function getAbreviation() { return $this->abreviation; }
	public function getPosition() { return $this->position; }
	
	//accesseurs set
	public function setLibelle($libelle = null) { $this->libelle = $libelle; }
	public function setAbreviation($abreviation = null) { $this->abreviation = $abreviation; }
	public function setPosition($position = null) { $this->position = $position; }
	
	//accesseurs bonus
	public static function getAllGrades() {
		$all = null;
		$query = "select id from ".T_GRADE." order by position, libelle asc";
		Mysql::Connect();
		$result = Mysql::query($query);
		while($row = $result->fetch()) {
			$all[] = new Grade($row['id']);
		}
		return $all;
	}
	
	//commit
	public function commit() {
		if($this->notExists()) {
			$query = "insert into ".T_GRADE."(libelle, abreviation , position) values(?, ?, ?)";
			$result = Mysql::query($query,$this->libelle,$this->abreviation,$this->position);
		} else {
			$query = "update ".T_GRADE." set libelle = ?, set abreviation = ?, position = ? where id = ?";
			$result = Mysql::query($query,$this->libelle,$this->abreviation,$this->position,$this->id);
		}
	}
	
}

?>