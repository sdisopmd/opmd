<?php

/*
* Fichier de class DataSituation 
* Fichier crée le 19/11/2016
* Auteur : Denis Sanson alias Multixvers
*/

class DataSituation {
  //attributs
  private $inters;
	private $moyens;
	private $personnes;
	private $annimaux;
	private $sites;

  //constructeur
  public function __construct() {
		$this->inters = null;
		$this->moyens = null;
		$this->personnes = null;
		$this->annimaux = null;
		$this->sites = null;
  }
	
	//accesseurs get
  public function getInters() { return $this->inters; }
	public function getMoyens() { return $this->moyens; }
	public function getPersonnes() { return $this->personnes; }
	public function getAnnimaux() { return $this->annimaux; }
	public function getSites() { return $this->sites; }
 
  //accesseurs set
  public function setInters( $inters = null) { $this->inters = $inters; }
	public function setMoyens( $moyens = null) { $this->moyens = $moyens; }
	public function setPersonnes( $personnes = null) { $this->personnes = $personnes; }
	public function setAnnimaux( $annimaux = null) { $this->annimaux = $annimaux; }
	public function setSites( $sites = null) { $this->sites = $sites; }
	
	
	
}



?>