<?php

/*
* Fichier de class EtatVehicule
* Fichier crée le 02/04/2014
* Auteur : Denis Sanson alias Multixvers
*/

class EtatVehicule extends Enregistrement {
	//attributs
	private $libelle;
	private $couleur_fond;
	private $couleur_texte;
	
	//constructeur
	public function __construct($id) {
		parent::__construct(T_ETAT_VEHICULE, $id);
		if($this->row) {
			$this->libelle = $this->row['libelle'];
			$this->couleur_fond = $this->row['couleur_fond'];
			$this->couleur_texte = $this->row['couleur_texte'];
		}
	}
	
	//accesseurs get
	public function getLibelle() { return $this->libelle; }
	public function getCouleurFond() { return $this->couleur_fond; }
	public function getCouleurTexte() { return $this->couleur_texte; }
	
	//accesseurs set
	public function setLibelle($v = null) { $this->libelle = $v; }
	public function setCouleurFond($v = null) { $this->couleur_fond = $v; }
	public function setCouleurText($v = null) { $this->couleur_texte = $v; }
	
	//accesseurs bonus
	public static function getAllEtatsVehicule() {
		$all = null;
		$query = 'select id from '.T_ETAT_VEHICULE.' order by id asc';
		Mysql::Connect();
		$result = Mysql::query($query);
		while($row = $result->fetch()) {
			$all[] = new EtatVehicule($row['id']);
		}
		return $all;
	}
	
	//commit
	public function commit() {
		if($this->notExists()) {
			$query = "insert into ".T_ETAT_VEHICULE."(libelle, couleur_fond, couleur_texte) values(?, ?, ?)";
			$result = Mysql::query($query,$this->libelle,$this->couleur_fond,$this->couleur_texte);
		} else {
			$query = "update ".T_ETAT_VEHICULE." set libelle = ?,couleur_fond = ?,couleur_texte = ? where id = ?";
			$result = Mysql::query($query,$this->libelle,$this->couleur_fond,$this->couleur_texte,$this->id);
		}
	}
}

?>