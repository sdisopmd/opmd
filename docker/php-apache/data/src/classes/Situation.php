<?php

/*
* Fichier de class Situation
* Fichier crée le 19/06/2014
* Auteur : Denis Sanson alias Multixvers
*/

class Situation extends Enregistrement implements Serializable {
    //attributs
    private $creer;
    private $centre;
    private $data;

    //constructeur
    public function __construct($id) {
      parent::__construct(T_SITUATION, $id);
      if($this->row) {
        $this->creer = $this->row['creer'];
        $this->centre = $this->row['centre'];
        $this->data = unserialize($this->row['data']);
      } else {
        $this->creer = DateHeure::getCurrentDateSql();
      }
    }
    
    //accesseurs get
    public function getCreer() { return DateHeure::DHC($this->creer); }
    public function getCentre() { return $this->centre; }
    public function getData() { return $this->data; }
  
    //accesseurs set
    public function setCreer( $creer = null) { $this->creer = DateHeure::FD($creer); }
    public function setCentre( $centre = null) { $this->centre = $centre; }
    public function setData(DataSituation $data = null) { $this->data = $data; }

    // spécial date
    public static function getDate() { return DateHeure::DHC(DateHeure::getCurrentDateSql()); }
    
    //accesseurs bonus
    public static function getAllSituations() {
      $all = null;
      $query = 'select id from '.T_SITUATION.' order by id desc';
      Mysql::Connect();
      $result = Mysql::query($query);
      while($row = $result->fetch()) {
        $all[] = new Situation($row['id']);
      }
      return $all;
    }

    public static function getAllSituationsByDate($date) {
      $all = null;
      $query = 'select id from '.T_SITUATION.' where DATE(creer) = ?';
      Mysql::Connect();
      $result = Mysql::query($query,$date);
      while($row = $result->fetch()) {
        $all[] = new Situation($row['id']);
      }
      return $all;
    }

    public static function lastSituationId() {
      $all = null;
      $query = 'select MAX(id) as id from '.T_SITUATION;
      Mysql::Connect();
      $result = Mysql::query($query);
      while($row = $result->fetch()) {
        $all = new Situation($row['id']);
      }
      return $all;
    }
    
    //commit
    public function commit() {
      if($this->notExists()) {
        $query = "insert into ".T_SITUATION."(creer, centre, data) values(?, ?, ?)";
        $result = Mysql::query($query, $this->creer, $this->centre, $this->serialize());
      } else {
        $query = "update ".T_SITUATION." set creer = ?, centre = ?, data = ? where id = ?";
        $result = Mysql::query($query,$this->creer, $this->centre, $this->serialize(), $this->id);
      }
    }
    
    //delete
    public function delete() {
      if($this->exists()) {
        $query = "delete from ".T_SITUATION." where id=?";
        $result = Mysql::query($query,$this->id);
      }
    }
    
    public function serialize() {
      return serialize($this->data);
    }
    
    public function unserialize($data) {
      $this->data = unserialize($data);
    }
	
}


?>