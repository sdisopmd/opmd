<?php

/*
* Fichier de class Personnel
* Fichier crée le 02/04/2014
* Auteur : Denis Sanson alias Multixvers
*/

class Personnel extends Enregistrement {
	//attributs
	private $grade;
	private $nom;
	private $prenom;
	private $absent;
	private $centre;
	private $libelle;
	
	
	//constructeur
	public function __construct($id) {
		parent::__construct(T_PERSONNEL, $id);		
		$this->absent = 0;
		if($this->row) {
			$this->grade = $this->row['grade'];
			$this->nom = $this->row['nom'];
			$this->prenom = $this->row['prenom'];
			$this->centre = $this->row['centre'];
			$this->absent = $this->row['absent'];
			$this->libelle = $this->nom ." ".$this->prenom;
		}
	}
	
	//accesseurs get
	public function getGrade($raw = false) { return $raw ? $this->grade : new Grade($this->grade); }
	public function getNom() { return $this->nom; }
	public function getPrenom() { return $this->prenom; }
	public function getCentre() { return new Centre($this->centre); }
	public function isAbsent() { return $this->absent; }
	public function getLibelle() { return $this->libelle = $this->nom ." ".$this->prenom; }
	
	//accesseurs set
	public function setGrade($v = null) { $this->grade = $v; }
	public function setNom($v = null) { $this->nom = $v; }
	public function setPrenom($v = null) { $this->prenom = $v; }
	public function setCentre($v = null) { $this->centre = $v; }
	public function setAbsent($v = null) { $this->absent = $v; }
	public function setLibelle( $libelle = null ) { $this->libelle = $libelle; }
	
	//accesseurs bonus
	public static function getAllPersonnel() {
		$all = null;
		Mysql::Connect();
		$query = 'select p.id from '.T_PERSONNEL.' p left join '.T_GRADE.' g on p.grade = g.id order by g.position, p.nom, p.prenom asc';
		$result = Mysql::query($query);
		while($row = $result->fetch()) {
			$all[] = new Personnel($row['id']);
		}
		return $all;
	}
	
	public static function getListPersonnel() {
		$all = null;
		Mysql::Connect();
		$query = 'select * from '.T_PERSONNEL.' order by nom, prenom asc';
		$result = Mysql::query($query);
		while($row = $result->fetch()) {
			$all[] = array("id" => $row['id'],"nom" => $row['nom']." ".$row['prenom']);
		}
		return $all;
	}
	
	public static function getAllPersonnelPresent() {
		$all = null;
		Mysql::Connect();
		$query = 'select p.id from '.T_PERSONNEL.' p join '.T_HORAIRE.' h on h.personnel = p.id left join '.T_GRADE.' g on p.grade = g.id where h.depart is null order by g.position, p.nom, p.prenom asc';
		$result = Mysql::query($query);
		while($row = $result->fetch()) {
			$all[] = new Personnel($row['id']);
		}
		return $all;
	}
	
	public static function getAllPersonnelPresentDispo() {
		$all = null;
		Mysql::Connect();		
		$query = 'select p.id from '.T_PERSONNEL.' p join '.T_HORAIRE.' h on h.personnel = p.id left join '.T_GRADE.' g on p.grade = g.id left join '.T_AFFECTATION_POSTE.' a on p.id = a.personnel left join '.T_VEHICULE.' v on v.id = a.vehicule where h.depart is null and (v.etat is null or v.etat = 10) order by g.position, p.nom, p.prenom asc';
		$result = Mysql::query($query);
		while($row = $result->fetch()) {
			$all[] = new Personnel($row['id']);
		}
		return $all;
	}
	
	public static function getAllPersonnelPresentEnInter() {
		$all = null;
		Mysql::Connect();		
		$query = 'select p.id from '.T_PERSONNEL.' p join '.T_HORAIRE.' h on h.personnel = p.id left join '.T_GRADE.' g on p.grade = g.id left join '.T_AFFECTATION_POSTE.' a on p.id = a.personnel left join '.T_VEHICULE.' v on v.id = a.vehicule where h.depart is null and (v.etat is not null and v.etat <> 10) order by v.libelle';
		$result = Mysql::query($query);
		while($row = $result->fetch()) {
			$all[] = new Personnel($row['id']);
		}
		return $all;
	}
	
	public static function getAllPersonnelAbsent() {
		$all = null;
		Mysql::Connect();
		$query = 'select p.id from '.T_PERSONNEL.' p left join '.T_GRADE.' g on p.grade = g.id where p.id not in(select p.id from '.T_PERSONNEL.' p join '.T_HORAIRE.' h on h.personnel = p.id where h.depart is null) order by p.nom, p.prenom asc';
		$result = Mysql::query($query);
		while($row = $result->fetch()) {
			$all[] = new Personnel($row['id']);
		}
		return $all;
	}
	
	private static function getNbPersonnelCaserneGradePositionBetween($min=-1, $max=999) {
		Mysql::Connect();
		$query = "select p.id from ".T_PERSONNEL." p join ".T_HORAIRE." h on h.personnel = p.id join ".T_GRADE." g on p.grade = g.id left join ".T_AFFECTATION_POSTE." ap on p.id = ap.personnel left join ".T_VEHICULE." v on v.id = ap.vehicule where h.depart is null and g.position between ? and ? and (v.intervention = 0 or v.intervention is null)";
		$result = Mysql::query($query,$min,$max);
		$nb = $result->rowCount();
		return $nb;
	}
	
	public static function getNbOfficiersCaserne() {
		return self::getNbPersonnelCaserneGradePositionBetween(5, 35);
	}
	
	public static function getNbSousOfficiersCaserne() {
		return self::getNbPersonnelCaserneGradePositionBetween(40, 55);
	}
	
	public static function getNbHommesDuRangCaserne() {
		return self::getNbPersonnelCaserneGradePositionBetween(60, 70);
	}
	
	private static function getNbPersonnelInterGradePositionBetween($min=-1, $max=999) {
		Mysql::Connect();
		$query = "select p.id from ".T_PERSONNEL." p join ".T_HORAIRE." h on h.personnel = p.id join ".T_GRADE." g on p.grade = g.id left join ".T_AFFECTATION_POSTE." ap on p.id = ap.personnel left join ".T_VEHICULE." v on v.id = ap.vehicule where h.depart is null and g.position between ? and ? and (v.intervention <> 0 and v.intervention is not null)";
		$result = Mysql::query($query,$min,$max);
		$nb = $result->rowCount();
		return $nb;
	}
	
	public static function getNbOfficiersInter() {
		return self::getNbPersonnelInterGradePositionBetween(5, 35);
	}
	
	public static function getNbSousOfficiersInter() {
		return self::getNbPersonnelInterGradePositionBetween(40, 55);
	}
	
	public static function getNbHommesDuRangInter() {
		return self::getNbPersonnelInterGradePositionBetween(60, 70);
	}
	
	public function getNomFormate() {
		return trim($this->getGrade()->getAbreviation().' '.$this->nom.' '.$this->prenom);
	}
	
	public function isEnIntervention() {
		$query = "select p.id from ".T_PERSONNEL." p left join ".T_AFFECTATION_POSTE." a on p.id = a.personnel left join ".T_VEHICULE." v on v.id = a.vehicule where p.id = ? and v.etat <> 10 and v.intervention <> 0";
		$result = Mysql::query($query,$this->id);
		$en_inter = $result->fetch();
		return ($en_inter !== false);
	}
	
	public static function getEnInter($id) {
		Mysql::connect();
		$id_v = 0;
		$id_i = 0;
		$id_h = 0;
		// rechercher si le perso et affecter à un poste ctacodis_affectation_poste
		$query = "select vehicule from ". T_AFFECTATION_POSTE ." where personnel = ?";
		$result = Mysql::query($query,$id);
		$id_v = $result->fetch();
		$id_v = ( isset($id_v['vehicule']) ) ? $id_v['vehicule'] : false;
		
		if( $id_v != false ){
			// rechercher id de l'inter en fonction de l'id du véhicule
			$sql = "SELECT intervention FROM ".T_VEHICULE." WHERE id = ?";
			$rep = Mysql::query($sql,$id_v);
			$id_i = $rep->fetch();
			$id_i = ( isset($id_i['intervention']) ) ? $id_i['intervention'] : false;
		} else {
			// rechercher si le personnel est en caserne
			$sql2 = "SELECT id FROM ".T_HORAIRE." WHERE personnel = ? and depart is null";
			$rep2 = Mysql::query($sql2,$id);
			$id_h = $rep2->fetch();
			$id_h = ( isset($id_h['id']) ) ? $id_h['id'] : false;
		}
		
		$pero = null;
		
		if ( $id_v != false && $id_v != 0 ){
			// ici le personnel est sur l'inter $id_i
			return $id_i;
		} else if ( $id_h != false && $id_h != 0 ) {
			// ici le personnel est en caserne
			return "caserne";
		} else {
			// le personnel est ni en inter ni en caserne
			return "aucun";
		}
		
	}
	
	public function getCouleur() {
		return ($this->isAbsent() ? 'cyan' : ($this->isEnIntervention() ? 'red' : 'lime'));
	}
	
	public function getTextCouleur() {
		return ($this->isAbsent() ? 'black' : ($this->isEnIntervention() ? 'white' : 'black'));
	}
	
	public function getPostesVehicules() {
		$all = null;
		$query = "select poste, vehicule from ".T_AFFECTATION_POSTE." where personnel = ?";
		$result = Mysql::query($query,$this->id);
		while($row = $result->fetch()) {
			$all[] = array(new PosteVehicule($row['poste']), new Vehicule($row['vehicule']));
		}
		return $all;
	}
	
	//en relation avec les horaires
	public function arrive() {
		if($this->exists()) {
			$query = "select * from ".T_HORAIRE." h where h.personnel = ? and h.depart is null";
			$result = Mysql::query($query,$this->id);
			if($result->fetchColumn() == 0) {
				$time = time();
				$query = "insert into ".T_HORAIRE."(personnel, arrivee) values(?, ?)";
				$result = Mysql::query($query,$this->id,$time);
			}
		}
	}
	
	public function undoArrive() {
		if($this->exists()) {
			$query = "delete from ".T_HORAIRE." where personnel = ? and depart is null";
			$result = Mysql::query($query,$this->id);
		}
	}
	
	public function repart($time) {
		if($this->exists()) {
			if(empty($time)) $time = time();

			$query = "update ".T_HORAIRE." set depart = ? where personnel = ? and depart is null";
			$result = Mysql::query($query,$time,$this->id);
			
			$query = "update ".T_AFFECTATION_POSTE." set personnel = 0 where personnel = ?";
			$result = Mysql::query($query,$this->id);

			$query = "update ".T_PERSONNEL." set absent = 0 where id = ?";
			$result = Mysql::query($query,$this->id);
		}
	}
	
	//commit
	public function commit() {
		if($this->notExists()) {
			$query = "insert into ".T_PERSONNEL."(grade, nom, prenom, centre, absent) values(?, ?, ?, ?, ?)";
			$result = Mysql::query($query,$this->grade,$this->nom,$this->prenom,$this->centre,$this->absent);
		} else {
			$query = "update ".T_PERSONNEL." set grade = ?, nom = ?, prenom = ?, centre = ?, absent = ? where id = ?";
			$result = Mysql::query($query,$this->grade,$this->nom,$this->prenom,$this->centre,$this->absent,$this->id);
		}
	}
	
	//delete
	public function delete() {
		$query = "update ".T_AFFECTATION_POSTE." set personnel = 0 where personnel = ?";
		$result = Mysql::query($query,$this->id);

		$query = "delete from ".T_PERSONNEL." where id = ?";
		$result = Mysql::query($query,$this->id);
	}
}

?>