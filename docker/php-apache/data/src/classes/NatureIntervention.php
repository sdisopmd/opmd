<?php

/*
* Fichier de class NatureIntervention
* Fichier crée le 02/04/2014
* Auteur : Denis Sanson alias Multixvers
*/

class NatureIntervention extends Enregistrement {
	//attributs
	private $libelle;
	
	//constructeur
	public function __construct($id) {
		parent::__construct(T_NATURE_INTERVENTION, $id);
		if($this->row) {
			$this->libelle = $this->row['libelle'];
		}
	}
	
	//accesseurs get
	public function getLibelle() { return $this->libelle; }
	
	//accesseurs set
	public function setLibelle($libelle = null) { $this->libelle = $libelle; }
	
	//accesseurs bonus
	public function getPrecisions() {
		$all = null;
		$query = "select id from ".T_PRECISION_INTERVENTION." where nature=? order by libelle asc";
		$result = Mysql::query($query,$this->id);
		while($row = $result->fetch()) {
			$all[] = new PrecisionIntervention($row['id']);
		}
		return $all;
	}
	
	public static function getAllNatures() {
		$all = null;
		$query = 'select id from '.T_NATURE_INTERVENTION.' order by libelle asc';
		Mysql::Connect();
		$result = Mysql::query($query);
		while($row = $result->fetch()) {
			$all[] = new NatureIntervention($row['id']);
		}
		return $all;
	}
	
	//commit
	public function commit() {
		if($this->notExists()) {
			$query = "insert into ".T_NATURE_INTERVENTION."(libelle) values(?)";
			$result = Mysql::query($query,$this->libelle);	
		} else {
			$query = "update ".T_NATURE_INTERVENTION." set libelle = ? where id = ?";
			$result = Mysql::query($query,$this->libelle,$this->id);
		}
	}

	//delete
    public function delete() {
		if($this->exists()) {
		  $query = "delete from ".T_NATURE_INTERVENTION." where id=?";
		  $result = Mysql::query($query,$this->id);
		}
	  }
}

?>