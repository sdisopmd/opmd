<?php

/*
* Fichier de class Centre
* Fichier crée le 02/04/2014
* Auteur : Denis Sanson alias Multixvers
*/

class Centre extends Enregistrement {
	//attributs
	private $tmp_id;
	private $libelle;
	private $abreviation;
	private $position;
	
	//constructeur
	public function __construct($id) {
		parent::__construct(T_CENTRE, $id);
		if($this->row) {
			$this->libelle = $this->row['libelle'];
			$this->abreviation = $this->row['abreviation'];
			$this->position = $this->row['position'];
		}else {
			$this->tmp_id = $id;
		}
	}
	
	//accesseurs get
	public function getLibelle() { return $this->libelle; }
	public function getAbreviation() { return $this->abreviation; }
	public function getPosition() { return $this->position; }
	
	//accesseurs set
	public function setLibelle($v = null) { $this->libelle = $v; }
	public function setAbreviation($v = null) { $this->abreviation = $v; }
	public function setPosition($v = null) { $this->position = $v; }
	
	//accesseurs bonus
	public static function getAllCentres() {
		$all = null;
		$query = 'select * from '.T_CENTRE.' order by position asc';
		Mysql::Connect();
		$result = Mysql::query($query);
		while($row = $result->fetch()) {
			$all[] = new Centre($row['id']);
		}
		return $all;
	}
	
	public static function getListCentres() {
		$all = null;
		$query = 'select * from '.T_CENTRE.' order by id asc';
		Mysql::Connect();
		$result = Mysql::query($query);
		while($row = $result->fetch()) {
			$all[] = new Centre($row['id']);
		}
		return $all;
	}
	
	public function getAllVehicules() {
		$all = null;
		$query = "select id from ".T_VEHICULE." where centre = ? order by libelle asc";
		$result = Mysql::query($query,$this->id);
		while($row = $result->fetch()) {
			$all[] = new Vehicule($row['id']);
		}
		return $all;
	}
	
	public function getAllMateriels() {
		$all = null;
		$query = "select id from ".T_MATERIEL." where centre = ? order by libelle asc";
		$result = Mysql::query($query,$this->id);
		while($row = $result->fetch()) {
			$all[] = new Materiel($row['id']);
		}
		return $all;
	}
	
	public function getAllVehiculesAffectables() {
		$all = null;
		$query = "select distinct v.id from ".T_VEHICULE." v join ".T_AFFECTATION_POSTE." ap on v.id = ap.vehicule where v.centre = ? order by v.libelle asc";
		$result = Mysql::query($query,$this->id);
		while($row = $result->fetch()) {
			$all[] = new Vehicule($row['id']);
		}
		return $all;
	}
	
	//commit
	public function commit() {
		if($this->notExists()) {
			$query = "insert into ".T_CENTRE."(libelle, abreviation, position) values(?, ?, ?)";
			$result = Mysql::query($query,$this->libelle,$this->abreviation,$this->position);
		} else {
			$query = "update ".T_CENTRE." set libelle = ?, abreviation = ?, position = ? where id = ?";
			$result = Mysql::query($query,$this->libelle,$this->abreviation,$this->position,$this->id);
		}
		
	}
	
	//delete
	public function delete() {
		if($this->exists()) {
			//affectations aux postes des véhicules
			$query = "delete from ".T_AFFECTATION_POSTE." where vehicule in (select id from ".T_VEHICULE." where centre=?)";
			$result = Mysql::query($query,$this->id);
			//véhicules du centre
			$query = "delete from ".T_VEHICULE." where centre=?";
			$result = Mysql::query($query,$this->id);
			//centre
			$query = "delete from ".T_CENTRE." where id=?";
			$result = Mysql::query($query,$this->id);
		}
	}
	
	public static function listCentres() {
		$query = "select * from ".T_CENTRE." order by id";
		Mysql::Connect();
		$result = Mysql::query($query);
		$tabCentre = array();
		while ($row = $result->fetch(PDO::FETCH_OBJ)) {
			array_push($tabCentre,$row);
		}
		return $tabCentre;
	}
	
	public static function getLastPosition() {
		$all = null;
		$query = "select * from ".T_CENTRE." order by position desc";
		Mysql::Connect();
		$result = Mysql::query($query);
		while($row = $result->fetch()) {
			$all[] = $row['position'];
		}
		$pos = $all[0] + 1;
		return $pos;
	}
	
}

?>