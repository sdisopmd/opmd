<?php

/*
* Fichier de class Commune
* Fichier crée le 02/04/2014
* Auteur : Denis Sanson alias Multixvers
*/

class Commune extends Enregistrement {
	//attributs
	private $libelle;
	private $code_postal;
	private $position;
	private $visible;
	
	//constructeur
	public function __construct($id) {
		parent::__construct(T_COMMUNE, $id);
		if($this->row) {
			$this->libelle = $this->row['libelle'];
			$this->code_postal = $this->row['code_postal'];
			$this->position = $this->row['position'];
			$this->visible = $this->row['visible'];
		}
	}
	
	//accesseurs get
	public function getLibelle() { return $this->libelle; }
	public function getCodePostal() { return $this->code_postal; }
	public function getPosition() { return $this->position; }
	public function getVisible() { return $this->visible; }
	
	//accesseurs set
	public function setLibelle( $libelle = null) { $this->libelle = $libelle; }
	public function setCodePostal( $code_postal = null ) { $this->code_postal = $code_postal; }
	public function setPosition( $position = null ) { $this->position = $position; }
	public function setVisible( $visible = null ) { $this->position = $visible; }
	
	//accesseurs bonus
	public static function getAllCommunes($full = false) {
		$all = null;
		if($full == true){
			$query = 'select id from '.T_COMMUNE.' order by position';
		} else {
			$query = 'select id from '.T_COMMUNE.' where visible = 1 order by position';
		}
		
		Mysql::Connect();
		$result = Mysql::query($query);
		while($row = $result->fetch()) {
			$all[] = new Commune($row['id']);
		}
		return $all;
	}
	
	public static function getLastPosition() {
		$nb = null;
		$query = 'select max(position) from '.T_COMMUNE;
		Mysql::Connect();
		$result = Mysql::query($query);
		$nb = $result->fetch();
		$tmp = intval($nb[0]);
		$nb = $tmp + 1;
		return $nb;
	}
	
	//commit
	public function commit() {
		if($this->notExists()) {
			$query = "insert into ".T_COMMUNE."(libelle,code_postal,position,visible) values(?, ?, ?, ?)";
			$result = Mysql::query($query,$this->libelle,$this->code_postal,$this->position,$this->visible);
		} else {
			$query = "update ".T_COMMUNE." set libelle = ?, code_postal = ?, position = ?, visible = ? where id = ?";
			$result = Mysql::query($query,$this->libelle,$this->code_postal,$this->position,$this->visible,$this->id);
		}
		
	}
	
	//delete
	public function delete() {
		if($this->exists()) {
			// recupérer les id d'adresses associées à la commune
			$query = "select id from ".T_ADRESSE." where commune = ?";
			$result = Mysql::query($query,$this->id);
			while($row = $result->fetch()) {
			// supprimer les adresses (ici update des inter + suppression des maps + suppression des adresses)
				$adresse = new Adresse($row['id']);
				$adresse->delete();
			}
			// suppression de la commune
			$query = "delete from ".T_COMMUNE." where id=?";
			$result = Mysql::query($query,$this->id);
		}
	}
	
	public static function isUnique($name){
		$reponse = "ok";
		$query = 'select id from '.T_COMMUNE.' where libelle = ?';
		Mysql::Connect();
		$result = Mysql::query($query,$name);
		while($row = $result->fetch()) {
			$reponse = ($row['id']) ? "erreur" : "ok";
		}
		echo $reponse;
	}
	
}

?>