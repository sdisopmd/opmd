<?php

/*
* Fichier de class Groupes
* Fichier crée le 02/04/2014
* Auteur : Denis Sanson alias Multixvers
*/

class Groupes extends Enregistrement {
	private $libelle;
	private $position;
	private $type;
	
	//constructeur
	public function __construct($id) {
		parent::__construct(T_GROUPES, $id);
		if($this->row) {
			$this->libelle = $this->row['libelle'];
			$this->position = $this->row['position'];
			$this->type = $this->row['type'];
		}
	}
	
	//accesseurs get
	public function getLibelle() { return $this->libelle; }
	public function getPosition() { return $this->position; }
	public function getType() { return $this->type; }
	
	//accesseurs set
	public function setLibelle($libelle = null) { $this->libelle = $libelle; }
	public function setPosition($position = null) { $this->position = $position; }
	public function setType($type = null) { $this->type = $type; }
	
	//accesseurs bonus
	public static function getAllGroupes($order = 'position') {
		$all = null;
		$query = "select id from ".T_GROUPES." order by ? asc";
		Mysql::Connect();
		$result = Mysql::query($query,$order);
		while($row = $result->fetch()) {
			$all[] = new Groupes($row['id']);
		}
		return $all;
	}
	
	public static function getLastPosition() {
		$all = null;
		$query = "select * from ".T_GROUPES." order by position desc";
		Mysql::Connect();
		$result = Mysql::query($query);
		while($row = $result->fetch()) {
			$all[] = $row['position'];
		}
		$pos = $all[0] + 1;
		return $pos;
	}
	
	public function getAllCentres($order = 'position') {
		$all = null;
		$query = "select id from ".T_CENTRE." where groupement = ? order by ? asc";
		$result = Mysql::query($query,$this->id,$order);
		while($row = $result->fetch()) {
			$all[] = new Centre($row['id']);
		}
		return $all;
	}
	
	//commit
	public function commit() {
		if($this->notExists()) {
			$query = "insert into ".T_GROUPES."(libelle, position, type) values(?, ?, ?)";
			$result = Mysql::query($query,$this->libelle,$this->position,$this->type);	
			$this->id = $this->bdd->lastInsertId(); 
		} else {
			$query = "update ".T_GROUPES." set libelle = ?,position = ?, type = ? where id = ?";
			$result = Mysql::query($query,$this->libelle,$this->position,$this->type,$this->id);	
		}
				
		
	}
	
	//delete
	public function delete() {
		if($this->exists()) {
			$query = "delete from ".T_GROUPES." where id = ?";
			$result = Mysql::query($query,$this->id);
		}
	}
	
	
}

?>