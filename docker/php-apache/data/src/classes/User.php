<?php

/*
* Fichier de class User
* Fichier crée le 01/05/2014
* Auteur : Denis Sanson alias Multixvers
*/

class User extends Enregistrement {
    //attributs
    private $nom;
    private $prenom;
    private $login;
    private $pass;
    private $groupe;
    private $active;
    private $connecter;
    private $info;
    
    //constructeur
    public function __construct($id) {
      parent::__construct(T_USER, $id);
      if($this->row) {
        $this->nom = $this->row['nom'];
        $this->prenom = $this->row['prenom'];
        $this->login = $this->row['login'];
        $this->pass = $this->row['pass'];
        $this->groupe  = $this->row['groupe_id'];
        $this->active = $this->row['active'];
        $this->connecter = $this->row['connecter'];
        $this->info = $this->row['info'];
      } else {
        $this->active = 0;
        $this->connecter = 0;
        $this->info = 0;
      }
    }
    
    //accesseurs get
    public function getNom() { return $this->nom; }
    public function getPrenom() { return $this->prenom; }
    public function getLogin() { return $this->login; }
    public function getPass() { return $this->pass; }
    public function getGroupe() { return $this->groupe; }
    public function getActive() { return $this->active; }
    public function getConnecter() { return $this->connecter; }
    public function getInfo() { return $this->info; }
    public function getNomFormat() { return $this->nom." ".$this->prenom; }
    public function getNomDB() { return $this->prenom."_".$this->id; }
    
    //accesseurs set
    public function setNom( $nom = null) { $this->nom = $nom; }
    public function setPrenom( $prenom = null) { $this->prenom = $prenom; }
    public function setLogin( $login = null) { $this->login = $login; }
    public function setPass( $pass = null) { $this->pass = $pass; }
    public function setGroupe( $groupe = null) { $this->groupe = $groupe; }
    public function setActive( $active = null) { $this->active = $active; }
    public function setConnecter( $connecter = null) { return $this->connecter = $connecter; }
    public function setInfo( $info = null) { return $this->info = $info; }
    
    //accesseurs bonus
    public static function getAllUsers() {
      $all = null;
      $query = 'select id from '.T_USER.' order by id asc';
      Mysql::Connect();
      $result = Mysql::query($query);
      while($row = $result->fetch()) {
        $all[] = new User($row['id']);
      }
      return $all;
    }
    
    public static function isLogin() {
      $all = null;
      if( isset($_SESSION['users']['id']) ) {
        $all = new User($_SESSION['users']['id']);
      } 
      return $all;
    }
    
    public static function isAuth() {
      $all = null;
      if( User::isLogin() != null ) {
        $u = User::isLogin();
        $all = new Group($u->getGroupe());
      } 
      return $all;
    }
    
    public static function login($user,$pass) {
      Mysql::Connect();
      $all = null;
      $query = 'select * from '.T_USER.' where login = ? and pass = ?';
      $result = Mysql::query($query,$user,$pass);
      $row = $result->fetch();
      if( isset($row['id']) ) {
        $all = $row['id'];
        $sql = "update ".T_USER." set connecter = 1 where id = ?";
        $reponse = Mysql::query($sql,$row['id']);
      } else {
        $all = "erreur";
      }
      return $all;
    }
    
    public function logout() {
        $all = null;
        $query = "update ".T_USER." set connecter = 0 where id = ?";
        $result = Mysql::query($query,$this->id);
        $all = "ok";
        session_unset(); 
        return $all;
    }
    
    public function updateInfo($info) {
      $query = "update ".T_USER." set info = ? where id = ?";
      $result = Mysql::query($query,$info,$this->id);
    }
    
    //commit
    public function commit() {
      if($this->notExists()) {
        $query = "insert into ".T_USER."(nom, prenom, login, pass, groupe_id, active, connecter, info) values(?, ?, ?, ?, ?, ?, ?, ?)";
        $result = Mysql::query($query, $this->nom, $this->prenom, $this->login, $this->pass, $this->groupe, $this->active, $this->connecter, $this->info);
      } else {
        $query = "update ".T_USER." set nom = ?, prenom = ?, login = ?, pass = ?, groupe_id = ?, active = ? , connecter = ?, info = ? where id = ?";
        $result = Mysql::query($query,$this->nom, $this->prenom, $this->login, $this->pass, $this->groupe, $this->active, $this->connecter, $this->info, $this->id);
      }
      
    }
    
    //delete
    public function delete() {
      if($this->exists()) {
        $query = "delete from ".T_USER." where id=?";
        $result = Mysql::query($query,$this->id);
      }
    }
    
    public static function isLoginUnique($login){
      $reponse = "ok";
      $query = 'select id from '.T_USER.' where login = ?';
      Mysql::Connect();
      $result = Mysql::query($query,$login);
      while($row = $result->fetch()) {
        $reponse = ($row['id']) ? "erreur" : "ok";
      }
      echo $reponse;
    }
  
}

?>