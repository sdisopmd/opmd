<?php

/*
* Fichier de class PosteVehicule
* Fichier crée le 02/04/2014
* Auteur : Denis Sanson alias Multixvers
*/

class PosteVehicule extends Enregistrement {
	//attributs
	private $libelle;
	private $obligatoire;
	
	//constructeur
	public function __construct($id) {
		parent::__construct(T_POSTE_VEHICULE, $id);
		if($this->row) {
			$this->libelle = $this->row['libelle'];
			$this->obligatoire = $this->row['obligatoire'];
		}
	}
	
	//accesseurs get
	public function getLibelle() { return $this->libelle; }
	public function getObligatoire() { return $this->obligatoire; }
	
	//accesseurs set
	public function setLibelle($libelle = null) { $this->libelle = $libelle; }
	public function setObligatoire($obligatoire = false) { $this->obligatoire = $obligatoire; }
	
	//accesseurs bonus
	public static function getAllPostesVehicule() {
		$all = null;
		Mysql::Connect();
		$query = 'select id from '.T_POSTE_VEHICULE.' order by libelle asc';
		$result = Mysql::query($query);
		while($row = $result->fetch()) {
			$all[$row['id']] = new PosteVehicule($row['id']);
		}
		return $all;
	}
	
	public static function getAllPostesObligatoires($raw = false) {
		$all = null;
		Mysql::Connect();
		$query = 'select id from '.T_POSTE_VEHICULE.' where obligatoire=true';
		$result = Mysql::query($query);
		while($row = $result->fetch()) {
			$all[] = $raw ? $row['id'] : new PosteVehicule($row['id']);
		}
		return $all;
	}
	
	//commit
	public function commit() {
		if($this->notExists()) {
			$query = "insert into ".T_POSTE_VEHICULE."(libelle, obligatoire) values(?, ?)";
			$result = Mysql::query($query,$this->libelle,$this->obligatoire);
		} else {
			$query = "update ".T_POSTE_VEHICULE." set libelle = ?, obligatoire = ? where id = ?";
			$result = Mysql::query($query,$this->libelle,$this->obligatoire,$this->id);
		}
	}
}

?>