<?php
/**
 * \brief : Système de gestion pour manipuler un fichier de log
 * \author Denis Sanson
 * \copyright Multixvers
 * \version PHP 7.0+
 */

class Logs {
  
  private $type;
  private $date;
  private $texte;
  private $time;
  private $fichier;
  private $chemin;
  private $source;
  
  public function __construct($type,$date,$texte,$time,$fichier,$chemin){
    $this->type = $type;
    $this->date = $date;
    $this->texte = $texte;
    $this->time = $time;
    $this->fichier = $fichier;
    $this->chemin = $chemin;
    $this->source = Log::rep($chemin."/".$fichier);
  }
  
  public function getType(){ return $this->type; }
  public function getDate(){ return $this->date; }
  public function getTexte(){ return $this->texte; }
  public function getTime(){ return $this->time; }
  public function getFichier(){ return $this->fichier; }
  public function getChemin(){ return $this->chemin; }
  public function getSource(){ return $this->source; }
  
  public function setType($type){ return $this->type = $type; }
  public function setDate($date){ return $this->date = $date; }
  public function setTexte($texte){ return $this->texte = $texte; }
  public function setTime($time){ return $this->time = $time; }
  public function setFichier($fichier){ return $this->fichier = $fichier; }
  public function setChemin($chemin){ return $this->chemin = $chemin; }
  public function setSource($source){ return $this->source = $source; }
  
}