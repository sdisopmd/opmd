<?php
/**
 * \brief : Système de gestion du temps pour les dates et les heures
 * \author Denis Sanson
 * \copyright Multixvers
 * \version PHP 7.0+
 */

final class DateHeure {
	
	/*
		CS = JJMMAAAA
    AN = AAAA
    JOUR = JJ
    MOIS = MM
		JMA = JJ/MM/AAAA
		HM = HHhMM
		C = JJ/MM/AAAA HHhMM
		J = jour
		M = mois
		DHL = date et heure format long
		DHC = date et heure format court = C
    DLC = Date Longue Courrante
		D = date format long
		H = heure avec :
    LOG = spécialement pour les logs JJ-MM-AAAA à hh:mm:ss
	*/

  /**
   * Permet d'avoir une date type traditionnel JJ/MM/AAAA à HH:MM:SS
   * @param int $timestamp Timestamp unix depuis le 1 janvier 1970
   * @return string le nom du mois ( ex : 01/12/2016 à 23:35:59 )
   */
  public static function courte($timestamp){
    $date = date('d/m/Y à H:i:s',$timestamp);
    return $date;
  }
  
  /**
   * Permet de formaté une date
   * @param string $date est une date au format SQL ( ex : 2016-01-31 00:00:00 )
   * @return string Retourne la date d'entrée au format JJ/MM/AAAA à HH:MM:SS ( ex : 01/12/2016 à 23:35:59 )
   */
  public static function format($date){
    $zone = new DateTimeZone('Europe/Paris');
		$d = new DateTime($date,$zone );
		return $d->format('d/m/Y à H:i:s');
  }

  /**
   * Permet de formaté une date de type lecture humaine
   * @param int $timestamp Timestamp unix depuis le 1 janvier 1970
   * @return string Retourne une date formaté suivant l'exemple ( ex : le Mardi 21 Décembre 2012, à 21h00 )
   */
  public static function longue($timestamp){
    $date = DateHeure::timestampToSql($timestamp);
    $zone = new DateTimeZone('Europe/Paris');
    $d = new DateTime($date,$zone );
		$jour = self::J($d->format('w'));
		$num = $d->format('j');
		$mois = self::M($d->format('n'));
		$an = $d->format('Y');
		$heure = $d->format('H\hi');
		$dates = "le ".ucfirst($jour)." ".$num." ".$mois." ".$an. ", à ".$heure;
		return $dates;
  }

  /**
   * Permet de formater une date inverser type 20160101
   * @param string $date C'est une date en AAAAMMJJ
   * @return string Retourne une date formaté suivant l'exemple ( ex : Lundi 1 Janvier 2016 )
   */
  public static function reverseDate($date){
    $an = substr($date,0,4);
    $mois = substr($date,4,2);
    $jour = substr($date,6,2);
    $date = $an."-".$mois."-".$jour;
    $zone = new DateTimeZone('Europe/Paris');
    $d = new DateTime($date,$zone );
    $time = $d->getTimestamp();
    $retour = DateHeure::D2($time);
    return $retour;
  }
  

  /**
   * Permet d'avoir une date courante
   * @return string Retourne une date au format JJMMAAAA ( ex : 31122016 )
   */
	public static function CS() {
		return date('dmY');
	}
  
  /**
   * Permet d'avoir l'année courante
   * @return string Retourne l'année courante sur 4 chiffres ( ex : 2016 )
   */
  public static function AN() {
		return date('Y');
	}
  
  /**
   * Permet d'avoir le numéro du jour courant
   * @return string Retourne le numéro du jour courant sur 2 chiffres ( ex : 31 )
   */
  public static function JOUR() {
		return date('d');
	}
  
  /**
   * Permet d'avoir le numéro du mois courant
   * @return string Retourne le numéro du mois courant sur 2 chiffres ( ex : 12 )
   */
  public static function MOIS() {
		return date('m');
	}
	
  /**
   * Permet de formaté une date
   * @param string $date est une date au format SQL ( ex : 2016-01-31 00:00:00 )
   * @return string Retourne la date d'entrée au format JJ/MM/AAAA ( ex : 01/01/2016 )
   */
	public static function JMA($date) {
    $zone = new DateTimeZone('Europe/Paris');
		$d = new DateTime($date,$zone );
		return $d->format('d/m/Y');
	}

  /**
   * Permet de formaté un temps
   * @param string $date est une date au format SQL ( ex : 2016-01-31 00:00:00 )
   * @return string Retourne une date formaté suivant l'exemple ( ex : 0h00 )
   */
	public static function HM($date) {
    $zone = new DateTimeZone('Europe/Paris');
		$d = new DateTime($date,$zone );
		return $d->format('G\hi');
	}

  /**
   * Permet de formaté une date
   * @param string $date est une date au format SQL ( ex : 2016-01-31 00:00:00 )
   * @return string Retourne une date formaté suivant l'exemple ( ex : 01/01/2016 00h00 )
   */
	public static function C($date) {
    $zone = new DateTimeZone('Europe/Paris');
		$d = new DateTime($date,$zone );
		return $d->format('d/m/Y H\hi');
	}

  /**
   * Permet d'avoir le nom du jour en français depuis son numéro PHP
   * @param int $j Numéro du jour PHP de 0 à 6 soit 0 Dimanche et 6 Samedi
   * @return string Retourne le nom du jour ( ex : Mardi )
   */
	public static function J($j){
		// w = 0 à 6 soit 0 Dimanche et 6 Samedi
		$jours = array("Dimanche","Lundi","Mardi","Mercredi","Jeudi","Vendredi","Samedi");
		return $jours[$j];
	}

  /**
   * Permet d'avoir le nom du mois en français depuis son numéro PHP
   * @param int $m Numéro du mois 1 à 12
   * @return string le nom du mois ( ex : Août )
   */
	public static function M($m){
		// n = 1 à 12 soit 1 Janvier et 12 Décembre
		$mois = array("aucun","Janvier","Février","Mars","Avril","Mai","Juin","Juillet","Août","Septembre","Octobre","Novembre","Décembre");
		return $mois[$m];
	}

  /**
   * Permet de formaté une date
   * @param string $date est une date au format SQL ( ex : 2016-01-31 00:00:00 )
   * @return string Retourne une date formaté suivant l'exemple ( ex : Lundi 1 Janvier 2016 à 00:00 )
   */
	public static function DHL($date){
		$zone = new DateTimeZone('Europe/Paris');
		$d = new DateTime($date,$zone );
		$jour = self::J($d->format('w'));
		$num = $d->format('j');
		$mois = self::M($d->format('n'));
		$an = $d->format('Y');
		$heure = $d->format('H:i');
		$dates = ucfirst($jour)." ".$num." ".$mois." ".$an. " à ".$heure;
		return $dates;
	}
  
  /**
   * Permet d'avoir la date courrante avec un format long
   * @return string Retourne une date formaté suivant l'exemple ( ex : le Lundi 1 Janvier 2016 à 00h00 )
   */
	public static function DLC(){
    $date = DateHeure::getCurrentDateSql();
		$zone = new DateTimeZone('Europe/Paris');
		$d = new DateTime($date,$zone );
		$jour = self::J($d->format('w'));
		$num = $d->format('j');
		$mois = self::M($d->format('n'));
		$an = $d->format('Y');
		$heure = $d->format('H\hi');
		$dates = "le ".ucfirst($jour)." ".$num." ".$mois." ".$an. " à ".$heure;
		return $dates;
	}

  /**
   * Permet de formaté une date
   * @param string $date est une date au format SQL ( ex : 2016-01-31 00:00:00 )
   * @return string Retourne une date formaté suivant l'exemple ( ex : 01/01/2016 à 0h00 )
   */
	public static function DHC($date){
		$zone = new DateTimeZone('Europe/Paris');
		$d = new DateTime($date,$zone );
		return $d->format('d/m/Y à G\hi');
  }
  
  /**
   * Permet de formaté une date
   * @param string $date est une date au format string ( ex : 01/01/2016 à 0H00 )
   * @return string Retourne une date au format SQL ( ex : 2016-01-31 00:00:00 )
   */
	public static function FD($dates){
    // sépare la date de l'heure
    $tmp = explode(" ",$dates);
    $date = explode("/",$tmp[0]);
    $time = explode("h",$tmp[2]);
    // format la date et heure
    $sql = $date[2]."-".$date[1]."-".$date[0]." ".$time[0].":".$time[1].":00";
    return $sql;
	}

  /**
   * Permet de formaté une date
   * @param string $date est une date au format SQL ( ex : 2016-01-31 00:00:00 )
   * @return string Retourne une date formaté suivant l'exemple ( ex : Lundi 1 Janvier 2016 )
   */
	public static function D($date){
		$zone = new DateTimeZone('Europe/Paris');
		$d = new DateTime($date,$zone );
		$jour = self::J($d->format('w'));
		$num = $d->format('j');
		$mois = self::M($d->format('n'));
		$an = $d->format('Y');
		$dates = ucfirst($jour)." ".$num." ".$mois." ".$an;
		return $dates;
	}
  
  /**
   * Permet de formaté une date
   * @param int $timestamp est un timestamp unix
   * @return string Retourne une date formaté suivant l'exemple ( ex : Lundi 1 Janvier 2016 )
   */
	public static function D2($timestamp){
    $date = DateHeure::timestampToSql($timestamp);
    $zone = new DateTimeZone('Europe/Paris');
    $d = new DateTime($date,$zone );
		$jour = self::J($d->format('w'));
		$num = $d->format('j');
		$mois = self::M($d->format('n'));
		$an = $d->format('Y');
		$dates = ucfirst($jour)." ".$num." ".$mois." ".$an;
		return $dates;
	}

  /**
   * Permet de formaté un temps
   * @param string $date est une date au format SQL ( ex : 2016-01-31 00:00:00 )
   * @return string Retourne une date formaté suivant l'exemple ( ex : 0h00 )
   */
	public static function H($date){
		$zone = new DateTimeZone('Europe/Paris');
		$d = new DateTime($date,$zone );
		return $d->format('G\hi');
	}
  
  /**
   * Permet d'avoir la date courrante pour l'utiliser en paramètre des fonction courte de DateHeure
   * @return string Retourne une date au format SQL ( ex : 2016-01-31 00:00:00 )
   */
  public static function getCurrentDateSql(){
    return DateHeure::timestampToSql(time());
  }
  
  
  /**
  * Permet de formaté une date
  * @param string $date est une date au format sql ( ex : 2016-01-31 00:00:00 )
  * @return string Retourne une date formaté suivant l'exemple ( ex : le Mardi 21 Décembre 2012, à 21h00 )
  */
  public static function fullDateAndTime($date){
    $zone = new DateTimeZone('Europe/Paris');
    $d = new DateTime($date,$zone );
		$jour = self::J($d->format('w'));
		$num = $d->format('j');
		$mois = self::M($d->format('n'));
		$an = $d->format('Y');
		$heure = $d->format('H\hi');
		$dates = "le ".$jour." ".$num." ".$mois." ".$an. ", à ".$heure;
		return $dates;
  }

  /**
  * Permet de formaté une date
  * @param string $date est une date au format sql ( ex : 2016-01-31 00:00:00 )
  * @return string Retourne une date formaté suivant l'exemple ( ex : le Mardi 21 Décembre, à 21h00 ) 
  */
  public static function getDateAndTime($date){
    $zone = new DateTimeZone('Europe/Paris');
    $d = new DateTime($date,$zone );
		$jour = self::J($d->format('w'));
		$num = $d->format('j');
		$mois = self::M($d->format('n'));
		$heure = $d->format('H\hi');
		$dates = "le ".$jour." ".$num." ".$mois.", à ".$heure;
		return $dates;
  }

  /**
  * Permet de formaté une date
  * @param string $date est une date au format sql ( ex : 2016-01-31 00:00:00 )
  * @return string Retourne une date formaté suivant l'exemple ( ex : le Mardi 21 Décembre )
  */
  public static function getDates($date){
    $zone = new DateTimeZone('Europe/Paris');
    $d = new DateTime($date,$zone );
		$jour = self::J($d->format('w'));
		$num = $d->format('j');
		$mois = self::M($d->format('n'));
		$an = $d->format('Y');
		$dates = "le ".ucfirst($jour)." ".$num." ".$mois;
		return $dates;
  }

  /**
  * Permet de formaté une date
  * @param string $date est une date au format sql ( ex : 2016-01-31 00:00:00 )
  * @return string Retourne une date formaté suivant l'exemple ( ex : 21 Décembre )
  */
  public static function getMonthDay($date){
    $zone = new DateTimeZone('Europe/Paris');
    $d = new DateTime($date,$zone );
		$num = $d->format('j');
		$mois = self::M($d->format('n'));
		$dates = $num." ".$mois;
		return $dates;
  }

  /**
  * Permet de formaté une date
  * @param string $date est une date au format sql ( ex : 2016-01-31 00:00:00 )
  * @return string Retourne une date formaté suivant l'exemple ( ex : 21 Décembre 2012 )
  */
  public static function getMonthYear($date){
    $zone = new DateTimeZone('Europe/Paris');
    $d = new DateTime($date,$zone );
		$num = $d->format('j');
		$mois = self::M($d->format('n'));
		$an = $d->format('Y');
		$dates = $num." ".$mois." ".$an;
		return $dates;
  }

  /**
  * Permet de récupérer une date formaté depuis un timestamp unix nombre de seconde écoulé depuis le 1 janvier 1970 00:00:00 GMT
  * @param int $timestamp est un timestamp unix nombre de seconde écoulé depuis le 1 janvier 1970 00:00:00 GMT
  * @return string Retourne une date formaté suivant l'exemple ( ex : le Mardi 21 Décembre 2012, à 21h00 et 00s )
  */
  public static function convertTimestamp($timestamp){
    $date = DateHeure::timestampToSql($timestamp);
    $zone = new DateTimeZone('Europe/Paris');
    $d = new DateTime($date,$zone );
		$jour = self::J($d->format('w'));
		$num = $d->format('j');
		$mois = self::M($d->format('n'));
		$an = $d->format('Y');
		$heure = $d->format('H\hi');
    $sec = $d->format("s");
		$dates = "le ".ucfirst($jour)." ".$num." ".$mois." ".$an. ", à ".$heure." et ".$sec."s";
		return $dates;
  }

 /**
 * Permet de récupérer le timestamp unix nombre de seconde écoulé depuis le 1 janvier 1970 00:00:00 GMT
 * @param mixed $objet est un objet datetime ou une date au format sql ( ex : 2016-01-31 00:00:00 )
 * @return string Retourne le timestamp de l'objet date passé en paramètre ( ex : 1356120000 )
 */
  public static function convertDatetime($objet){
    if(gettype($objet) == "string") {
      $objet = new Datetime($objet);
    }
    $temp = $objet->getTimestamp();
    return $temp;
  }

  /**
   * Permet de convertir un objet DateTime en timestamp unix
   * @param DateTime $objet est un objet DateTime
   * @return string Retourne le timestamp de l'objet date passé en paramètre ( ex : 1356120000 )
   */
  public static function datetimeToTimestamp(DateTime $objet){
    return $objet->getTimestamp();
  }

  /**
   * Permet de convertir une date format sql en timestamp unix
   * @param string $date est une date au format sql ( ex : 2016-01-31 00:00:00 )
   * @return string Retourne le timestamp de l'objet date passé en paramètre ( ex : 1356120000 )
   */
  public static function dateToTimestamp($date){
    $zone = new DateTimeZone('Europe/Paris');
    $d = new DateTime($date,$zone );
    $timestamp = $d->getTimestamp();
    return $timestamp;
  }

  /**
   * Permet de convertir un timestamp unix en objet DateTime
   * @param int $timestamp est un timestamp unix nombre de seconde écoulé depuis le 1 janvier 1970 00:00:00 GMT
   * @return DateTime Retourne un objet DateTime
   */
  public static function timestampToDatetime($timestamp){
    $date = DateHeure::timestampToSql($timestamp);
    $zone = new DateTimeZone('Europe/Paris');
    $d = new DateTime($date,$zone );
    return $d;
  }

  /**
   * Permet de convertir un timestamp unix en une date au format sql
   * @param int $timestamp est un timestamp unix nombre de seconde écoulé depuis le 1 janvier 1970 00:00:00 GMT
   * @return string Retourne une date au format sql ( ex : 2016-01-31 00:00:00 )
   */
  public static function timestampTodate($timestamp){
    return DateHeure::timestampToSql($timestamp);
  }

  /**
  * Transforme un timestamp unix en date pour un champ DateTime en SQL
  * @param int $timestamp est un timestamp unix nombre de seconde écoulé depuis le 1 janvier 1970 00:00:00 GMT
  * @return string Retourne une date pour un champ DateTime SQL ( ex : 2012-12-21 21:00:00 )
  */
  public static function timestampToSql($timestamp){
    $date = date('Y-m-d H:i:s',$timestamp);
    return $date;
  }

  /**
  * Transforme un champ DateTime SQL en timestamp unix nombre de seconde écoulé depuis le 1 janvier 1970 00:00:00 GMT
  * @param string $datetime est un champ datetime de SQL ( ex : 2016-01-31 00:00:00 )
  * @return string Retourne le timestamp de la date passé en paramètre ( ex : 1356120000 )
  */
  public static function sqlToTimestamp($datetime){
    $timestamp = self::dateToTimestamp($datetime); 
    return $timestamp;
  }

  /**
  * Permet de récupérer le nom du mois en français depuis une date
  * @param string $date est une date au format sql ( ex : 2016-01-31 00:00:00 )
  * @return string Retourne le nom du mois en français ( ex : Décembre )
  */
  public static function getMonth($date){
    $zone = new DateTimeZone('Europe/Paris');
    $d = new DateTime($date,$zone );
		$mois = self::M($d->format('n'));
		$dates = $mois;
		return $dates;
  }

  /**
  * Permet de récupérer le nom du jour en français depuis une date
  * @param string $date est une date au format sql ( ex : 2016-01-31 00:00:00 )
  * @return string Retourne le nom du jour en français ( ex : Mardi )
  */
  public static function getDay($date){
    $zone = new DateTimeZone('Europe/Paris');
    $d = new DateTime($date,$zone );
		$jour = self::J($d->format('w'));
		$dates = ucfirst($jour);
		return $dates;
  }

  /**
  * Permet de récupérer uniquement l'heure et minute d'une date
  * @param string $date est une date au format sql ( ex : 2016-01-31 00:00:00 )
  * @return string Retourne un temps au format HH:mm ( ex : 21:00 )
  */
  public static function getTimes($date){
    $zone = new DateTimeZone('Europe/Paris');
    $d = new DateTime($date,$zone );
		$heure = $d->format('H:i');
		$dates = $heure;
		return $dates;
  }

  /**
  * Permet d'avoir le singulier ou pluriel du nombre d'année
  * @param number $annee est un nombre d'année
  * @return string Retourne le singulier ou pluriel du nombre d'année(s)
  */
  public static function viewYear($annee){
    if($annee > 1) {
        return $annee. " ans";
    } else {
        return "une année";
    }
  }

  /**
  * Permet d'avoir le singulier ou pluriel du nombre de jour
  * @param number $jours est un nombre de jour
  * @return string Retourne le singulier ou pluriel du nombre de jour(s)
  */
  public static function viewDays($jours){
    if($jours > 1) {
      return $jours. " jours";
    } else {
      return "un jour";
    }
  }

  /**
  * Permet d'avoir le singulier ou pluriel du nombre d'heure
  * @param number $heure est un nombre d'heure
  * @return string Retourne le singulier ou pluriel du nombre d'heure(s)
  */
  public static function viewHours($heure){
    if($heure > 1) {
      return $heure. " heures";
    } else {
      return "une heure";
    }
  }

  /**
  * Permet d'avoir le singulier ou pluriel du nombre de minute
  * @param number $minutes est un nombre de minute
  * @return string Retourne le singulier ou pluriel du nombre de minute(s)
  */
  public static function viewMinutes($minutes){
    if($minutes > 1) {
      return $minutes. " minutes";
    } else {
      return "une minute";
    }
  }

  /**
  * Permet d'avoir le singulier ou pluriel du nombre de seconde
  * @param number $seconds est un nombre de seconde
  * @return string Retourne le singulier ou pluriel du nombre de seconde(s) ou Maintenant
  */
  public static function viewSeconds($seconds){
    if($seconds > 1) {
       return $seconds. " secondes";
    } else if($seconds == 1) {
       return "une seconde";
    } else {
       return "Maintenant";
    }
  }

  /**
  * Permet d'avoir une différence de temps entre 2 dates
  * @param string $creer est une date de création au format sql ( ex : 2016-01-31 00:00:00 )
  * @param string $modifier est une date de modification au format sql ( ex : 2016-01-31 00:00:00 )
  * @return string Retourne une différence de temps entre modifier - creer
  */
  public static function getResolve($creer, $modifier){
    $dateDebut = new DateTime($creer);
    $dateFin = new DateTime($modifier);
    $date = clone $dateDebut;
    $dateInter = $date->diff($dateFin);
    $secondes = $dateInter->s ;
    $minutes = $dateInter->i ;
    $heures = $dateInter->h ;
    $jours = $dateInter->d ;
    $mois = $dateInter->m ;
    $ans = $dateInter->y ;

    if($ans > 0) {
      if($mois > 0) {
        if($jours > 0) {
          $temps = DateHeure::viewYear($ans)." ".$mois." mois ".DateHeure::viewDays($jours) ; 
        } else {
          $temps = DateHeure::viewYear($ans)." ".$mois." mois "; 
        }
      } 
      if($jours > 0) {
          $temps = DateHeure::viewYear($ans)." ".DateHeure::viewDays($jours) ; 
      } else {
          $temps = DateHeure::viewYear($ans); 
      }
    } else {
      if($mois > 0) {
        if($jours > 0) {
          $temps = $mois." mois ".DateHeure::viewDays($jours) ; 
        } else {
          $temps = $mois." mois "; 
        }
      } else {
        if($jours > 0) {
          $temps = DateHeure::viewDays($jours) ; 
        } else {
          if($heures > 0) {
            if($minutes > 0) {
              if($secondes > 0) {
                $temps = DateHeure::viewHours($heures)." ".DateHeure::viewMinutes($minutes)." ".DateHeure::viewSeconds($secondes);
              } else {
                $temps = DateHeure::viewHours($heures)." ".DateHeure::viewMinutes($minutes);
              }
            } else {
              if($secondes > 0) {
                $temps = DateHeure::viewHours($heures)." ".DateHeure::viewSeconds($secondes);
              } else {
                $temps = DateHeure::viewHours($heures);
              }
            }
          } else {
            if($minutes > 0) {
              if($secondes > 0) {
                $temps = DateHeure::viewMinutes($minutes)." ".DateHeure::viewSeconds($secondes);
              } else {
                $temps = DateHeure::viewMinutes($minutes);
              }
            } else {
              if($secondes > 0) {
                $temps = DateHeure::viewMinutes($minutes)." ".DateHeure::viewSeconds($secondes);
              } else {
                $temps = DateHeure::viewMinutes($minutes);
              }
            }
          }
        }
      } 
    }
    return $temps;
  }

  /**
  * Permet d'avoir une différence de temps en texte comme sur Facebook
  * @param string $date est une date au format sql ( ex : 2016-01-31 00:00:00 )
  * @return string Retourne une différence de temps entre la date et le temps actuel
  */
  public static function formatDate($date){
    $datetime = new Datetime($date);
    $dateNow = new DateTime('NOW');
    $dateFin = clone $datetime;
    $date = clone $dateNow;
    $dateInter = $date->diff($dateFin);
    $secondes = $dateInter->s ;
    $minutes = $dateInter->i ;
    $heures = $dateInter->h ;
    $jours = $dateInter->d ;
    $mois = $dateInter->m ;
    $ans = $dateInter->y ;

    if($ans > 0) {
      $temps = DateHeure::getMonthYear($datetime) .", ".DateHeure::getTimes($datetime); 
    } else if($mois > 0) {
      $temps = "le ".DateHeure::getMonthDay($datetime).", à ".DateHeure::getTimes($datetime);
    } else if($jours > 0) {
      if($jours == 1) {
        $temps = "Hier, à ".DateHeure::getTimes($datetime);
      } else if($jours == 2) {
        $calc = clone $dateFin;
        $calc->modify('-2 days');
        $temps = DateHeure::getDay($calc).", à ".DateHeure::getTimes($datetime);
      } else if($jours == 3) {
        $calc = clone $dateFin;
        $calc->modify('-3 days');
        $temps = DateHeure::getDay($calc).", à ".DateHeure::getTimes($datetime);
      } else {
        $temps = "le ".  DateHeure::getMonthDay($datetime).", à ".DateHeure::getTimes($datetime);   
      }
    } else if($heures > 0) {
      if($heures <= 1) {
        $temps = "il y a une heure";
      } else {
        $temps = "il y a ".$heures." heures";   
      }
    } else if($minutes > 0) {
      if($minutes <= 1) {
        $temps = "il y a une minute";
      } else {
        $temps = "il y a ".$minutes." minutes";
      }
    } else if($secondes > 1) {
      $temps = "il y a quelques secondes";
    } else if($secondes == 1) {
      $temps = "il y a une seconde";
    } else {
      $temps = "Maintenant";
    }

    return $temps; 

  }

}
