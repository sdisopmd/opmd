<?php
/**
 * \brief : Système de gestion pour faire des logs
 * \author Denis Sanson
 * \copyright Multixvers
 * \version PHP 7.0+
 */

class Log {
	
	/*
		N = log une NOTIF
		I = log une INFO
		D = log un DEBUG
    O = log un OBJET
		W = log un AVERTISSEMENT
		E = log une ERREUR
    T = log un TEXTE
    H = log un contenu d'une page HTML
    Ajouter 2 constante dans le fichier init.php ou point d'entré de l'application
    la première constante et LOG ( true | false ) pour mettre en place le logage
    type de log [0:aucun,1:erreur,2:warning,3:notif,4:info,5:debug,6:traçage,7:objet]
	*/
  const DIR_LOG = DEBOGS.AN."/".MOIS."/";
  const FILE_LOG_NOTIF = "notifs.log";
  const FILE_LOG_INFO = "infos.log";
  const FILE_LOG_DEBUG = "debug.log";
  const FILE_LOG_WARNING = "warning.log";
  const FILE_LOG_ERROR = "errors.log";
  

  /**
   * Permet de changé les séparateur de chemin en fonction de OS
   * @param string $chem c'est le chemin complet que l'on veut adapte à OS
   */
  public static function rep($chem){
    $fs = new FileSystem();
    return $fs->os($chem);
  }
  
  /**
   * Permet de créer la structure de dossier AAAA / MM dans LOGS
   * @param string $chem c'est le chemin complet du dossier
   */
  public static function createDir($chem){
    $fs = new FileSystem();
    $fs->mkdir($chem);
  }
  
  /**
   * Permet d'inscrire une date formaté pour les Log
   * @return string Retourne une date au format JJ-MM-AAA à HH:mm:ss ( ex : 12-31-2016 à 23:59:59 )
   */
	public static function DH(){
		$zone = new DateTimeZone('Europe/Paris');
		$d = new DateTime("now",$zone );
		return $d->format('d-m-Y à H:i:s');
	}
	
  /**
   * Log une Notification
   * @param string $texte Texte à loggé
   **/
	public static function N($texte){
    Log::createDir(Log::DIR_LOG);
		if(LOG == true){
      $date = Log::DH();
      $chem = Log::rep(Log::DIR_LOG.Log::FILE_LOG_NOTIF);
      $time = time();
      $log = $date." | NOTIF | ".$texte." | ".$time."\n";
      if(file_exists($chem)){
        file_put_contents($chem, $log, FILE_APPEND);
      } else {
        file_put_contents($chem, $log);
      }
		}
	}
	
  /**
   * Log une Information
   * @param string $texte Texte à loggé
   **/
	public static function I($texte){
    Log::createDir(Log::DIR_LOG);
		if(LOG == true){
      $date = Log::DH();
      $chem = Log::rep(Log::DIR_LOG.Log::FILE_LOG_INFO);
      $time = time();
      $log = $date." | INFO | ".$texte." | ".$time."\n";
      if(file_exists($chem)){
        file_put_contents($chem, $log, FILE_APPEND);
      } else {
        file_put_contents($chem, $log);
      }
		}
	}
	
  /**
   * Log un avertissement
   * @param string $texte Texte à loggé
   * @param string $dir Constante de Log sinon chemin complet vers le répertoire de log
   **/
	public static function W($texte,$dir = Log::DIR_LOG){
    Log::createDir(Log::DIR_LOG);
		if(LOG == true){
      $date = Log::DH();
      $chem = Log::rep(Log::DIR_LOG.Log::FILE_LOG_WARNING);
      $time = time();
      $log = $date." | WARNING | ".$texte." | ".$time."\n";
      if(file_exists($chem)){
        file_put_contents($chem, $log, FILE_APPEND);
      } else {
        file_put_contents($chem, $log);
      }
		}
	}
	
  /**
   * Log une Erreur
   * @param string $texte Texte à loggé
   **/
	public static function E($texte){
    Log::createDir(Log::DIR_LOG);
		if(LOG == true){
      $date = Log::DH();
      $chem = Log::rep(Log::DIR_LOG.Log::FILE_LOG_ERROR);
      $time = time();
      $log = $date." | ERREUR | ".$texte." | ".$time."\n";
      if(file_exists($chem)){
        file_put_contents($chem, $log, FILE_APPEND);
      } else {
        file_put_contents($chem, $log);
      }
		}
	}
	
  /**
   * Log un élément type tableau ou objet
   * @param string $debug Debug à loggé ou debug
   **/
	public static function D($debug){
    Log::createDir(Log::DIR_LOG);
		if(LOG == true){
      $date = Log::DH();
      $chem = Log::rep(Log::DIR_LOG.Log::FILE_LOG_DEBUG);
      $time = time();
      ob_start();
      print_r($debug);
      $data = ob_get_contents();
      $log = $date." | DEBUG | ".  $data ." | ".$time."\n";
      if(file_exists($chem)){
        file_put_contents($chem, $log, FILE_APPEND);
      } else {
        file_put_contents($chem, $log);
      }
		}
	}
  
  /**
   * Log un texte
   * @param string $texte Texte à loggé
   * */
  public static function T($texte) {
    Log::createDir(Log::DIR_LOG);
    if (LOG == true) {
      $date = Log::DH();
      $chem = Log::rep(Log::DIR_LOG .Log::FILE_LOG_DEBUG);
      $time = time();
      $log = $date . " | DEBUG | " . $texte . " | " . $time . "\n";
      if (file_exists($chem)) {
        file_put_contents($chem, $log, FILE_APPEND);
      } else {
        file_put_contents($chem, $log);
      }
    }
  }

  /**
   * Log un tracage
   * @param string $obj Objet a tracé
   * @param string $file est le nom du fichier de logs
   **/
	public static function O($obj, $file){
    Log::createDir(Log::DIR_LOG);
		if(LOG == true){
      $date = Log::DH();
      $chem = Log::rep(Log::DIR_LOG.$file);
      $time = time();
      ob_start(); 
      print_r($obj);
      $data = ob_get_contents();
      ob_end_clean(); 
      $log = $date." | TRACE | ".$data." | ".$time."##\n";
      if(file_exists($chem)){
        file_put_contents($chem, $log, FILE_APPEND);
      } else {
        file_put_contents($chem, $log);
      }
		}
	}
  
  /**
   * Log un contenu HTML
   * @param string $html Code HTML à loggé
   * @param string $file Le nom du fichier de logs
   * */
  public static function H($html, $file = Log::FILE_LOG_DEBUG) {
    Log::createDir(Log::DIR_LOG);
    if (LOG == true) {
      $date = Log::DH();
      $chem = Log::rep(Log::DIR_LOG . $file);
      $time = time();
      $log = $date . " | PAGE HTML | " . $html . " | " . $time . "\n";
      if (file_exists($chem)) {
        file_put_contents($chem, $log, FILE_APPEND);
      } else {
        file_put_contents($chem, $log);
      }
    }
  }

  /**
   * Cette fonction permet de formaté un tableau de log au format HTML
   * @param array $listeLogs Fournir un tableau retourné par Log::combine(), Log::toArray() ou Log::toObjet()
   * @param string $format (array | object) mettre à object si $listeLogs vient de  Log::toObjet()
   * @return html retourne le contenu des logs au format HTML ou null si erreur ou vide
   */
  public static function formatHTML($listeLogs, $format = "array") {
    $html = null;
    $tabColor = array("", "red", "orange", "cyan", "yellow", "greenyellow", "#e02bff");
    $tabIco = array("", "glyphicon-fire", "glyphicon-alert", "glyphicon-bell", "glyphicon-info-sign", "glyphicon-bullhorn", "glyphicon-flash");

    if (empty($listeLogs)) {
      return null;
    }

    if ($format == "array") {
      for ($i = 0; $i < count($listeLogs); $i++) {
        $ligne = $listeLogs[$i];
        $type = $ligne['type'];
        $num = Log::getType($type);
        $html .= "<b style='color:" . $tabColor[$num] . "'><span class='glyphicon " . $tabIco[$num] . "'></span> " . $type . " | ";
        if ($num == 6) {
          $html .= $ligne['date'] . " : <pre>" . $ligne['texte'] . "</pre></b><br>";
        } else {
          $html .= $ligne['date'] . " : " . $ligne['texte'] . "</b><br>";
        }
      }
    } else {
      foreach ($listeLogs as $ligne) {
        $type = $ligne->getType();
        $num = Log::getType($type);
        $html .= "<b style='color:" . $tabColor[$num] . "'><span class='glyphicon " . $tabIco[$num] . "'></span> " . $type . " | ";
        if ($num == 6) {
          $html .= $ligne->getDate() . " : <pre>" . $ligne->getTexte() . "</pre></b><br>";
        } else {
          $html .= $ligne->getDate() . " : " . $ligne->getTexte() . "</b><br>";
        }
      }
    }
    return $html;
  }

  /**
   * Permet de récupérer soit le numéro du type ou son nom
   * @param array $listeLogs Fournir un tableau retourné par Log::combine(), Log::toArray() ou Log::toObjet()
   * @param string $format (array | object) mettre à objet si $listeLogs vient de  Log::toObjet() serta aussi au format du retour
   * @param string $type soit le type de trie par( type | time )
   * @param string $order soit le type d'ordre de trie par( asc | desc )
   * @return array/object retourne le contenu du fichier sous forme de tableau contenant des Objest de Logs pour chaque ligne
   */
  public static function ordonner($listeLogs, $format = "array", $type = "type", $order = "asc") {

    if (empty($listeLogs)) {
      return null;
    }

    // preparation au trie
    foreach ($listeLogs as $key => $row) {
      if ($format == "array") {
        if ($type == "type") {
          $tri[$key] = Log::getType($row['type']);
        } else {
          $tri[$key] = $row['time'];
        }
      } else {
        if ($type == "type") {
          $tri[$key] = Log::getType($row->getType());
        } else {
          $tri[$key] = $row->getTime();
        }
      }
    }

    // trie le tableau sur le ( time | type ) (SORT_ASC | SORT_DESC)
    if ($order == "asc") {
      array_multisort($tri, SORT_ASC, $listeLogs);
    } else {
      array_multisort($tri, SORT_DESC, $listeLogs);
    }

    return $listeLogs;
  }

  /**
   * Permet de récupérer soit le numéro du type ou son nom
   * @param mixed $type soit le numéro du type ou son nom
   * @return string/int retourne un texte si on passe un nombre et retourne un nombre si on passe un texte
   */
  public static function getType($type) {
    $tabNum = array("AUCUN" => 0, "ERREUR" => 1, "WARNING" => 2, "NOTIF" => 3, "INFO" => 4, "DEBUG" => 5, "ERREUR_PHP" => 6, "TRACE" => 7, "OBJET" => 8);
    $tabTxt = array("AUCUN", "ERREUR", "WARNING", "NOTIF", "INFO", "DEBUG", "ERREUR_PHP", "TRACE", "OBJET");
    if (is_int($type)) {
      return $tabTxt[$type];
    } else {
      foreach ($tabNum as $k => $v) {
        if ($type == $k) {
          return $v;
        }
      }
    }
  }

  /**
   * Cette fonction permet de récupérer le contenu des fichiers de logs selon le type permetant de définir le niveau maximum soit le niveau 6 car le niveau 7 et 8 sont a part
   * @param number $type [0:aucun,1:erreur,2:warning,3:notif,4:info,5:debug,6:traçage,7:objet]
   * @param mixed $format soit (array | object) type du format de retour
   * @param int $filename si type 0 à 5 laissé null, si type 6 ou 7 alors précissé le nom du fichier avec extension
   * @return array/object retourne le contenu des logs au format indiqué soit tableau associatif soit Objet Logs le tout dans un tableau indéxé
   */
  public static function combine($type, $format = "object", $filename = null) {
    $flux = null;
    $file = null;
    $tabType = array(null, Log::FILE_LOG_ERROR, Log::FILE_LOG_WARNING, Log::FILE_LOG_NOTIF, Log::FILE_LOG_INFO, Log::FILE_LOG_DEBUG);
    $tabRetour = null;

    if ($type > 5) {
      if ($filename != null && !empty($filename)) {
        $file = Log::rep(Log::DIR_LOG . $filename);
        if (!file_exists($file)) {
          return null;
        } else {
          if (file_exists($file)) {
            $flux .= file_get_contents($file);
          }
          if ($flux != null) {
            $lignes = explode("##\n", $flux);
            if (!empty($lignes)) {
              foreach ($lignes as $l) {
                if (!empty($l)) {
                  $temp = explode("|", $l);
                  $date = $temp[0];
                  $type_msg = trim($temp[1]);
                  $msg = $temp[2];
                  $time = $temp[3];
                  if ($format == "objet") {
                    $elem = new Logs($type_msg, $date, $msg, $time, $filename, Log::DIR_LOG);
                  } else {
                    $elem = array("date" => $date, "type" => $type_msg, "texte" => $msg, "time" => $time);
                  }
                  $tabRetour[] = $elem;
                }
              }
            }
          }
        }
      }
    } else {

      for ($i = 1; $i <= $type; $i++) {
        $fichier = Log::rep(Log::DIR_LOG . $tabType[$i]);
        if (file_exists($fichier)) {
          $flux .= file_get_contents($fichier);
        }
      }

      if ($flux != null) {
        $lignes = explode("\n", $flux);
        if (!empty($lignes)) {
          foreach ($lignes as $l) {
            if (!empty($l)) {
              $temp = explode("|", $l);
              $date = $temp[0];
              $type_msg = trim($temp[1]);
              $msg = $temp[2];
              $time = $temp[3];
              if ($format == "object") {
                $elem = new Logs($type_msg, $date, $msg, $time, $filename, Log::DIR_LOG);
              } else {
                $elem = array("date" => $date, "type" => $type_msg, "texte" => $msg, "time" => $time);
              }
              $tabRetour[] = $elem;
            }
          }
        }
      }
    }

    return $tabRetour;
  }

  /**
   * Cette fonction permet de récupérer le contenu d'un fichier de logs au format tableau associatif 
   * @param number $type [0:aucun,1:erreur,2:warning,3:notif,4:info,5:debug,6:traçage,7:objet]
   * @param int $filename si type 0 à 5 laissé null, si type 6 ou 7 alors précissé le nom du fichier avec extension
   * @return array/object retourne le contenu du fichier sous forme de tableau indéxé contenant un tableau associatif pour chaque ligne
   */
  public static function toArray($type, $filename = null) {
    $flux = null;
    $tabLogs = null;
    $tabType = array(null, Log::FILE_LOG_ERROR, Log::FILE_LOG_WARNING, Log::FILE_LOG_NOTIF, Log::FILE_LOG_INFO, Log::FILE_LOG_DEBUG);
    $filename = ($filename == null) ? $tabType[$type] : $filename;
    $fichier = Log::rep(Log::DIR_LOG . $filename);

    if ($type > 5) {
      if ($filename != null && !empty($filename)) {
        $file = Log::rep(Log::DIR_LOG . $filename);
        if (!file_exists($file)) {
          return null;
        } else {
          if (file_exists($file)) {
            $flux .= file_get_contents($file);
          }
          if ($flux != null) {
            $lignes = explode("##\n", $flux);
            if (!empty($lignes)) {
              foreach ($lignes as $l) {
                if (!empty($l)) {
                  $temp = explode("|", $l);
                  $date = $temp[0];
                  $type_msg = trim($temp[1]);
                  $msg = $temp[2];
                  $time = $temp[3];
                  $message = array("date" => $date, "type" => $type_msg, "texte" => $msg, "time" => $time);
                  $tabLogs[] = $message;
                }
              }
            }
          }
        }
      }
    } else {

      if (file_exists($fichier)) {
        $flux .= file_get_contents($fichier);
      }

      if ($flux != null) {
        $lignes = explode("\n", $flux);
        if (!empty($lignes)) {
          foreach ($lignes as $l) {
            if (!empty($l)) {
              $temp = explode("|", $l);
              $date = $temp[0];
              $type_msg = trim($temp[1]);
              $msg = $temp[2];
              $time = $temp[3];
              $message = array("date" => $date, "type" => $type_msg, "texte" => $msg, "time" => $time);
              $tabLogs[] = $message;
            }
          }
        }
      }
    }
    return $tabLogs;
  }

  /**
   * Cette fonction permet de récupérer le contenu d'un fichier de logs au format Objet de Logs 
   * @param number $type [0:aucun,1:erreur,2:warning,3:notif,4:info,5:debug,6:traçage,7:objet]
   * @param int $filename si type 0 à 5 laissé null, si type 6 ou 7 alors précissé le nom du fichier avec extension
   * @return array/object retourne le contenu du fichier sous forme de tableau contenant des Objet de Logs pour chaque ligne
   */
  public static function toObject($type, $filename = null) {
    $flux = null;
    $tabLogs = null;
    $tabType = array(null, Log::FILE_LOG_ERROR, Log::FILE_LOG_WARNING, Log::FILE_LOG_NOTIF, Log::FILE_LOG_INFO, Log::FILE_LOG_DEBUG);
    $filename = ($filename == null) ? $tabType[$type] : $filename;
    $fichier = Log::rep(Log::DIR_LOG . $filename);

    if ($type > 6) {
      if ($filename != null && !empty($filename)) {
        $file = Log::rep(Log::DIR_LOG . $filename);
        if (!file_exists($file)) {
          return null;
        } else {
          if (file_exists($file)) {
            $flux .= file_get_contents($file);
          }
          if ($flux != null) {
            $lignes = explode("##\n", $flux);
            if (!empty($lignes)) {
              foreach ($lignes as $l) {
                if (!empty($l)) {
                  $temp = explode("|", $l);
                  $date = $temp[0];
                  $type_msg = trim($temp[1]);
                  $msg = $temp[2];
                  $time = $temp[3];
                  $log = new Logs($type_msg, $date, $msg, $time, $filename, Log::DIR_LOG);
                  $tabLogs[] = $log;
                }
              }
            }
          }
        }
      }
    } else {

      if (file_exists($fichier)) {
        $flux .= file_get_contents($fichier);
      }

      if ($flux != null) {
        $lignes = explode("\n", $flux);
        if (!empty($lignes)) {
          foreach ($lignes as $l) {
            if (!empty($l)) {
              $temp = explode("|", $l);
              $date = $temp[0];
              $type_msg = trim($temp[1]);
              $msg = $temp[2];
              $time = $temp[3];
              $log = new Logs($type_msg, $date, $msg, $time, $filename, Log::DIR_LOG);
              $tabLogs[] = $log;
            }
          }
        }
      }
    }
    return $tabLogs;
  }
  
  /**
   * Cette fonction permet de récupérer le contenu du fichier de log d'erreur php au format brut 
   */
  public static function recupPhpErrors() {
    $flux = null;
    $html = "";
    $tab = null;
    $fichier = FILE_LOG_PHP;

    if (file_exists($fichier)) {
      $flux = file_get_contents($fichier);
    }

    if ($flux != null) {
      $lignes = explode("\n", $flux);
      if (!empty($lignes)) {
        
        foreach ($lignes as $l) {
          if (!empty($l)) {
            $html .= "<p style='color:greenyellow'><b>". $l."</b></p>";
          }
        }
      }
    }

    return $html;
  }

}

