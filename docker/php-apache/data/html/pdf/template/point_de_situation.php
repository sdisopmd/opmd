<?php
require_once('../../init.php');

if ( !isset($_SESSION['users']) ){
	$serveur = "Location: http://".$_SERVER['SERVER_NAME'];
	header($serveur);
} else {

	if ( isset($_POST['id']) && $_POST['id'] != -1 ) {
		// cas ou on affiche une demande
		$id = $_POST['id'];
		$s = new Situation($id);
		$date = $s->getCreer();
		$centre = new Centre($s->getCentre());
		$data = $s->getData();
		$inters = $data->getInters();
		$moyens = $data->getMoyens();
		$personnes = $data->getPersonnes();
		$animaux = $data->getAnnimaux();
		$sites = $data->getSites();
		$nb_it_actu = ( isset($inters[0]) ) ? $inters[0] : "";
		$nb_it_diff = ( isset($inters[1]) ) ? $inters[1] : "";
		$nb_it_ann = ( isset($inters[2]) ) ? $inters[2] : "";
		$nb_it_ter = ( isset($inters[3]) ) ? $inters[3] : "";
		$off = ( isset($moyens[0]) ) ? $moyens[0] : "";
		$soff = ( isset($moyens[1]) ) ? $moyens[1] : "";
		$hdr = ( isset($moyens[2]) ) ? $moyens[2] : "";
		$sssm = ( isset($moyens[3]) ) ? $moyens[3] : "";
		$p_sauv = ( isset($personnes[0]) ) ? $personnes[0] : "";
		$p_secu = ( isset($personnes[1]) ) ? $personnes[1] : "";
		$p_prec = ( isset($personnes[2]) ) ? $personnes[2] : "";
		$a_sauv = ( isset($animaux[0]) ) ? $animaux[0] : "";
		$a_secu = ( isset($animaux[1]) ) ? $animaux[1] : "";
		$a_prec = ( isset($animaux[2]) ) ? $animaux[2] : "";
		$vehicules = "";

?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<title>POINT DE SITUATION</title>
		<style>
			html{font-size:10px;-webkit-tap-highlight-color:rgba(0,0,0,0)}
			body{font-family:"Helvetica Neue",Helvetica,Arial,sans-serif;font-size:14px;line-height:1.42857143;color:#333;background-color:#fff}
			h1,h2,h3,h4,h5,h6{font-family:inherit;font-weight:500;line-height:1.1;color:inherit}h1,h2,h3{margin-top:20px;margin-bottom:10px}
			h4,h5,h6{margin-top:10px;margin-bottom:10px}h1{font-size:26px}h2{font-size:24px}h3{font-size:18px}h4{font-size:16px}h5{font-size:14px}h6{font-size:12px}
			p{margin:5px 0px 0px 0px}.container{width:100%;}table th, table td{padding: 5px;}.row{display:inline-block;width:100%;clear:both;}
			.col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12 {float: left;}
			.col-sm-12 {width: 100%;}.col-sm-11 {width: 91.66666667%;}.col-sm-10 {width: 83.33333333%;}.col-sm-9 {width: 75%;}
			.col-sm-8 {width: 66.66666667%;}.col-sm-7 {width: 58.33333333%;}.col-sm-6 {width: 50%;}.col-sm-5 {width: 41.66666667%;}
			.col-sm-4 {width: 33.33333333%;}.col-sm-3 {width: 25%;}.col-sm-2 {width: 16.66666667%;}.col-sm-1 {width: 8.33333333%;}
			.col-sm-offset-12 {margin-left: 100%;}.col-sm-offset-11 {margin-left: 91.66666667%;}.col-sm-offset-10 {margin-left: 83.33333333%;}
			.col-sm-offset-9 {margin-left: 75%;}.col-sm-offset-8 {margin-left: 66.66666667%;}.col-sm-offset-7 {margin-left: 58.33333333%;}
			.col-sm-offset-6 { margin-left: 50%;}.col-sm-offset-5 {margin-left: 41.66666667%;}.col-sm-offset-4 {margin-left: 33.33333333%;}
			.col-sm-offset-3 {margin-left: 25%;}.col-sm-offset-2 {margin-left: 16.66666667%;}.col-sm-offset-1 {margin-left: 8.33333333%;}
			/*.table{width:100%;max-width:100%;margin-bottom:20px;background-color:transparent;border-spacing:0;border-collapse:collapse}
			.table th, .table td{padding: 10px;}.tables{width:100%;border-spacing:0;}.tables th, .tables td{padding: 1px;width:50%;} */
			.table-bordered{border:1px solid black;} tr{border:1px solid black;} th, td{border:1px solid black;}
			body{margin: 0px;padding:0px;overflow-x: hidden;}td{text-align:center;}
			.bg-danger {background-color: #f2dede;}.bg-info {background-color: #d9edf7;}
		</style>

	</head>

	<body id="fondSite">

		<!-- titre -->
		<div class="row" style="margin-top:10px;margin-bottom:10px;">
			<div class="col-sm-2">
				<img src="<?= ImageBase64::LOGO; ?>" alt="Logo" style="width:100px;"/>
			</div>
			<div class="col-sm-6 col-sm-offset-4" style="text-align:right">
				<h3>
					N° <b><?= $id; ?></b> FAIT le <b><?= $date; ?></b>
				</h3>
			</div>
		</div>
		<!-- /titre -->

		<br style="clear:both"><br>

		<div class="row">
			<div class="col-sm-12" style="text-align:center">
				<h1 class="text-center">POINT DE SITUATION</h1>
				<p style="font-size:20px">Centre de : <b><?= $centre->getLibelle() ; ?></b></p>
			</div>
		</div>

		<br style="clear:both"><br>

		<!-- interventions -->
		<div class="row">
			<div class="col-sm-12">
				<table class="table table-bordered" style="width:100%">
					<thead>
						<tr class="bg-danger">
							<th class="text-center" colspan="4">INTERVENTIONS</th>
						</tr>
					
						<tr class="bg-info">
							<th class="text-center">En cours</th>
							<th class="text-center">Différé</th>
							<th class="text-center">Annulées</th>
							<th class="text-center">Terminées</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td class="text-center inter-actu"><?= $nb_it_actu ?></td>
							<td class="text-center inter-diff"><?= $nb_it_diff ?></td>
							<td class="text-center inter-annul"><?= $nb_it_ann ?></td>
							<td class="text-center inter-term"><?= $nb_it_ter ?></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<!-- /interventions -->

		<br style="clear:both"><br>

		<!-- MOYENS MOBILISES -->
		<div class="row">
			<div class="col-sm-12">
				
				<table class="table table-bordered" style="width:100%;">
					<thead>
						
						<tr class="bg-danger">
							<th class="text-center" colspan="5">MOYENS MOBILISES</th>
						</tr>
						<tr class="bg-info">
							<th class="text-center">OFFICIERS</th>
							<th class="text-center">SOUS OFFICIERS</th>
							<th class="text-center">CAPORAUX / SAPEURS</th>
							<th class="text-center">SSSM</th>
							<th class="text-center">Véhicules(type & nombre)</th>
						</tr>
					</thead>

					<tbody>

						<tr>
							<td class="text-center off"><b><?= $off; ?></b></td>
							<td class="text-center soff"><b><?= $soff; ?></b></td>
							<td class="text-center hdr"><b><?= $hdr; ?></b></td>
							<td class="text-center"><b><?= $sssm ?></b></td>
							<td class="text-center"><b><?= $vehicules ?></b></td>
						</tr>

					</tbody>

				</table>

			</div>
		</div>
		<!-- / MOYENS MOBILISES -->

		<br style="clear:both"><br>

		<!-- INTERVENTIONS AU PROFIT DE PERSONNE -->
		<div class="row">
			<div class="col-sm-6">			
				<table class="table table-bordered" style="width:100%;">
					<thead>						
						<tr class="bg-danger">
							<th class="text-center" colspan="2">INTERVENTIONS AU PROFIT DE PERSONNE</th>
						</tr>
						<tr class="bg-info">
							<th class="text-center">Sauvetage(s)</th>
							<th class="text-center">Mise(s) en sécurité</th>
						</tr>
					</thead>

					<tbody>
						<tr>
							<td class="text-center"><?= $p_sauv ?></td>
							<td class="text-center"><?= $p_secu ?></td>
						</tr>						
						<tr class="text-center">
							<td colspan="2"><b>A préciser</b></td>
						</tr>
						<tr class="text-center">
							<td colspan="2"><?= $p_prec ?></td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="col-sm-6">
				<table class="table table-bordered" style="width:100%;">
					<thead>
						<tr class="bg-danger">
							<th class="text-center" colspan="2">INTERVENTION AU PROFIT D'ANNIMAUX</th>
						</tr>
						<tr class="bg-info">
							<th class="text-center">Sauvetage(s)</th>
							<th class="text-center">Mise(s) en sécurité</th>
							
						</tr>
					</thead>

					<tbody>
						<tr>
							<td class="text-center"><?= $a_sauv ?></td>
							<td class="text-center"><?= $a_secu ?></td>
						</tr>
						<tr class="text-center">
							<td colspan="2"><b>A préciser (chiens/chevaux...)</b></td>
						</tr>
						<tr class="text-center">
							<td colspan="2"><?= $a_prec ?></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>

		<!-- /INTERVENTIONS AU PROFIT DE PERSONNE -->

		<br style="clear:both"><br>

		<!-- INTERVENTIONS AU PROFIT DE SITES SENSIBLES: ERP / ENTREPRISE IMPORTANTE / ER .... -->
		<div class="row">
			<div class="col-sm-12">
				<table class="table table-bordered" style="width:100%;">
					<thead>		
						<tr class="bg-danger">
							<th class="text-center" colspan="3">INTERVENTIONS AU PROFIT DE SITES SENSIBLES: ERP / ENTREPRISE IMPORTANTE / ER </th>
						</tr>
						<tr class="bg-info">
							<th class="text-center">Nom de l'établissement / Nature</th>
							<th class="text-center">Dégâts particuliers</th>
							<th class="text-center">Chômage technique (NB)</th>
						</tr>
					</thead>
					<tbody id="ligne">
						<?php if($sites == null) : ?>
						<tr>
							<td class="text-center"></td>
							<td class="text-center"></td>
							<td class="text-center"></td>
						</tr>
						<?php else : ?>
							<?php foreach($sites as $s) : ?>
							<tr>
								<td class="text-center"><?= ( isset($s[0]) ) ? $s[0] : "" ?></td>
								<td class="text-center"><?= ( isset($s[1]) ) ? $s[1] : "" ?></td>
								<td class="text-center"><?= ( isset($s[2]) ) ? $s[2] : "" ?></td>
							</tr>
							<?php endforeach; ?>
						<?php endif; ?>
					</tbody>
				</table>
			</div>
		</div>

	</body>

</html>

<?php
	}
}
?>