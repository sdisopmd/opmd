<div id="menus">
	<nav class="navbar navbar-default" role="navigation" id="myMenu">
	  <div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
		  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		  </button>
		  <a class="navbar-brand" href="/"><img src="<?= IMG; ?>/pucelle.png" alt="Logo" width="41px" height="45px" style="margin-top:-10px"></a>
		</div>
	
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="menu">
			
		  <ul class="nav navbar-nav" role="menu">
			<?php if(User::isAuth()->getId() < 3) : ?>
			<li id="gestion-admin"><a href="#"><b>ADMINISTRATION</b></a></li>
			<?php endif; ?>
			<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><b>Gérer</b> <b class="caret"></b></a>
				<ul class="dropdown-menu">
					<li id="gestion-vehicules"><a href="#"><b>Véhicules</b></a></li>
					<li id="gestion-materiels"><a href="#"><b>Matériels</b></a></li>
					<li id="gestion-centres"><a href="#"><b>Centres</b></a></li>
					<li id="gestion-groupes"><a href="#"><b>Groupes</b></a></li>
					<li id="gestion-communes"><a href="#"><b>Commune</b></a></li>
					<li id="gestion-natures"><a href="#"><b>Natures</b></a></li>
					<li id="gestion-precisions"><a href="#"><b>Précision</b></a></li>
					<li id="gestion-affectation"><a href="#"><b>Feuille de garde</b></a></li>
				</ul>
			</li>
			<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><b>Intervention</b> <b class="caret"></b></a>
				<ul class="dropdown-menu">
					<li id="gestion-consulter"><a href="#"><b>Numéro</b></a></li>
				</ul>
			</li>
			<li id="gestion-permanence"><a href="#"><b>Permanence</b></a></li>
			<li id="gestion-consignes"><a href="#"><b>Consignes</b></a></li>
			<li><a href="#" onclick="window.open('<?= PAGES; ?>/point_de_situation.php');"><b>Point de situation</b></a></li>
			<li><a href="#" onclick="window.open('<?= PAGES; ?>/feuille_recapitulatif.php');"><b>Récapitulatif</b></a></li>
			<li id="modal-depart"><a href="#" /><b style="color:red">Feuille de départ</b></a></li>
			<li id="modal-search-codis"><a href="#" /><b style="color:green"><span class="glyphicon glyphicon-search"></span> N° Codis</b></a></li>
		  </ul>
		  <ul class="nav navbar-nav pull-right" role="menu">
			<?php if(User::isLogin() != null) : ?>
			<li><a href="#" id="deco" data="<?=  User::isLogin()->getId();?>"><b><?= User::isAuth()->getNom()." : ".User::isLogin()->getNomFormat(); ?></b><b class="badge-success" style="margin-left:10px"> Déconnexion</b></a></li>
			<?php endif; ?>
			<li><a href="#"><b id="horloge">12h00:00</b></a></li>
		  </ul>
		</div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>
	
</div>