<?php


 //modal pour feuille de départ 
function creerModal($idModal = "modal", $titre = "", $idBody = "", $fichier = "", $classM = "",$body = ""){

	echo "<div class='modal fade' id='{$idModal}' tabindex='-1' role='dialog' aria-labelledby='dial-{$idModal}' aria-hidden='true'>";
	  echo "<div class='modal-dialog modal-lg'>";
	    echo "<div class='modal-content' style='border-radius:20px;'>";
	      echo "<div class='modal-header {$classM}' style='border-radius:20px;'>";
	        echo "<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>";
	        echo "<h1 class='modal-title text-center' id='dial-{$idModal}'>{$titre}</h1>";
	      echo "</div>";
	      echo "<div class='modal-body'>";
	      	
				if($fichier == false){
					echo $body;
				} else {
					echo "<div id='{$idBody}'>";
	      			 		include_once($fichier); 
					echo "</div>";
				}
	
	      echo "</div>";
	    echo "</div>";
	  echo "</div>";
	echo "</div>";
	
}

?>
