
<html lang="fr">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>AUTHENTIFICATION</title>
		
		<?php
			$stylesLibs = array(
				BOOTSTRAP_CSS.DS."bootstrap.css",
				BOOTSTRAP_CSS.DS."bootstrap-multixvers.css",
				GRITTER_CSS.DS."gritter.css"
			);
			
			$stylesApplis = array(
				CSS.DS."multixvers.css"
			);
		?>


		<?php foreach($stylesLibs as $lib) : ?>
			<link rel="stylesheet" type="text/css" href="<?= URL.$lib; ?>"/>
		<?php endforeach; ?>

		<?php foreach($stylesApplis as $app) : ?>
			<link rel="stylesheet" type="text/css" href="<?= URL.$app; ?>"/>
		<?php endforeach; ?>
		
		<link rel="shortcut icon" href="favicon.ico">
		<script type="text/javascript" src="<?= URL.JQUERY.DS."jquery-2.1.0.js"; ?>" ></script>
	</head>
	
	<body id="page-login">
	
	
	<div id="bloc-auth">
	
		<div id="auth">
		
		<div id="titre-login" class="bg-success">
			<h1 class="text-center">FORMULAIRE D'AUTHENTIFICATION</h1>
		</div>
		
		
		<form class="form-horizontal" role="form" id="form-login">
		
		  <div class="form-group">
			<label for="login" class="col-sm-offset-1 col-sm-3 control-label">UTILISATEUR</label>
			<div class="col-sm-6">
			  <input type="text" name="login" maxlength="50" class="form-control">
			</div>
		  </div>
		  
		  <div class="form-group">
			<label for="pass" class="col-sm-offset-1 col-sm-3 control-label">MOT DE PASSE</label>
			<div class="col-sm-6">
				<input type="password" name="pass" maxlength="50" class="form-control">
			</div>
		  </div>
			  

		  <div class="form-group">
			<div class="col-sm-offset-4 col-sm-2">
			  <button type="button" class="btn btn-success" id="connect">Se connecter</button>
			</div>
		  </div>
		  
		</form>
		
		
		</div>
	
	</div>

	<div id="bloc-error"></div>
	
	<?php
		$scriptsLibs = array(
			BOOTSTRAP_JS.DS."bootstrap.js",
			GRITTER_JS.DS."gritter.js",
			LOADER.DS."spin.js"
		);
		
		$scriptsApplis = array(
			JS.DS."utils.js",
			JS.DS."notif.js",
			JS.DS."md5.js"
		);

	?>

	<?php foreach($scriptsLibs as $lib) : ?>
		<script type="text/javascript" src="<?= URL.$lib; ?>"></script>
	<?php endforeach; ?>

	<?php foreach($scriptsApplis as $app) : ?>
		<script type="text/javascript" src="<?= URL.$app; ?>"></script>
	<?php endforeach; ?>
	
	<script>

		$(document).ready(function(e){
			
			$('#connect').on('click', function(){
				var login = $('input[name=login]').val();
				var pass = md5($('input[name=pass]').val());
				var obj = new Object();
				obj.login = login;
				obj.pass = pass;
				$.post('/admin/requetes/user.php', obj).always(function(arg){
					if(arg.retour == "ok" ){
						location.reload();
					} else {
						notif(arg.retour,"danger","","");
					}
					msg(obj);
				});
			});
			
		});
		
	</script>
	
	</body>
	
	
	
</html>