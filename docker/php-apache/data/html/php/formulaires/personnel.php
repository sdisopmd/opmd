<?php
	require_once('../../init.php');
	
	function displayList() {
		echo "<div>";
		$pas = Personnel::getAllPersonnelAbsent();
		if($pas != null) {
			echo "<table class='table table-bordered table-striped table-hover'>";
			echo "<thead>";
				echo "<tr>
						<th>EDIT</th>
						<th>GRADE</th>
						<th>NOM</th>
						<th>PRENOM</th>
						<th>AJOUTER</th>
					</tr>";
			echo "</thead>";
			echo "<tbody>";
			foreach($pas as $pa) {
				echo "<tr>
						<td><img src='".IMG."/edit.png' /></td>
						<td>{$pa->getGrade()->getLibelle()}</td>
						<td>{$pa->getNom()}</td>
						<td>{$pa->getPrenom()}</td>
						<td><img src='".IMG."/in.png' /></td>
					</tr>";
			}
			echo "</tbody>";
			echo "</table>";
		}
		echo "</div>";
	}
?>

<script>
	$(function() {
		$("#open-personnel").load("/php/formulaires/modif-personnel.php");
	});
</script>

<?php
	
	displayList();

?>