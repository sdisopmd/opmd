<?php
	require_once('../../init.php'); 
	
	if(isset($_POST['id'])) {
		//formulaire validé
		$m = new Materiel($_POST['id']);
		if($_POST['del']) {
			$m->delete();
			print("ok");
		} else {
			$m->setLibelle(strtoupper($_POST['libelle']));
			$m->setCentre($_POST['centre']);
			$m->setVisible($_POST['visible']);
			$m->setGroupe($_POST['groupe']);
			$m->setEtat($_POST['etat']);
			$m->setQuantite($_POST['quantite']);
			$m->setUtiliser($_POST['utiliser']);
			$m->setDispo($_POST['dispo']);
			$m->commit();
		}
		exit;
	}
	
	$m = new Materiel(@$_GET['id']);
	

?>



	<form class="form-horizontal" role="form" id="form-materiel" method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>" onsubmit="return false;">
		
	  <div class="form-group">
	  	<div class="col-sm-12">
	  		<div id="error" />

				<input type="hidden" name="id" value="<?php echo $m->getId(); ?>" />
				<input type="hidden" name="del" />
				<input type="hidden" name="utiliser" value="<?php echo $m->getUtiliser(); ?>" />
	  	</div>
	  </div>
		
	  <div class="form-group">
	    <label for="libelle" class="col-sm-2 control-label">LIBELLÉ</label>
	    <div class="col-sm-10">
	      <input type="text" name="libelle" maxlength="50" class="form-control" value="<?php echo $m->getLibelle(); ?>">
	    </div>
	  </div>
	  
	  <div class="form-group">
	    <label for="centre" class="col-sm-2 control-label">CENTRE</label>
	    <div class="col-sm-10">
	      <select name="centre" class="form-control">
					<option value="-1"></option>
					<?php
						$cs = Centre::getListCentres();
						$id = $m->getCentre()->getId();
						if($cs != null) foreach($cs as $c) {
							if($c->getId() == $id){
								echo "<option value='{$c->getId()}' selected='selected'>{$c->getLibelle()}</option>";
							} else if($c->getId() == 1){
								echo "<option value='{$c->getId()}' selected='selected'>{$c->getLibelle()}</option>";
							}	else {
								echo "<option value='{$c->getId()}'>{$c->getLibelle()}</option>";
							}
						}
					?>
				</select>
	    </div>
	  </div>
		
		 <div class="form-group">
	    <label for="groupe" class="col-sm-2 control-label">GROUPE</label>
	    <div class="col-sm-10">
	      <select name="groupe" class="form-control">
	      	<option value="-1"></option>
					<?php
						$gr = Groupes::getAllGroupes();
						if($gr != null) foreach($gr as $g) {
							if($g->getId() == $m->getGroupe()->getId()){
								echo "<option value='{$g->getId()}' selected='selected'>{$g->getLibelle()}</option>";
							} else {
								echo "<option value='{$g->getId()}'>{$g->getLibelle()}</option>";
							}
						}
					?>
				</select>
	    </div>
	  </div>
		
		<div class="form-group">
	    <label for="quantite" class="col-sm-2 control-label">QUANTITE</label>
	    <div class="col-sm-10">
	      <input type="number" name="quantite" maxlength="50" class="form-control" value="<?php echo $m->getQuantite(); ?>">
	    </div>
	  </div>
	  
	  <div class="form-group">
	  	<label for="visible" class="col-sm-2 control-label">VISIBLE</label>
	  		<div class="col-sm-10">
			    <label class="radio-inline">
					  <input type="radio" name="visible" value="1" onclick="visibility(1);" <?= ($m->getVisible() == 1) ? "checked" : ""; ?>> OUI
					</label>
					<label class="radio-inline">
					  <input type="radio" name="visible" value="0" onclick="visibility(0);" <?= ($m->getVisible() == 0) ? "checked" : ""; ?>> NON
					</label>
				</div>
	  </div>
	  
	  	<div class="form-group">
			<label for="dispo" class="col-sm-2 control-label">DISPONIBILITE</label>
			<div class="col-sm-10">
				<label class="radio-inline">
					<input type="radio" name="dispo" value="5" onclick="disponibilite(5);" <?= ($m->getEtat()->getId() == 5) ? "checked" : ""; ?>> INDISPONIBLE
				</label>
				<label class="radio-inline">
					<input type="radio" name="dispo" value="1" onclick="disponibilite(1)" <?= ($m->getEtat()->getId() == 1) ? "checked" : ""; ?>> DEPART
				</label>
				<label class="radio-inline">
					<input type="radio" name="dispo" value="10" onclick="disponibilite(10)" <?= ($m->getEtat()->getId() == 10) ? "checked" : ""; ?>> RETOUR
				</label>
			</div>
		</div>
		
	  
<?php if(isset($_GET['mode']) && $_GET['mode'] == 'add') : ?>
	  
	  <div class="form-group">
	    <div class="col-sm-offset-2 col-sm-2">
	      <button type="submit" class="btn btn-info" data-dismiss="modal">Fermer</button>
	    </div>
	    <div class="col-sm-8">
	      <button type="submit" class="btn btn-success" id="addMateriel">Ajouter</button>
	    </div>
	  </div>
	  

	
<?php elseif(isset($_GET['mode']) && $_GET['mode'] == 'mod') : ?>
	
	<div class="form-group">
	    <div class="col-sm-offset-2 col-sm-2">
	      <button type="submit" class="btn btn-info" data-dismiss="modal">Fermer</button>
	    </div>
	    <div class="col-sm-2">
	      <button type="submit" class="btn btn-success" id="saveMateriel">Sauvegarder</button>
	    </div>
	    <div class="col-sm-6">
	      <button type="submit" class="btn btn-danger" id="delMateriel">Supprimer</button>
	    </div>
	  </div>
	
<?php endif; ?>

	</form>
	
	
	<script type="text/javascript" charset="utf-8">
	
	var voir = <?= ($m->getVisible()) ? $m->getVisible() : 1; ?>;
	var disponible = <?= $m->getEtat()->getId(); ?>;
	
	function visibility(val){
		voir = val;
	}
	
	function disponibilite(val){
		disponible = val;
	}
	
	function recupDatas(){

		var data = {
			id : $("input[name=id]").val(),
			libelle : $("input[name=libelle]").val(),
			centre : $("select[name=centre]").val(),
			groupe : $("select[name=groupe]").val(),
			visible : voir,
			etat : disponible,
			quantite : $("input[name=quantite]").val(),
			utiliser : $("input[name=utiliser]").val(),
			dispo : 0,
			del : $("input[name=del]").val()
		}
		var q = new Number(data.quantite);
		var u = new Number(data.utiliser);
		var d = q - u;
		data.dispo = (d <= 0 ) ? 0 : d;
		return data;
	}
	
	
	
	$("#addMateriel").click(function(){
	
		var data = recupDatas();
		var retour = valid_materiel($("#form-materiel"),data);
		if(retour){
			$.post("/php/formulaires/materiel.php",data)
				.always(function(arg) {
					var n = arg.search("Notice");
					if( n > 0 ) {
						arg = "";
					}

					if( trim(arg) != ""){
						viderModal();
						$("#bloc-error").append(arg);
						notif("Erreur lors de l'ajout du matériel","danger","","");
						
					} else {
						viderModal();
						updateIntelligent();
						notif("Votre matériel à bien été ajouter à la liste","success","","");
					}
					
				});
		}
	
	});
	
	$("#saveMateriel").click(function(){
		var data = recupDatas();
		
		var retour = valid_vehicule($("#form-materiel"),data);
		if(retour){
			$.post("/php/formulaires/materiel.php",data)
				.always(function(arg) {
					var n = arg.search("Notice");
					if( n > 0 ) {
						arg = "";
					}

					if( trim(arg) != ""){
						viderModal();
						$("#bloc-error").append(arg);
						notif("Erreur lors de la modification du matériel","danger","","");
						
					} else {
						viderModal();
						updateIntelligent();
						notif("Votre matériel à bien été modifié et mis à jour dans la liste","success","","");
					}
					
				});
		}
	});
	
	$("#delMateriel").click(function(){
		delMateriels($("input[name=id]").val(), "materiel");
	});
	
	
		
	</script>

