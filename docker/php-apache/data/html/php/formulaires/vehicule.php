<?php
	require_once('../../init.php');
	
	if(isset($_POST['id'])) {
		//formulaire validé
		$v = new Vehicule($_POST['id']);
		if($_POST['del']) {
			$v->delete();
			print("ok");
		} else {
			$v->setLibelle(strtoupper($_POST['libelle']));
			$v->setCentre($_POST['centre']);
			$v->setGroupe($_POST['groupe']);
			$v->setPostes($_POST['postes']);
			$v->setVisible($_POST['visible']);
			$v->setEtat($_POST['etat']);
			if($_POST['etat'] == 10 ){
				$v->setIntervention();
			}
			$v->commit();
		}
		exit;
	}
	
	$v = new Vehicule(@$_GET['id']);
	if($v->exists()) {
		$vp = $v->getPostes(true);
		$grs = $v->getGroupe();
		$ce = $v->getCentre();
	} else {
		$vp = PosteVehicule::getAllPostesObligatoires(true);
	}
	$vp[] = "0";

?>



	<form class="form-horizontal" role="form" id="form-vehicule" method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>" onsubmit="return false;">
		
	  <div class="form-group">
	  	<div class="col-sm-12">
	  		<div id="error" />

				<input type="hidden" name="id" value="<?php echo $v->getId(); ?>" />
				<input type="hidden" name="del" />
	  	</div>
	  </div>
		
	  <div class="form-group">
	    <label for="libelle" class="col-sm-2 control-label">LIBELLÉ</label>
	    <div class="col-sm-10">
	      <input type="text" name="libelle" maxlength="50" class="form-control" value="<?php echo $v->getLibelle(); ?>">
	    </div>
	  </div>
	  
	  <div class="form-group">
	    <label for="centre" class="col-sm-2 control-label">CENTRE</label>
	    <div class="col-sm-10">
	      <select name="centre" class="form-control">
					<option value="-1"></option>
					<?php
						$cs = Centre::getListCentres();
						if($cs != null) foreach($cs as $c) {
							if($c->getId() == $v->getCentre()->getId()){
								echo "<option value='{$c->getId()}' selected='selected'>{$c->getLibelle()}</option>";
							} else if ($c->getId() == 1) {
								echo "<option value='{$c->getId()}' selected='selected'>{$c->getLibelle()}</option>";
							}else {
								echo "<option value='{$c->getId()}'>{$c->getLibelle()}</option>";
							}
						}
					?>
				</select>
	    </div>
	  </div>
	  
	  <div class="form-group">
	    <label for="groupe" class="col-sm-2 control-label">GROUPE</label>
	    <div class="col-sm-10">
	      <select name="groupe" class="form-control">
	      	<option value="-1"></option>
					<?php
						$gr = Groupes::getAllGroupes();
						if($gr != null) foreach($gr as $g) {
							if($g->getId() == $v->getGroupe()->getId()){
								echo "<option value='{$g->getId()}' selected='selected'>{$g->getLibelle()}</option>";
							} else {
								echo "<option value='{$g->getId()}'>{$g->getLibelle()}</option>";
							}
						}
					?>
				</select>
	    </div>
	  </div>
	  
	  <div class="form-group">
	    <label for="postes[]" class="col-sm-2 control-label">FONCTIONS</label>
	    <div class="col-sm-10" id="poste">
	      <?php
					for($i=0 ; $i<count($vp) ; $i++) {
						$cur = $vp[$i];
						$onchange = ($cur == 0) ? "addSelect(this, 'poste')" : '';
				?>
				<select name="postes[]" onchange="<?php echo $onchange; ?>" class="form-control">
					<option value="-1"></option>
					<?php
						$pvs = PosteVehicule::getAllPostesVehicule();
						if($pvs != null) foreach($pvs as $pv) {
							$selected = ($cur == $pv->getId()) ? ' selected="selected"' : '';
							echo "<option value=\"{$pv->getId()}\"{$selected}>{$pv->getLibelle()}</option>";
						}
					?>
				</select>
				<?php
						if($i < count($vp)-1) {
							echo '<br />';
						}
					}
				?>
	    </div>
	  </div>
	  
	  <div class="form-group">
	  	<label for="visible" class="col-sm-2 control-label">VISIBLE</label>
	  		<div class="col-sm-10">
			    <label class="radio-inline">
					  <input type="radio" name="visible" value="1" onclick="visibility(1);" <?= ($v->getVisible() == 1) ? "checked" : ""; ?>> OUI
					</label>
					<label class="radio-inline">
					  <input type="radio" name="visible" value="0" onclick="visibility(0);" <?= ($v->getVisible() == 0) ? "checked" : ""; ?>> NON
					</label>
			</div>
	  </div>
	  
	<div class="form-group">
		<label for="dispo" class="col-sm-2 control-label">DISPONIBILITE</label>
		<div class="col-sm-10">
			<label class="radio-inline">
				<input type="radio" name="dispo" value="5" onclick="disponibilite(5);" <?= ($v->getEtat()->getId() == 5) ? "checked" : ""; ?>> INDISPONIBLE
			</label>
			<label class="radio-inline">
				<input type="radio" name="dispo" value="1" onclick="disponibilite(1)" <?= ($v->getEtat()->getId() == 1) ? "checked" : ""; ?>> DEPART
			</label>
			<label class="radio-inline">
				<input type="radio" name="dispo" value="3" onclick="disponibilite(3)" <?= ($v->getEtat()->getId() == 3) ? "checked" : ""; ?>> VL RECO
			</label>
			<label class="radio-inline">
				<input type="radio" name="dispo" value="10" onclick="disponibilite(10)" <?= ($v->getEtat()->getId() == 10) ? "checked" : ""; ?>> RETOUR
			</label>
		</div>
	</div>
	  
<?php if(isset($_GET['mode']) && $_GET['mode'] == 'add') : ?>
	  
	  <div class="form-group">
	    <div class="col-sm-offset-2 col-sm-2">
	      <button type="submit" class="btn btn-info" data-dismiss="modal">Fermer</button>
	    </div>
	    <div class="col-sm-8">
	      <button type="submit" class="btn btn-success" id="addVehicule">Ajouter</button>
	    </div>
	  </div>
	  

	
<?php elseif(isset($_GET['mode']) && $_GET['mode'] == 'mod') : ?>
	
	<div class="form-group">
	    <div class="col-sm-offset-2 col-sm-2">
	      <button type="submit" class="btn btn-info" data-dismiss="modal">Fermer</button>
	    </div>
	    <div class="col-sm-2">
	      <button type="submit" class="btn btn-success" id="saveVehicule">Sauvegarder</button>
	    </div>
	    <div class="col-sm-6">
	      <button type="submit" class="btn btn-danger" id="delVehicule">Supprimer</button>
	    </div>
	</div>
	
<?php endif; ?>

	</form>
	
	
	<script type="text/javascript" charset="utf-8">
	
	var voir = <?= ($v->getVisible()) ? $v->getVisible() : 1; ?>;
	var disponible = <?= $v->getEtat()->getId(); ?>;
	
	function visibility(val){
		voir = val;
	}
	
	function disponibilite(val){
		disponible = val;
	}
	
	function recupDatas(){
		var poste = new Array();
		var tabPostes = $('select[name="postes[]"]').each(function(){ $(this).val()});
		var i
		for( i = 0; i < tabPostes.length; i++ ){
			poste.push($(tabPostes[i]).val());
		}
		
		var data = {
			id : $("input[name=id]").val(),
			libelle : $("input[name=libelle]").val(),
			centre : $("select[name=centre]").val(),
			groupe : $("select[name=groupe]").val(),
			visible : voir,
			etat : disponible,
			del : $("input[name=del]").val(),
			postes : poste
		}
		
		return data;
	}
	
	$("#addVehicule").click(function(){
		
		var data = recupDatas();
		var retour = valid_vehicule($("#form-vehicule"),data);
		if(retour){
			$.post("/php/formulaires/vehicule.php",data)
				.always(function(arg) {
					var n = arg.search("Notice");
					if( n > 0 ) {
						arg = "";
					}

					if( trim(arg) != ""){
						viderModal();
						$("#bloc-error").append(arg);
						notif("Erreur lors de l'ajout du véhicule","danger","","");
						
					} else {
						viderModal();
						updateIntelligent();
						notif("Votre véhicule à bien été ajouter à la liste","success","","");
					}
					
				});
		}
	});
	
	$("#saveVehicule").click(function(){
		var data = recupDatas();
		var retour = valid_vehicule($("#form-vehicule"),data);
		if(retour){
			$.post("/php/formulaires/vehicule.php",data)
				.always(function(arg) {
					var n = arg.search("Notice");
					if( n > 0 ) {
						arg = "";
					}

					if( trim(arg) != ""){
						viderModal();
						$("#bloc-error").append(arg);
						notif("Erreur lors de la modification du véhicule","danger","","");
						
					} else {
						viderModal();
						updateIntelligent();
						notif("Votre véhicule à bien été modifié et mis à jour dans la liste","success","","");
					}
					
				});
		}
	});
	
	$("#delVehicule").click(function(){
		delVehicules($("input[name=id]").val(), "vehicule");
	});
	
	
		
	</script>










