<?php
require_once('../../init.php');

?>

<div id="rechercher">
    <form class="form-horizontal" role="form" name="search_situation" method="POST" enctype="multipart/form-data" action="">

        <div class="form-group">
            <label class="col-md-2 col-md-offset-3 control-label">VOTRE DATE : </label>
            <div class="col-md-2">
                <input class="form-control input-md" name="date" placeholder="JJ/MM/AAAA">
            </div>
            <label class="col-md-1 control-label">(JOUR/MOIS/ANNEE)</label>
        </div>

        <div class="col-md-12 text-center">
            <div id="errors"> </div>
        </div>

        <div class="form-group">
            <div class="row">
                <div class="col-md-12 text-center"><button type="button" id="search-point" class="btn btn-success">RECHERCHER</button></div>
            </div>
        </div>

    </form>
</div>

<script type="text/javascript">
		
    $(document).ready(function(e){
        $("#search-point").on('click',function(){
            $("#resultat").empty();
            var dates = $("input[name=date]").val();
            var n = dates.search("/");
            if(dates.length < 8 || n == -1){
                var html = "<h3 style='color:red' class='text-center'>Veuillez saisir une date au format JOUR(1-31) / MOIS(1-12) / ANNEE(2017 et +)</h3>";
                $("#resultat").append(html);
            }
            if(n != -1){
                var tmp = dates.split("/");
                if(tmp.length == 3){
                    var jour = tmp[0];
                    var mois = tmp[1];
                    var an = tmp[2];

                    if(jour > 0 && jour < 32){
                        if(mois > 0 && mois < 13){
                            if(an >= 2017){
                                jour = (jour < 9) ? "0"+jour : jour;
                                mois = (mois < 9) ? "0"+mois : mois;
                                dates = jour +"/" + mois + "/" + an;
                                $.post("/php/requetes/recherche_point_de_situation.php",{date : dates}).always(function(code){
                                    $("#resultat").append(code);
                                });
                            } else {
                                var html = "<h3 style='color:red' class='text-center'>L'année ne peut pas être plus petit que 2017</h3>";
                                $("#resultat").append(html);
                            }
                        } else {
                            var html = "<h3 style='color:red' class='text-center'>Le mois commence à 1 et ne peut pas être plus grand que 13</h3>";
                            $("#resultat").append(html);
                        }
                    } else {
                        var html = "<h3 style='color:red' class='text-center'>Le jour commence à 1 et ne peut pas être plus grand que 31</h3>";
                        $("#resultat").append(html);
                    }

                } else {
                    var html = "<h3 style='color:red' class='text-center'>Veuillez saisir une date au format JOUR(1-31) / MOIS(1-12) / ANNEE(2017 et +)</h3>";
                    $("#resultat").append(html);
                }
            }
        });
    });

</script>

<div id="resultat"></div>