<?php
	require_once('../../init.php');

	/* ajout/modification */
	if(isset($_POST['id'])) {
		$p = new Personnel($_POST['id']);
		if(isset($_POST['del'])){
			$p->delete();
			print("ok");
		} else {
			//formulaire validé
			$p = new Personnel($_POST['id']);
			$p->setGrade($_POST['grade']);
			$p->setNom($_POST['nom']);
			$p->setPrenom($_POST['prenom']);
			$p->setCentre($_POST['centre']);
			$p->commit();
			exit;
		}
	}
	
	
	
	/* arrivée en caserne */
	if(isset($_GET['in'])) {
		$p = new Personnel($_GET['in']);
		$p->arrive();
	}

	$p = new Personnel(@$_GET['id']);

	function displayList() {
		echo "<div>";
		$pas = Personnel::getAllPersonnelAbsent();
		if($pas != null) {
			echo "<table class='table table-bordered table-striped table-hover'>";
			echo "<thead>";
				echo "<tr>
						<th>ACTION</th>
						<th>GRADE</th>
						<th>NOM</th>
						<th>PRENOM</th>
						<th>CENTRE</th>
						<th>AJOUTER</th>
					</tr>";
			echo "</thead>";
			echo "<tbody>";
			foreach($pas as $pa) {
				echo "<tr>
						<td class='text-center'>
							<span onclick='editPersonnel({$pa->getId()});' style='margin-right:30px' ><img src='assets/img/edit.png' /></span> 
							<span onclick=\"deletePersonnel({$pa->getId()},'{$pa->getGrade()->getLibelle()} {$pa->getNom()} {$pa->getPrenom()}');\"><img src='assets/img/delete.gif' /></span>
						</td>
						<td id='grade'>{$pa->getGrade()->getLibelle()}</td>
						<td id='lastname'>{$pa->getNom()}</td>
						<td id='firstname'>{$pa->getPrenom()}</td>
						<td id='centre'>{$pa->getCentre()->getAbreviation()}</td>
						<td class='text-center'><button type='button' class='btn btn-success' onclick='insertPersonnel({$pa->getId()},\"{$pa->getGrade()->getLibelle()} {$pa->getNom()} {$pa->getPrenom()}\");'> AJOUTER &nbsp;&nbsp;<span  class='glyphicon glyphicon-user'></span></button></td>
					</tr>";
			}
			echo "</tbody>";
			echo "</table>";
		}
		echo "</div>";
	}
?>


<?php
	if(isset($_GET['id'])) {
?>
<form class="form-horizontal" role="form" id="form-personnel" method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>" onsubmit="return false;">
	
	<div class="form-group">
		<div class="col-sm-12">
			<div id="error"></div>
			<input type="hidden" name="id" value="<?php echo $p->getId(); ?>" />
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-sm-3">
			<h4 class="text-center text-info"><b>GRADE</b></h4>
		</div>
		<div class="col-sm-3">
			<h4 class="text-center text-info"><b>NOM</b></h4>
		</div>
		<div class="col-sm-3">
			<h4 class="text-center text-info"><b>PRENOM</b></h4>
		</div>
		<div class="col-sm-3">
			<h4 class="text-center text-info"><b>CENTRE</b></h4>
		</div>
	</div>
	
	
	<div class="form-group">
		<div class="col-sm-3">
			<select name="grade" class="form-control">
			<?php
				$gs = Grade::getAllGrades();
				if($gs != null) foreach($gs as $g) {
					$selected = ($p->getGrade(true) == $g->getId()) ? ' selected="selected"' : '';
					echo "<option value=\"{$g->getId()}\"{$selected}>{$g->getLibelle()}</option>\n";
				}
			?>
			</select>
		</div>
		
		<div class="col-sm-3">
			<input type="text" name="nom" maxlength="40" size="15" value="<?php echo $p->getNom(); ?>"  class="form-control"/>
		</div>
		
		<div class="col-sm-3">
			<input type="text" name="prenom" maxlength="40" size="15" value="<?php echo $p->getPrenom(); ?>"  class="form-control"/>
		</div>

		<div class="col-sm-3">
			<select name="centre" class="form-control">
			<?php
				$cs = Centre::getAllCentres();
				if($cs != null) foreach($cs as $c) {
					$selected = ($c->getLibelle() == $p->getCentre()->getLibelle()) ? ' selected="selected"' : '';
					echo "<option value=\"{$c->getId()}\"{$selected}>{$c->getLibelle()}</option>\n";
				}
			?>
			</select>
		</div>
	</div>
	
	<br>
	<hr>
	
	<div class="form-group" id="formBtn">
	    <div class="col-sm-offset-4 col-sm-2">
	      <button type="submit" class="btn btn-info" data-dismiss="modal">Fermer</button>
	    </div>
	    <div class="col-sm-2">
	      <button type="submit" class="btn btn-success" id="savePersonnel">Sauvegarder</button>
	    </div>
	</div>
	
</form>


<?php
	} else {
		displayList();
	}
?>

<script type="text/javascript">

	function recupDatas(){

		var data = {
			id : $("input[name=id]").val(),
			grade : $("select[name=grade]").val(),
			nom : $("input[name=nom]").val(),
			prenom : $("input[name=prenom]").val(),
			centre : $("select[name=centre]").val()
		}
		
		return data;
	}
	
	$("#savePersonnel").click(function(){
		var data = recupDatas();
		msg(data);
		var retour = valid_personnel($("#form-personnel"),data);
		if(retour){
			$.post("/php/formulaires/modif-personnel.php",data)
				.always(function(arg) {
					var n = arg.search("Notice");
					if( n > 0 ) {
						arg = "";
					}

					if( trim(arg) != ""){
						viderModal();
						$("#bloc-error").append(arg);
						notif("Erreur lors de la modification du personnel","danger",2000,"");
						
					} else {
						viderModal();
						updateIntelligent();
						notif("Votre personnel a bien été modifié et mis à jour dans la liste","success",1500,"");
					}
					
				});
		}
	});
	
	$("#dial-personnels").append("<span class='glyphicon glyphicon-plus text-success' style='margin-left:45px' onclick='addPerso();'></span>");
	
	function addPerso() { 
		creerModal("add-personnels", "AJOUTER DU PERSONNEL", "add-personnel","/php/formulaires/modif-personnel.php?id=-1","get", 'bg-success',"");   
	}
	
	function deletePersonnel(id,nom) { 
	
		var textes = "Confirmer la suppression définitive du personnel : "+nom;
		var titres = "CONFIRMATION DE SUPPRESSION";
		
		$.post("/php/requetes/confirm.php",{titre : titres, texte : textes}).always(function(arg){ suitedel(arg,id); });
	}
	
	function suitedel(arg,id){
		viderModal();
				$('#bloc-modal').append(arg);
				$('#confirm').modal("show");
				
				$("#del-confirm").on("click",function(){
					$.post("/php/formulaires/modif-personnel.php",{id: id, del: true} )
						.always(function(arg) {
							var n = arg.search("Notice");
							if( n > 0 ) {
								arg = "";
							}

							if( trim(arg) != ""){
								viderModal();
								$("#bloc-error").append(arg);
								notif("Erreur lors de la suppression du personnel","danger",2000,"");
							} else {
								viderModal();
								updateIntelligent();
								notif("Votre personnel a bien été supprimé définitivement de la liste","success",1500,"");
								updateIntelligent();
							}
						
						});
				});
				
				$("#del-annuler").on("click",function(){
					notif("L'opération a été annulée par l'utilisateur","warning","","");
				});
	}
	
	function insertPersonnel(id,nom){
		$.get('/php/formulaires/modif-personnel.php?in='+id).always( function(e){ 
			notif("Le personnel "+ nom + " a bien été ajouté à la liste","success",1500,""); 
			updateIntelligent();
			$("span","#dial-personnels").remove();
			$("#open-personnel").load("/php/formulaires/personnel.php");
		});
	}
	
	function editPersonnel(id){
		edit_personnel(id);
	}
</script>