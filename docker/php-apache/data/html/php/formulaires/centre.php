<?php
	require_once('../../init.php');

	if(isset($_POST['id'])) {
		//formulaire validé
		$c = new Centre($_POST['id']);
		if($_POST['del']) {
			$c->delete();
			print("ok");
		} else {
			$c->setLibelle(strtoupper($_POST['libelle']));
			$c->setAbreviation(strtoupper($_POST['abreviation']));
			$c->setPosition($_POST['position']);
			$c->commit();
		}
		exit;
	}

	$c = new Centre(@$_GET['id']);

?>


	<form class="form-horizontal" role="form" id="form-centre" method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>" onsubmit="return false;">
		
	  <div class="form-group">
	  	<div class="col-sm-12">
	  		<div id="error" />

				<input type="hidden" name="id" value="<?php echo $c->getId(); ?>" />
				<input type="hidden" name="del" />
	  	</div>
	  </div>
		
	  <div class="form-group">
	    <label for="centre" class="col-sm-2 control-label">CENTRE</label>
	    <div class="col-sm-10">
	      <input type="text" name="centre" maxlength="50" class="form-control" value="<?php echo $c->getLibelle(); ?>">
	    </div>
	  </div>
	  
	  <div class="form-group">
	    <label for="abreviation" class="col-sm-2 control-label">ABREVIATION</label>
	    <div class="col-sm-10">
	      <input type="text" name="abreviation" maxlength="5" class="form-control" value="<?php echo $c->getAbreviation(); ?>">
	    </div>
	  </div>
	  
	  <div class="form-group">
	    <label for="position" class="col-sm-2 control-label">POSITION</label>
	    <div class="col-sm-10">
	      <input type="number" name="position" maxlength="50" class="form-control" disabled="disabled" value="<?= ($c->getPosition()) ? $c->getPosition() : Centre::getLastPosition(); ?>">
	    </div>
	  </div>
	  
	  
<?php if(isset($_GET['mode']) && $_GET['mode'] == 'add') : ?>
	  
	  <div class="form-group">
	    <div class="col-sm-offset-2 col-sm-2">
	      <button type="submit" class="btn btn-info" data-dismiss="modal">Fermer</button>
	    </div>
	    <div class="col-sm-8">
	      <button type="submit" class="btn btn-success" id="addCentre">Ajouter</button>
	    </div>
	  </div>
	  

	
<?php elseif(isset($_GET['mode']) && $_GET['mode'] == 'mod') : ?>
	
	<div class="form-group">
	    <div class="col-sm-offset-2 col-sm-2">
	      <button type="submit" class="btn btn-info" data-dismiss="modal">Fermer</button>
	    </div>
	    <div class="col-sm-2">
	      <button type="submit" class="btn btn-success" id="saveCentre">Sauvegarder</button>
	    </div>
	    <div class="col-sm-6">
	      <button type="submit" class="btn btn-danger" id="delCentre">Supprimer</button>
	    </div>
	  </div>
	
<?php endif; ?>

	</form>
	
	
	<script type="text/javascript" charset="utf-8">
	
	function recupDatas(){

		var data = {
			id : $("input[name=id]").val(),
			libelle : $("input[name=centre]").val(),
			abreviation : $("input[name=abreviation]").val(),
			position : $("input[name=position]").val(),
			del : $("input[name=del]").val()
		}
		
		return data;
	}
	
	$("#addCentre").click(function(){
		var data = recupDatas();
		var retour = valid_centre($("#form-centre"),data);
		if(retour){
			$.post("/php/formulaires/centre.php",data)
				.always(function(arg) {
					var n = arg.search("Notice");
					if( n > 0 ) {
						arg = "";
					}

					if( trim(arg) != ""){
						viderModal();
						$("#bloc-error").append(arg);
						notif("Erreur lors de l'ajout du centre","danger","","");
						
					} else {
						viderModal();
						updateIntelligent();
						notif("Votre centre à bien été ajouter à la liste","success","","");
					}
					
				});
		}
	});
	
	$("#saveCentre").click(function(){
		var data = recupDatas();
		var retour = valid_centre($("#form-centre"),data);
		if(retour){
			$.post("/php/formulaires/centre.php",data)
				.always(function(arg) {
					var n = arg.search("Notice");
					if( n > 0 ) {
						arg = "";
					}

					if( trim(arg) != ""){
						viderModal();
						$("#bloc-error").append(arg);
						notif("Erreur lors de la modification du centre","danger","","");
						
					} else {
						viderModal();
						updateIntelligent();
						notif("Votre centre à bien été modifié et mis à jour dans la liste","success","","");
					}
					
				});
		}
	});
	
	$("#delCentre").click(function(){
		delCentres($("input[name=id]").val(), "centre");
	});
		
	</script>