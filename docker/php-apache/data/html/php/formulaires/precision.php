<?php
	require_once('../../init.php'); 
	
	if(isset($_POST['id'])) {
		//formulaire validé
		$p = new PrecisionIntervention($_POST['id']);
		if($_POST['del']) {
			$p->delete();
			print("ok");
		} else {
			$p->setLibelle(strtoupper($_POST['libelle']));
			$p->setNature($_POST['nature']);
			$p->commit();
		}
		exit;
	}
	
	$p = new PrecisionIntervention(@$_GET['id']);
	

?>



	<form class="form-horizontal" role="form" id="form-precision" method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>" onsubmit="return false;">
		
	  <div class="form-group">
	  	<div class="col-sm-12">
	  		<div id="error" />

				<input type="hidden" name="id" value="<?php echo $p->getId(); ?>" />
				<input type="hidden" name="del" />
	  	</div>
	  </div>
		
	  <div class="form-group">
	    <label for="libelle" class="col-sm-2 control-label">LIBELLÉ</label>
	    <div class="col-sm-10">
	      <input type="text" name="libelle" maxlength="50" class="form-control" value="<?php echo $p->getLibelle(); ?>">
	    </div>
	  </div>
	  
	  <div class="form-group">
	    <label for="nature" class="col-sm-2 control-label">NATURE</label>
	    <div class="col-sm-10">
	      <select name="nature" class="form-control">
					<option value="-1"></option>
					<?php
						$ns = NatureIntervention::getAllNatures();
						$id = $p->getNature()->getId();
						if($ns != null) foreach($ns as $n) {
							if($n->getId() == $id){
								echo "<option value='{$n->getId()}' selected='selected'>{$n->getLibelle()}</option>";
							} else if($n->getId() == 1){
								echo "<option value='{$n->getId()}' selected='selected'>{$n->getLibelle()}</option>";
							}	else {
								echo "<option value='{$n->getId()}'>{$n->getLibelle()}</option>";
							}
						}
					?>
				</select>
	    </div>
	  </div>
	  
<?php if(isset($_GET['mode']) && $_GET['mode'] == 'add') : ?>
	  
	  <div class="form-group">
	    <div class="col-sm-offset-2 col-sm-2">
	      <button type="submit" class="btn btn-info" data-dismiss="modal">Fermer</button>
	    </div>
	    <div class="col-sm-8">
	      <button type="submit" class="btn btn-success" id="addPrecision">Ajouter</button>
	    </div>
	  </div>
	  

	
<?php elseif(isset($_GET['mode']) && $_GET['mode'] == 'mod') : ?>
	
	<div class="form-group">
	    <div class="col-sm-offset-2 col-sm-2">
	      <button type="submit" class="btn btn-info" data-dismiss="modal">Fermer</button>
	    </div>
	    <div class="col-sm-2">
	      <button type="submit" class="btn btn-success" id="savePrecision">Sauvegarder</button>
	    </div>
	    <div class="col-sm-6">
	      <button type="submit" class="btn btn-danger" id="delPrecision">Supprimer</button>
	    </div>
	  </div>
	
<?php endif; ?>

	</form>
	
	
	<script type="text/javascript" charset="utf-8">
	
	function recupDatas(){
		var data = {
			id : $("input[name=id]").val(),
			libelle : $("input[name=libelle]").val(),
			nature : $("select[name=nature]").val(),
			del : $("input[name=del]").val()
		}
		return data;
	}
	
	$("#addPrecision").click(function(){
	
		var data = recupDatas();
		var retour = valid_precision($("#form-precision"),data);
		if(retour){
			$.post("/php/formulaires/precision.php",data)
				.always(function(arg) {
					var n = arg.search("Notice");
					if( n > 0 ) {
						arg = "";
					}

					if( trim(arg) != ""){
						viderModal();
						$("#bloc-error").append(arg);
						notif("Erreur lors de l'ajout de la précision","danger","","");
						
					} else {
						viderModal();
						updateIntelligent();
						notif("Votre précision a bien été ajouter à la liste","success","","");
					}
					
				});
		}
	
	});
	
	$("#savePrecision").click(function(){
		var data = recupDatas();
		
		var retour = valid_precision($("#form-precision"),data);
		if(retour){
			$.post("/php/formulaires/precision.php",data)
				.always(function(arg) {
					var n = arg.search("Notice");
					if( n > 0 ) {
						arg = "";
					}

					if( trim(arg) != ""){
						viderModal();
						$("#bloc-error").append(arg);
						notif("Erreur lors de la modification de la précision","danger","","");
						
					} else {
						viderModal();
						updateIntelligent();
						notif("Votre précision a bien été modifié et mis à jour dans la liste","success","","");
					}
					
				});
		}
	});
	
	$("#delPrecision").click(function(){
		delPrecisions($("input[name=id]").val(), "materiel");
	});
	
	
		
	</script>

