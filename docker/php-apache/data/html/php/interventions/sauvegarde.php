<?php

require_once('../../init.php');
header('Content-type: application/json');

// Info l'Etat correspond 0:Aucun, 1: En cour, 2: Reconnaissance, 3: Différée et 4 : Terminer, 5 : Annuler, 6: Liée
// Le Status correspond à l'état du véhicule soit :
// 1:En inter (orange), 3: VL reco (cyan), 5: Indisponible (noi) et 10 : Cloture inter (vert) 


// pour une inter en cour  etat à 1 et status à 1
// pour une inter en reco  etat à 2 et status à 3
// pour une inter en diff  etat à 3 et status à 1
// pour une inter en term  etat à 4 et status à 10
// pour une inter en annul etat à 5 et status à 10

if(isset($_POST['id'])) {
  
	//debug($_POST);
	$newEtat = $_POST['etatUpdate'];
	//formulaire validé
	$inter = new Intervention($_POST['id']);
	$id = $_POST['id'];
	$d = explode('/', $_POST['date']);
	$heure = strval($_POST['heure']);
	$h = substr($heure, 0, 2);
	$m = substr($heure, 2, 2);
	//$time = mktime($h, $m, 0, $d[1], $d[2], $d[0]);
	$time = mktime($h, $m, 0, $d[1], $d[0], $d[2]);
	if($inter->notExists()) {
		$inter->setDateHeure($time);
	}
	$interLier = trim($_POST['codisLier']);
	$inter->setCodisLier($interLier);
	$inter->setDateHeure($time);
	$inter->setNomAppelant($_POST['nom']);
	$inter->setPrenomAppelant($_POST['prenom']);
	$inter->setNumeroRue($_POST['num_rue']);
	$inter->setCommune($_POST['commune']);
	$inter->setAdresse($_POST['adresse']);
	$inter->setEtage($_POST['etage']);
	$inter->setAppartement($_POST['appt']);
	$inter->setTelephoneAppelant($_POST['tel']);
	$inter->setNature($_POST['nature']);
	$inter->setPrecision($_POST['precision']);
	$inter->setComplement($_POST['compl']);
	$pext = @$_POST['presencesexternes'];
	$mat = (isset($_POST['materiel']) && is_array($_POST['materiel']) ) ? $_POST['materiel'] : null;
	$pe = null;
	if( $pext != null ){
			if($pext['id'] != null){
				$pextId = $pext['id'];
				$pextNb = $pext['quantite'];
				$total = count($pextId);
				for( $i=0 ; $i < $total; $i++ ) {
						$id_pe = $pextId[$i];
						$id_inter = $_POST['id'];
						$nb = $pextNb[$i];
						$pe[] = array("id" => $id_pe, "quantite" => $nb);
				}
				$inter->setPresenceExterne($pe);
			}
	}

  $materiels = Materiel::getAllMaterielsByInter($id);

	switch($newEtat) {
		
		case 0 : // Aucun
		{
			$inter->setFin(0);
			$inter->setEtat(0);
			$inter->setStatus(5);
			$inter->setInterventionsLiees();
			$inter->setVehicules(array());
			$inter->setMateriels(array());
      Materiel::updateInter(null, $id);
			$inter->setDifferee(0);
      $inter->setPresenceExterne(array());
		}
		break;
		
		case 1 : // En cours
		{
			$inter->setFin(0);
			$inter->setEtat(1);
			$inter->setStatus(1);
			$inter->setVehicules(@$_POST['vehicule']);
			$inter->setDifferee(0);
			$veh = $_POST['vehicule'];
			if( $veh != null ){
				foreach( $veh as $v ) {
					$v = new Vehicule($v);
					$v->setEtat(1);
					$v->setIntervention($_POST['id']);
					$v->commit();
				}
			}

      Materiel::updateInter($mat, $id);
		}
		break;
		
		case 2 : // Reconnaissance
		{
			$inter->setFin(0);
			$inter->setEtat(2);
			$inter->setStatus(3);
			$inter->setMateriels(array());
      Materiel::updateInter(null, $id);
			$inter->setVehicules(@$_POST['vehicule']);
			$veh = $_POST['vehicule'];
			if( $veh != null ){
				foreach( $veh as $v ) {
					$v = new Vehicule($v);
					$v->setEtat(3);
					$v->setIntervention($_POST['id']);
					$v->commit();
				}
			}
			$inter->setDifferee(0);
      $inter->setPresenceExterne(array());
		}
		break;
		
		case 3 : // Differee
		{
			$inter->setFin(0);
			$inter->setEtat(3);
			$inter->setStatus(1);
			$inter->setMateriels(array());
      Materiel::updateInter(null, $id);
			$inter->setVehicules(array());
			$inter->setDifferee(1);
      $inter->setPresenceExterne(array());
		}
		break;
		
		case 4 : // Terminer
		{
			$inter->setFin(time());
			$inter->setEtat(4);
			$inter->setStatus(10);
			$inter->setMateriels(array());
      Materiel::updateInter(null, $id);
			$inter->setVehicules(array());
			$inter->setDifferee(0);
		}
		break;
		
		case 5 : // Annuler
		{
			$inter->setFin(time());
			$inter->setEtat(5);
			$inter->setStatus(10);
			$inter->setMateriels(array());
      Materiel::updateInter(null, $id);
			$inter->setVehicules(array());
			$inter->setDifferee(0);
      $inter->setPresenceExterne(array());
		}
		break;
	}
	
	$inter->commit();
	if ( $pe != null ) {
		foreach( $pe as $p ){
			if($p['id'] != null){
				PresenceExterne::setQuantite($id,$p['id'],$p['quantite']);
			}
		}
	} else {
		PresenceExterne::delQuantite($id);
	}

	echo json_encode(array("retour" => "ok"));
}

?>