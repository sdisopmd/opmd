

<form class="form-horizontal" role="form" name="feuille_depart" method="POST" enctype="multipart/form-data" action="">

<div class="row">

<div class="col-md-8">

  <div class="form-group has-feedback">
	<label for="date" class="col-sm-4 control-label">DATE </label>
	<div class="col-sm-3">
	<input type="text" class="form-control input-sm" name="date" value="<?= $date; ?>" placeholder="jj/mm/aaaa">
	<span class="glyphicon glyphicon-time form-control-feedback" onclick="setDnow($('input[name=date]'));" style="margin-top:-3px"></span>
	</div>
	<div id="error-date"> </div>
	<label for="heure" class="col-sm-2 control-label">HEURE </label>
	<div class="col-sm-3">
	  <input type="text" class="form-control input-sm" name="heure" value="<?= $heure; ?>" placeholder="hhmm">
	  <span class="glyphicon glyphicon-time form-control-feedback" onclick="setHnow($('input[name=heure]'));" style="margin-top:-3px"></span>
	</div> 
	<div id="error-heure"> </div>
  </div>
  
  <div class="form-group">
	<label for="id" class="col-sm-4 control-label">NUMERO CODIS</label>
	<div class="col-sm-8">
	  <input type="number" class="form-control input-sm exist" name="id" value="<?= $inter->getId(); ?>" disabled="disabled">
	  <div id="error-codis"> </div>
	</div>
  </div>
	
	<div class="form-group">
	<label for="id" class="col-sm-4 control-label">INTER LIÉE</label>
	<div class="col-sm-8">
		<?php if($inter->exists()) : ?>
	  <input type="text" class="form-control input-sm exist" name="codis_lier" value="<?= $inter->getCodisLier(); ?>">
	  <?php else : ?>
	  <input type="text" class="form-control input-sm" name="codis_lier" value="<?php if($inter->exists()) echo $inter->getCodisLier(); ?>" onchange="verifCodisLier($(this).val());">
	  <?php endif; ?>
	  <div id="error-codis_lier"> </div>
	</div>
  </div>
  
  <div class="form-group">
	<label for="nom" class="col-sm-4 control-label">NOM</label>
	<div class="col-sm-8">
	  <input type="text" class="form-control input-sm" name="nom" maxlength="40" value="<?php echo $inter->getNomAppelant(); ?>">
	  <div id="error-nom"> </div>
	</div>
  </div>
  
  <div class="form-group">
	<label for="prenom" class="col-sm-4 control-label">PRENOM</label>
	<div class="col-sm-8">
	  <input type="text" class="form-control input-sm" name="prenom" maxlength="40" value="<?php echo $inter->getPrenomAppelant(); ?>">
	  <div id="error-prenom"> </div>
	</div>
  </div>
  
	<div class="form-group">
	<label for="commune" class="col-sm-4 control-label">COMMUNE</label>
	<div class="col-sm-5">
	  <select name="commune" onclick="$.getScript('/php/requetes/cp.php?output=cp&n='+$(this).val())" class="form-control input-sm">
		<option value="0"></option>
		<?php
			$cur = $inter->getCommune();
			$cs = Commune::getAllCommunes();
			if($cs != null) foreach($cs as $c) {
				if( $c->getId() == $cur->getId() ){
				echo $c->getId()." == ".$cur->getId();
					echo "<option value='{$inter->getCommune()->getId()}' selected='selected'>{$inter->getCommune()->getLibelle()}</option>";
				} else {
					echo "<option value='{$c->getId()}'>{$c->getLibelle()}</option>";
				}
			}
		?>
	  </select>
	  <div id="error-commune"> </div>
	</div>
	<div class="col-sm-3">
	  <input type="number" id="cp" maxlength="5" value="<?= $cp ?>" name="cp" disabled class="form-control input-sm">
	  <div id="error-cp"> </div>
	</div>
  </div>
  
  <div class="form-group">
		<label for="adresse" class="col-sm-4 control-label">ADRESSE</label>
			<div class="col-sm-8">
				<?php	if( $inter->exists() ) : ?>
						<input type="text" class="form-control input-sm" name="adresse" value="<?php echo $inter->getAdresse(); ?>">
				<?php	else : ?>
						<input type="text" class="form-control input-sm" name="adresse" value="">
				<?php endif;	?>
				<div id="error-adresse"> </div>
			</div>
  </div>
  
  <div class="form-group">
	<label for="num_rue" class="col-sm-4 control-label">N°</label>
	<div class="col-sm-2">
	  <input type="number" class="form-control input-sm" name="num_rue" maxlength="5" value="<?php echo $inter->getNumeroRue(); ?>">
	  <div id="error-num"> </div>
	</div>
	<label for="etage" class="col-sm-1 control-label">ETAGE</label>
	<div class="col-sm-2">
	  <input type="number" class="form-control input-sm" name="etage" maxlength="5" value="<?php echo $inter->getEtage(); ?>">
	  <div id="error-num"> </div>
	</div>
	<label for="appt" class="col-sm-1 control-label">APPT</label>
	<div class="col-sm-2">
	  <input type="number" class="form-control input-sm" name="appt" maxlength="5" value="<?php echo $inter->getAppartement(); ?>">
	  <div id="error-num"> </div>
	</div>

  </div>
 
  <div class="form-group">
	<label for="telephone" class="col-sm-4 control-label">TELEPHONE</label>
	<div class="col-sm-8">
	  <input type="tel" name="telephone" maxlength="14" value="<?php echo $inter->getTelephoneAppelant(); ?>" class="form-control input-sm">
	  <div id="error-tel"> </div>
	</div>
  </div>
  
  <div class="form-group">
	<label for="complement" class="col-sm-4 control-label">COMPLEMENTS D'INFORMATION</label>
	<div class="col-sm-8">
	  <textarea name="complement" maxlength="100" class="form-control input-sm" rows="2"><?php echo $inter->getComplement(); ?></textarea>
	  <div id="error-compl"> </div>
	</div>
  </div>
  
  <div class="form-group">
	<label for="nature" class="col-sm-4 control-label">NATURE</label>
	<div class="col-sm-8">
	  <select name="nature" class="form-control input-sm" onchange="$.getScript('/php/requetes/precisions.php?form=feuille_depart&select=precision&n='+this.options[this.selectedIndex].value)">
		<?php 
			$a = $inter->getNature();
      $one = false;
      echo '<option value="0"></option>';
      $ns = NatureIntervention::getAllNatures();
      if($ns != null) foreach($ns as $n) {
        if($a->exists() && $one == false){
          $one = true;
          echo "<option value=\"{$a->getId()}\" selected=\"selected\">{$a->getLibelle()}</option>\n";
        } else {
          echo "<option value=\"{$n->getId()}\">{$n->getLibelle()}</option>\n";
        }
      }
			?>
	  </select>
	  <div id="error-nature"> </div>
	</div>
  </div>
  
  <div class="form-group">
	<label for="precision" class="col-sm-4 control-label">PRECISION</label>
	<div class="col-sm-8">
	  <select name="precision" class="form-control input-sm">
		<?php
			$p = $inter->getPrecision();
			if($p->exists())
				echo "<option value=\"{$p->getId()}\">{$p->getLibelle()}</option>";
		?>
	  </select>
	  <div id="error-precision"> </div>
	</div>
  </div>
		  
	  
	<div class="form-group">
		<label for="etat" class="col-sm-4 control-label">ETAT</label>
		<div class="col-sm-8">
				
			<?php
				$listEtat = Intervention::listEtat();
				$etatVal = $inter->getEtat();
				$etatLab = $inter->recupEtat($etatVal);
				$etatClass = $inter->classEtat($etatVal);
			
			?>
			<input type="button" class="form-control text-center btn btn-<?= $etatClass; ?>" name="etat" data="<?= $etatVal; ?>" value="<?= $etatLab; ?>" >
		</div> 
	</div>

</div>