<form class="form-horizontal" role="form" name="feuille_depart" method="POST" enctype="multipart/form-data" action="">

<div class="row">

<div class="col-md-8">

  <div class="form-group has-feedback">
	<label for="date" class="col-sm-4 control-label">DATE </label>
	<div class="col-sm-3">
	<input type="text" class="form-control input-sm" name="date" value="<?= $date; ?>" placeholder="jj/mm/aaaa">
	<span class="glyphicon glyphicon-time form-control-feedback" onclick="setDnow($('input[name=date]'));" style="margin-top:-3px"></span>
	</div>
	<div id="error-date"> </div>
	<label for="heure" class="col-sm-2 control-label">HEURE </label>
	<div class="col-sm-3">
	  <input type="text" class="form-control input-sm" name="heure" value="<?= $heure; ?>" placeholder="hhmm">
	  <span class="glyphicon glyphicon-time form-control-feedback" onclick="setHnow($('input[name=heure]'));" style="margin-top:-3px"></span>
	</div> 
	<div id="error-heure"> </div>
  </div>
  
  <div class="form-group">
	<label for="id" class="col-sm-4 control-label">NUMERO CODIS</label>
	<div class="col-sm-8">
		<?php if($inter->exists()) : ?>
	  <input type="number" class="form-control input-sm exist" name="id" value="<?= $inter->getId(); ?>" disabled="disabled">
	  <?php else : ?>
	  <input type="number" class="form-control input-sm" name="id" value="<?php if($inter->exists()) echo $inter->getId(); ?>" onchange="verifCodis($(this).val());">
	  <?php endif; ?>
	  <div id="error-codis"> </div>
	</div>
  </div>

	<div class="form-group">
	<label for="id" class="col-sm-4 control-label">INTER LIÉE</label>
	<div class="col-sm-8">
		<?php if($inter->exists()) : ?>
	  <input type="text" class="form-control input-sm exist" name="codis_lier" value="<?= $inter->getCodisLier(); ?>">
	  <?php else : ?>
	  <input type="text" class="form-control input-sm" name="codis_lier" value="<?php if($inter->exists()) echo $inter->getCodisLier(); ?>" onchange="verifCodisLier($(this).val());">
	  <?php endif; ?>
	  <div id="error-codis_lier"> </div>
	</div>
  </div>
  
  <div class="form-group">
	<label for="nom" class="col-sm-4 control-label">NOM</label>
	<div class="col-sm-8">
	  <input type="text" class="form-control input-sm" name="nom" maxlength="40" value="<?php echo $inter->getNomAppelant(); ?>">
	  <div id="error-nom"> </div>
	</div>
  </div>
  
  <div class="form-group">
	<label for="prenom" class="col-sm-4 control-label">PRENOM</label>
	<div class="col-sm-8">
	  <input type="text" class="form-control input-sm" name="prenom" maxlength="40" value="<?php echo $inter->getPrenomAppelant(); ?>">
	  <div id="error-prenom"> </div>
	</div>
  </div>
  
   <div class="form-group">
	<label for="commune" class="col-sm-4 control-label">COMMUNE</label>
	<div class="col-sm-5">
	  <select name="commune" onclick="$.getScript('/php/requetes/cp.php?output=cp&n='+$(this).val())" class="form-control input-sm">
		<option value="0"></option>
		<?php
			$cur = $inter->getCommune();
			$cs = Commune::getAllCommunes();
			if($cs != null) foreach($cs as $c) {
				if( $c->getId() == $cur->getId() ){
				echo $c->getId()." == ".$cur->getId();
					echo "<option value='{$inter->getCommune()->getId()}' selected='selected'>{$inter->getCommune()->getLibelle()}</option>";
				} else {
					echo "<option value='{$c->getId()}'>{$c->getLibelle()}</option>";
				}
			}
		?>
	  </select>
	  <div id="error-commune"> </div>
	</div>
	<div class="col-sm-3">
	  <input type="number" id="cp" maxlength="5" value="<?= $cp ?>" name="cp" disabled class="form-control input-sm">
	  <div id="error-cp"> </div>
	</div>
  </div>
  
  <div class="form-group">
		<label for="adresse" class="col-sm-4 control-label">ADRESSE</label>
			<div class="col-sm-8">
				<?php	if( $inter->exists() ) : ?>
						<input type="text" class="form-control input-sm" name="adresse" value="<?php echo $inter->getAdresse(); ?>">
				<?php	else : ?>
						<input type="text" class="form-control input-sm" name="adresse" value="">
				<?php endif;	?>
				<div id="error-adresse"> </div>
			</div>
  </div>
  
  <div class="form-group">
	<label for="num_rue" class="col-sm-4 control-label">N°</label>
	<div class="col-sm-2">
	  <input type="number" class="form-control input-sm" name="num_rue" maxlength="5" value="<?php echo $inter->getNumeroRue(); ?>">
	  <div id="error-num"> </div>
	</div>
	<label for="etage" class="col-sm-1 control-label">ETAGE</label>
	<div class="col-sm-2">
	  <input type="number" class="form-control input-sm" name="etage" maxlength="5" value="<?php echo $inter->getEtage(); ?>">
	  <div id="error-num"> </div>
	</div>
	<label for="appt" class="col-sm-1 control-label">APPT</label>
	<div class="col-sm-2">
	  <input type="number" class="form-control input-sm" name="appt" maxlength="5" value="<?php echo $inter->getAppartement(); ?>">
	  <div id="error-num"> </div>
	</div>

  </div>
 
  <div class="form-group">
	<label for="telephone" class="col-sm-4 control-label">TELEPHONE</label>
	<div class="col-sm-8">
	  <input type="tel" name="telephone" maxlength="14" value="<?php echo $inter->getTelephoneAppelant(); ?>" class="form-control input-sm">
	  <div id="error-tel"> </div>
	</div>
  </div>
  
  <div class="form-group">
	<label for="complement" class="col-sm-4 control-label">COMPLEMENTS D'INFORMATION</label>
	<div class="col-sm-8">
	  <textarea name="complement" class="form-control input-sm" rows="2"><?php echo $inter->getComplement(); ?></textarea>
	  <div id="error-compl"> </div>
	</div>
  </div>
  
  <div class="form-group">
	<label for="nature" class="col-sm-4 control-label">NATURE</label>
	<div class="col-sm-8">
	  <select name="nature" class="form-control input-sm" onchange="$.getScript('/php/requetes/precisions.php?form=feuille_depart&select=precision&n='+this.options[this.selectedIndex].value)">
		<?php 
			$a = $inter->getNature();
      $one = false;
      echo '<option value="0"></option>';
      $ns = NatureIntervention::getAllNatures();
      if($ns != null) foreach($ns as $n) {
        if($a->exists() && $one == false){
          $one = true;
          echo "<option value=\"{$a->getId()}\" selected=\"selected\">{$a->getLibelle()}</option>\n";
        } else {
          echo "<option value=\"{$n->getId()}\">{$n->getLibelle()}</option>\n";
        }
      }
			?>

	  </select>
	  <div id="error-nature"> </div>
	</div>
  </div>
  
  <div class="form-group">
	<label for="precision" class="col-sm-4 control-label">PRECISION</label>
	<div class="col-sm-8">
	  <select name="precision" class="form-control input-sm">
		<?php
			$p = $inter->getPrecision();
			if($p->exists())
				echo "<option value=\"{$p->getId()}\">{$p->getLibelle()}</option>";
		?>
	  </select>
	  <div id="error-precision"> </div>
	</div>
  </div>
		  
	  
	<div class="form-group">
		<label for="etat" class="col-sm-4 control-label">ETAT</label>
		<div class="col-sm-8">
				
			<?php
				$listEtat = Intervention::listEtat();
				$etatVal = $inter->getEtat();
				$etatLab = $inter->recupEtat($etatVal);
				$etatClass = $inter->classEtat($etatVal);
			
			?>
			<input type="button" class="form-control text-center btn btn-<?= $etatClass; ?>" name="etat" data="<?= $etatVal; ?>" value="<?= $etatLab; ?>" >
		</div> 
	</div>

</div>

<div class="col-md-4">
		
	<div id='cssmenu'>

		<ul>
	
	<?php $vGr = Vehicule::getVehiculeByGroupOrderPosition(); ?>
	
	<li class='has-sub'><a href='#'><span>VEHICULES</span></a>
	
	<ul>
	<?php
    if($vGr != null){
	foreach( $vGr as $g ) {
		echo "<li class='has-sub'><a href='#'><span>{$g->getLibelle()}</span></a>";
		$vg = Vehicule::getAllVehiculesByGroup($g->getId());
		echo "<ul>";
    if($vg != null){
      foreach( $vg as $v ) {
        if($v->getVisible() == 1) {
          $etat_v = $v->getEtat();
          $actifV = $v->getEtat()->getId();
          $styleV = "background-color:{$etat_v->getCouleurFond()}; color:{$etat_v->getCouleurTexte()};";
          if($actifV == 10){
            echo "<li id='veh-{$v->getId()}'><a style='{$styleV}' href='#' onclick=\"addVehicule('liste_vehicules', '{$v->getId()}', '{$v->getLibelle()}');\"><span> {$v->getLibelle()} {$v->getCentre()->getAbreviation()}</span></a></li>";
          } else {
            echo "<li id='veh-{$v->getId()}'><a style='{$styleV}' href='#' disabled><span> {$v->getLibelle()} {$v->getCentre()->getAbreviation()}</span></a></li>";
          }
      
        }
      }
    }
		
		echo "</ul>";
		echo "</li>";
	}}
	echo "</li></ul>";
		echo "</li>";

	
	$mGr = Materiel::getMaterielByGroupOrderPosition(); 
	echo "<li class='has-sub'><a href='#'><span>MATERIELS</span></a>";
	echo "<ul>";
  if($mGr != null){
	foreach( $mGr as $g ) { 
		echo "<li class='has-sub'><a href='#'><span>{$g->getLibelle()}</span></a>";
		$materiel = Materiel::getAllMaterielsByGroup($g->getId());
	echo "<ul>";
  if($materiel != null){
    foreach( $materiel as $m ) {
      if($m->getVisible() == 1) {
        $etat_m = $m->getEtat();
        $total = $m->getQuantite();
        $actifM = $m->getEtat()->getId();
        $reste = $m->getDispo();
        $styleM = "background-color:{$etat_m->getCouleurFond()}; color:{$etat_m->getCouleurTexte()};";
        if($actifM == 10 || $reste > 0){
          echo "<li id='mat-{$m->getId()}' data-total='{$total}'><a style='{$styleM}' href='#' onclick=\"addMateriel('liste_materiels', '{$m->getId()}', '{$m->getLibelle()}');\"><span class='texte'> {$m->getLibelle()} {$m->getCentre()->getAbreviation()} </span><b class='pull-right badge-white dispo'>{$m->getDispo()}</b></a></li>";
        } else {
          echo "<li id='mat-{$m->getId()}' data-total='{$total}'><a style='{$styleM}' href='#' disabled><span> {$m->getLibelle()} {$m->getCentre()->getAbreviation()} <b class='pull-right badge-white dispo'>{$m->getDispo()}</b></span></a></li>";
        }
      
      }
    }
  }
		echo "</ul>";
		echo "</li>";
	}}
		echo "</li></ul>";
		echo "</li>";
		
		
	echo "<li class='last has-sub'><a href='#'><span>PERSONNALITE</span></a>";
			   	$pes = PresenceExterne::getAllPresencesExternes();
					if($pes != null) {
						echo "<ul>";
						foreach($pes as $pe) {
								echo "<li><a onclick='addPresenceExterne(\"presence_externe\", {$pe->getId()}, \"{$pe->getLibelle()}\");'>{$pe->getLibelle()}</a></li>";
						}
						echo "</ul>";
					}
					
			   echo "</li>";
	
	echo "</ul>";
	echo "</div>";
	

		
		echo "<div class='bg-danger' style='clear:both'>";
				echo "<h3>VEHICULES</h3>";
				echo "<div id='error-vehicule'> </div>";
				echo "<div id='liste_vehicules'>";
					$veh = $inter->getAllVehicules();
					if($veh != null) foreach($veh as $v) {
						echo "<span id='v_{$v->getId()}' class='label label-danger space size' onclick='removeVehicule(\"liste_vehicules\", {$v->getId()});removeItemsV(\"v_{$v->getId()}\");'><input type='hidden' name='vehicules[]' value='{$v->getId()}' >{$v->getLibelle()} {$v->getCentre()->getAbreviation()}</span>";
					}
				echo "</div>";
			echo "</div>";
			echo "<br><br><br><br>";

				echo "<div class='bg-warning' style='clear:both'>";
				echo "<h3>MATERIELS</h3>";
				echo "<div id='error-materiel'> </div>";
				echo "<div id='liste_materiels'>";
					$mat = $inter->getAllMateriels();
					if($mat != null) foreach($mat as $m) {
						$util = ($m->getNbByInter($inter->getId()) != 0 ) ? $m->getNbByInter($inter->getId()) : "";
						echo "<span id='m_{$m->getId()}' class='label label-warning space size' onclick='removeMateriel(\"liste_materiels\", {$m->getId()});'><input type='hidden' name='materiels[]' value='{$m->getId()}' data='{$util}'>{$m->getLibelle()} {$m->getCentre()->getAbreviation()} {$util}</span>";
					}
				echo "</div>";
			echo "</div>";
			echo "<br><br><br><br>";
		
		
		
		echo "<div id='presence_externe' class='bg-info' style='clear:both'>";
			echo "<h3>PERSONNALITE</h3>";
			echo "<div id='error-perso'> </div>";
			$pes = $inter->getPresencesExternes();
			$id_inter = $inter->getId();
			if($pes) foreach($pes as $pe) {
				$quantite = 1;
				$txt = 1;
				echo "<span id='pe_{$pe->getId()}' class='label label-info space size prext' onclick='removePresenceExterne(\"presence_externe\",{$pe->getId()});'><input type='hidden' name='presencesexternes[]' value='{$pe->getId()}' data='{$quantite}'>{$pe->getLibelle()} {$txt}</span>";
			}
		echo "</div>";
		?>
		
		
		
		
		
		<input type='hidden' name='fin' value='0' >
		
	</div>
	
		