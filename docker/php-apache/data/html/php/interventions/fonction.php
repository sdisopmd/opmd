<?php

require_once('../../init.php');


function menuVehicules() {
	
	$vGr = Vehicule::getVehiculeByGroupOrderPosition();
	echo "<li class='has-sub'><a href='#'><span>VEHICULES</span></a>";
	
	echo "<ul>";
	foreach( $vGr as $g ) {
		echo "<li class='has-sub'><a href='#'><span>{$g->getLibelle()}</span></a>";
		$vg = Vehicule::getAllVehiculesByGroup($g->getId());
		echo "<ul>";
		foreach( $vg as $v ) {
			if($v->getVisible() == 1) {
			$etat_v = $v->getEtat();
			$actifV = $v->getEtat()->getId();
			$styleV = "background-color:{$etat_v->getCouleurFond()}; color:{$etat_v->getCouleurTexte()};";
			if($actifV == 10){
				echo "<li id='veh-{$v->getId()}'><a style='{$styleV}' href='#' onclick=\"addVehicule('liste_vehicules', '{$v->getId()}', '{$v->getLibelle()}', 'danger');\"><span> {$v->getLibelle()} {$v->getCentre()->getAbreviation()}</span></a></li>";
			} else {
				echo "<li id='veh-{$v->getId()}'><a style='{$styleV}' href='#' disabled><span> {$v->getLibelle()} {$v->getCentre()->getAbreviation()}</span></a></li>";
			}
			
			}
		}
		echo "</ul>";
		echo "</li>";
	}
	echo "</li></ul>";
		echo "</li>";
	
}

function menuMateriels() {
	$mGr = Materiel::getMaterielByGroupOrderPosition(); 
	echo "<li class='has-sub'><a href='#'><span>MATERIELS</span></a>";
	echo "<ul>";
	foreach( $mGr as $g ) { 
		echo "<li class='has-sub'><a href='#'><span>{$g->getLibelle()}</span></a>";
		$materiel = Materiel::getAllMaterielsByGroup($g->getId());
	echo "<ul>";
	foreach( $materiel as $m ) {
		if($m->getVisible() == 1) {
			$etat_m = $m->getEtat();
			$actifM = $m->getEtat()->getId();
			$reste = $m->getDispo();
			$styleM = "background-color:{$etat_m->getCouleurFond()}; color:{$etat_m->getCouleurTexte()};";
			if($actifM == 10 || $reste > 0){
				echo "<li id='mat-{$m->getId()}'><a style='{$styleM}' href='#' onclick=\"addMateriel('liste_materiels', '{$m->getId()}', '{$m->getLibelle()}');\"><span> {$m->getLibelle()} {$m->getCentre()->getAbreviation()} <b class='pull-right badge-white dispo'>{$m->getDispo()}</b></span></a></li>";
			} else {
				echo "<li id='mat-{$m->getId()}'><a style='{$styleM}' href='#' disabled><span> {$m->getLibelle()} {$m->getCentre()->getAbreviation()} <b class='pull-right badge-white dispo'>{$m->getDispo()}</b></span></a></li>";
			}
		}
	}
	echo "</ul>";
	echo "</li>";
	}
	echo "</li></ul>";
	echo "</li>";
}

function menuPresences() {
	
	echo "<li class='last has-sub'><a href='#'><span>PERSONNALITE</span></a>";
		$pes = PresenceExterne::getAllPresencesExternes();
		$nb = count($pes);
		$i=0;
		if($pes != null) {
			echo "<ul>";
			foreach($pes as $pe) {
				if($i == $nb) {
					echo "<li><a onclick='addPresenceExterne(\"presence_externe\", {$pe->getId()}, \"{$pe->getLibelle()}\", \"info\");'>{$pe->getLibelle()}</a></li>";
				} else {
					echo "<li><a onclick='addPresenceExterne(\"presence_externe\", {$pe->getId()}, \"{$pe->getLibelle()}\", \"info\");'>{$pe->getLibelle()}</a></li>";
				}
				$i++;
			}
			echo "</ul>";
		}
		
   echo "</li>";
	
}


function encartVehicules($inter) {
	
	echo "<div class='bg-danger' style='clear:both'>";
		echo "<h3>VEHICULES</h3>";
		echo "<div id='error-vehicule'> </div>";
		echo "<div id='liste_vehicules'>";
			$veh = $inter->getAllVehicules();
			if($veh != null) foreach($veh as $v) {
				echo "<span id='v_{$v->getId()}' class='label label-danger space size' onclick='removeVehicule(\"liste_vehicules\", {$v->getId()});removeItemsV(\"v_{$v->getId()}\");'><input type='hidden' name='vehicules[]' value='{$v->getId()}' >TEST{$v->getLibelle()} {$v->getCentre()->getAbreviation()}</span>";
			}
		echo "</div>";
	echo "</div>";
	echo "<br><br><br><br>";
	
}


function encartMateriels($inter) {
	
	echo "<div class='bg-warning' style='clear:both'>";
		echo "<h3>MATERIELS</h3>";
		echo "<div id='error-materiel'> </div>";
		echo "<div id='liste_materiels'>";
			$mat = $inter->getAllMateriels();
			if($mat != null) foreach($mat as $m) {
				$util = ( $m->getUtiliser() > 1 ) ? $m->getUtiliser() : "";
				echo "<span id='m_{$m->getId()}' class='label label-warning space size' onclick='removeMateriel(\"liste_materiels\", {$m->getId()});'><input type='hidden' name='materiels[]' value='{$m->getId()}' data='{$m->getDispo()}'>{$m->getLibelle()} {$m->getCentre()->getAbreviation()} {$util}</span>";
			}
		echo "</div>";
	echo "</div>";
	echo "<br><br><br><br>";
	
}

function encartPresences($inter) {
	
	echo "<div id='presence_externe' class='bg-info' style='clear:both'>";
		echo "<h3>PERSONNALITE</h3>";
		echo "<div id='error-perso'> </div>";
		$pes = $inter->getPresencesExternes();
		if($pes) foreach($pes as $pe) {
			$quantite = 1;
			$txt = 1;
			echo "<span id='pe_{$pe->getId()}' class='label label-info space size prext' onclick='removePresenceExterne(\"presence_externe\",{$pe->getId()});'><input type='hidden' name='presencesexternes[]' value='{$pe->getId()}' data='{$quantite}'>{$pe->getLibelle()} {$txt}</span>";
		}
	echo "</div>";
	
}

function creerMenu($deb, $intermediaire, $fin) {
	
	if ( $deb == true ){
		echo "<div class='col-md-4'>";
		echo "<div id='cssmenu'>";
		echo "<ul>";
	}
	
	if ( $intermediaire == true ){
		echo "</ul>";
		echo "</div>";
	}
	
	if ( $fin == true ) {
		echo "<input type='hidden' name='fin' value='0' >";
		echo "</div>";
	}
	
}

// Info l'Etat correspond 0:Création, 1: En cour, 2: Reconnaissance, 3: Différée et 4 : Terminer, 5 : Annuler, 6: Liée
function afficheMenu($inter, $etat) {
	
	// inter en création
	if ( $etat == 0 ) {
		creerMenu(true, false, false);
		menuVehicules();
		menuMateriels();
		menuPresences();
		creerMenu(false, true, false);
		encartVehicules($inter);
		encartMateriels($inter);
		encartPresences($inter);
		creerMenu(false, false, true);
	}
	
	// inter en cour
	if ( $etat == 1 ) {
		creerMenu(true, false, false);
		menuVehicules();
		menuMateriels();
		menuPresences();
		creerMenu(false, true, false);
		encartVehicules($inter);
		encartMateriels($inter);
		encartPresences($inter);
		creerMenu(false, false, true);
	}
	
	// inter en différée
	if ( $etat == 3 ) {
		creerMenu(true, false, false);
		menuVehicules();
		menuMateriels();
		menuPresences();
		creerMenu(false, true, false);
		encartVehicules($inter);
		encartMateriels($inter);
		encartPresences($inter);
		creerMenu(false, false, true);
	}
	
	// inter en vl reco
	if ( $etat == 2 ) {
		creerMenu(true, false, false);
		menuVehicules();
		creerMenu(false, true, false);
		encartVehicules($inter);
		creerMenu(false, false, true);
	}
	

}

?>