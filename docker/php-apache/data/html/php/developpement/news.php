<?php
require_once('../../init.php');
header('Content-type: application/json');



if(isset($_GET['diff'])) {
	$inter = new Intervention($_GET['diff']);
	if($inter->exists()) {
		$inter->setStatus(1);
		$inter->setEtat(3);
		$inter->setMateriels(array());
		$inter->setVehicules(array());
		$inter->setDifferee(1);
		$inter->commit();
	}
	echo json_encode(array("retour" => "ok"));
}
	
?>