<?php
require_once('../../init.php');

$vs = Vehicule::getAllVehicules();
$etatDispo = 0;
$etatInter = 0;
$etatReco = 0;
$etatIndispo = 0;

if($vs != null) {
	for($i=0 ; $i<count($vs) ; $i++) {
		$v = $vs[$i];
		if($v->getVisible() == 1 && $v->getCentre(true) == 1 )
		{
			$etat_v = $v->getEtat();
			if( $etat_v->getId() == 1 ){ ++$etatInter; }
			if( $etat_v->getId() == 3 ){ ++$etatReco; }
			if( $etat_v->getId() == 5 ){ ++$etatIndispo; }
			if( $etat_v->getId() == 10 ){ ++$etatDispo; }
		}
	}
}

echo "<div class='panel-heading bg-apple text-center'><b>LISTE DES VEHICULES AU CENTRE DE CHAUMONT ( <span class='badge-success'>{$etatDispo}</span> / <span class='badge-warning'>{$etatInter}</span> / <span class='badge-info'>{$etatReco}</span> / <span class='badge-black'>{$etatIndispo}</span> )</b></div>";
if($vs != null) {
	for($i=0 ; $i<count($vs) ; $i++) {
		$v = $vs[$i];
		if($v->getVisible() == 1 && $v->getCentre(true) == 1 )
		{
			$etat_v = $v->getEtat();
			$style = "background-color:{$etat_v->getCouleurFond()}; color:{$etat_v->getCouleurTexte()}";
			echo "<button class='btn space' style='{$style}' onclick='modVehicules2({$v->getId()})';><b>{$v->getLibelle()} {$v->getCentre()->getAbreviation()}</button></b>";
		}
	}
}



?>