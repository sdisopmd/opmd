<?php
require_once('../../init.php');

$compteur = Intervention::compteurInter();
$nb_inter_en_cour = $compteur['enCour'];
$nb_inter_en_reco = $compteur['vlreco'];
$nb_inter_differee = $compteur['diff'];
$total = $compteur['total'];
$off = Personnel::getNbOfficiersCaserne();
$ssoff = Personnel::getNbSousOfficiersCaserne();
$hdr = Personnel::getNbHommesDuRangCaserne();

$perso_c = (Personnel::getAllPersonnelPresentDispo() != null) ? count(Personnel::getAllPersonnelPresentDispo()) : 0;
$perso_i = (Personnel::getAllPersonnelPresentEnInter() != null) ?  count(Personnel::getAllPersonnelPresentEnInter()) : 0;

?>

<div class='panel-heading bg-apple text-center'><b>TOTAL INTERVENTIONS : <span class=""><?= $total; ?></span></b></div>
				
<ul class="list-group">
	<li class="list-group-item bg-white" style="color:orange"><b>INTERVENTIONS EN COURS : <span class="badge-warning pull-right"><?= $nb_inter_en_cour; ?></span></b></li>
	<li class="list-group-item text-yellow bg-white"><b>INTERVENTIONS DIFFÉRÉES : <span class="badge-yellow pull-right" style="color:black"><?= $nb_inter_differee; ?></span></b></li>
	<li class="list-group-item text-info bg-white"><b>VEHICULE EN RECONNAISSANCE : <span class="badge-info pull-right"><?= $nb_inter_en_reco; ?></span></b></li>
</ul>

<div class='panel-heading bg-apple text-center'><b>PERSONNELS</b></div>
				
<ul class="list-group">
	<li class="list-group-item text-success bg-white"><b>PERSONNELS EN CASERNE : <span class="badge-success pull-right"><?= $perso_c; ?></span></b></li>
	<li class="list-group-item bg-white" style="color:red"><b>PERSONNELS EN INTER : <span class="badge-danger pull-right" style="padding:3px 8px;"><?= $perso_i; ?></span></b></li>
</ul>