<?php
require_once('../../init.php');
header('Content-type: application/json');
if( isset($_POST['num']) ){
    $codis = intval($_POST['num']);
    $id = Intervention::searchCodis($codis);
    $ret = array('msg' => "ok" ,'id' => $id);
} else {
    $ret = array('msg' => "Aucun numéro codis reçu !");
}
echo json_encode($ret);
?>