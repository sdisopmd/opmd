<?php
	require_once('../../init.php');

	if(isset($_GET['id'])) {
		$id = $_GET['id'];
		$i = new Intervention($id);
		
		if($i->notExists()) {
			echo "alert('Intervention n° {$id} inexistante');";
			exit;
		}
		
		if(!$i->isDifferee() && $i->getVehicules() == null) {
			echo "if(confirm('Intervention terminée. Souhaitez-vous la relancer ?')){
			var titre = \"FEUILLE DE DEPART\";
			 creerModalDiff('depart', titre, 'feuille_depart', 'div_feuille_depart.php?change={$id}', '','bg-danger',true);
			
			}";
			exit;
		}
		
		echo "var titre = \"FEUILLE DE DEPART\";
			 creerModalDiff('depart', titre, 'feuille_depart', 'div_feuille_depart.php?change={$id}', '','bg-danger',true);";
	}
?>
