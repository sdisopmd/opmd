<?php
require_once('../../init.php');
header('Content-type: application/json');
$id = $_POST['id'];
$perm = new Permanence($id);
$deb_perm = $perm->getDebut();
$fin_perm = $perm->getFin();
$duree = "";
$deb = "<b>". dateD($deb_perm) ." - ". dateH2($deb_perm)."</b>";
$fin = ($fin_perm == null) ? "" : "<b>". dateD($fin_perm) ." - ". dateH2($fin_perm)."</b>";
if ( $fin != "" ) {
	$datetime1 = new DateTime(dateDH3($deb_perm));
	$datetime2 = new DateTime(dateDH3($fin_perm));
	$duree = $datetime1->diff($datetime2);
	$duree = "<b>".$duree->format('%hh-%imin')."</b>";
} 
echo json_encode(array("deb" => $deb,"fin" => $fin,"duree" => $duree));
?>