<?php
require_once('../../init.php');

if(isset($_GET['id']) && isset($_GET['dt'])) {
	if($_GET['dt'] != ((string)intval($_GET['dt']))) {
		echo "alert('Paramètre incorrect.');";
	} else {
		$p = new Personnel($_GET['id']);
		if($p->isEnIntervention()) {
			echo 'notif("Action impossible : personne en intervention.","danger","","");';
		} else {
			$p->repart($_GET['dt']);
		}
	}
}
	
?>
