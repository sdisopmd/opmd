<?php
require_once('../../init.php');
header('Content-type: application/json');
$tab = null;
$id = $_POST['id'];
$ope = $_POST['operation'];
$mat = New Materiel($id);
$total = $mat->getQuantite();
$util = $mat->getUtiliser();
$dispo = $mat->getDispo();
if( $ope == "plus" ){
	if( $util < $total ){
		$util = $utils + 1;
		$dispo = $dispo - 1;
		$tab = array("rep" => "ok", "dispo" => $dispo);
	} else {
		$tab = array("rep" => "no", "dispo" => 0);
	}
} else {
	if( $dispo == $total ){
		$tab = array("rep" => "no", "dispo" => "total");
	} else {
		$util = $utils - 1;
		$dispo = $dispo + 1;
		$tab = array("rep" => "ok", "dispo" =>$dispo);
	}
}
$mat->setUtiliser($util);
$mat->setDispo($dispo);
$mat->commit();
$ret = $tab;
echo json_encode($ret);

?>