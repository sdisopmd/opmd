<?php
require_once('../../init.php');
header('Content-type: application/json');
if(isset($_GET['diff'])) {
	$inter = new Intervention($_GET['diff']);
	if($inter->exists()) {
		$inter->setStatus(1);
		$inter->setEtat(3);
		$inter->setMateriels(array());
		$inter->setVehicules(array());
    Materiel::updateInter(null, $inter->getId());
		$inter->setDifferee(1);
    $inter->setPresenceExterne(array());
		$inter->commit();
	}
	echo json_encode(array("retour" => "ok"));
}
	
?>
