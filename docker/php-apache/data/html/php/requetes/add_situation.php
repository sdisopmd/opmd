<?php
require_once('../../init.php');
header('Content-type: application/json');

if ( !isset($_SESSION['users']) ){
	$serveur = "Location: http://".$_SERVER['SERVER_NAME'];
	header($serveur);
} else {
	$rep = array("msg" => "erreur");
	if ( isset($_POST['id']) && $_POST['id'] == -1 ){
		// cas ou on ajoute
		$id = $_POST['id'];
		$date = $_POST['date'];
		$centre = $_POST['centre'];
		$datas = new DataSituation();
		$datas->setInters($_POST['tabInters']);
		$datas->setMoyens($_POST['tabMoyens']);
		$datas->setPersonnes($_POST['tabPersonnes']);
		$datas->setAnnimaux($_POST['tabAnnimaux']);
		$datas->setSites($_POST['tabSites']);
		$d_personnes = null;
		$d_annimaux = null;
		$d_sites = null;
		$d_moyen = null;
		$vide = null;
		if(empty($datas->getPersonnes())){
			$d_personnes = false;
		}
		if(empty($datas->getAnnimaux())){
			$d_annimaux = false;
		}
		if(empty($datas->getSites())){
			$d_sites = false;
		}
		if(empty($datas->getMoyens()[3]) && empty($datas->getMoyens()[4])){
			$d_moyen = false;
		}
		if($d_personnes == false && $d_annimaux == false && $d_sites == false){
			$vide = true;
		}
		if($vide == true){
			$rep = array("msg" => "no");
		} else {
			$si = new Situation($id);
			$si->setCreer($date);
			$si->setCentre($centre);
			$si->setData($datas);
			$si->commit();
			$rep = array("msg" => "ok");
		}
	}

	echo json_encode($rep);
}
?>