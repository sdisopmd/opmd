<?php
require_once('../../init.php');require_once('../../init.php');
header('Content-type: application/json');
if(isset($_GET['dep'])) {
	$inter = new Intervention($_GET['dep']);
	if($inter->exists()) {
		$id = $inter->getId();
		$inter->setFin(0);
		$inter->setEtat(1); // etat en cours
		$inter->setStatus(1); // status inter
		$mat = $inter->getMateriels();
    Debug::D($mat);
		$veh = $inter->getVehicules();
		if( !empty($mat) ) {
			foreach( $mat as $m ) {
				$m->setEtat(1);
				//$m->setIntervention($id);
				$m->commit();
			}
		}
		if( !empty($veh) ) {
			foreach( $veh as $v ) {
				$v->setEtat(1);
				$v->setIntervention($id);
				$v->commit();
			}
		}
		$inter->setDifferee(0);
		$inter->commit();
	}
	echo json_encode(array("retour" => "ok"));
}
	
?>
