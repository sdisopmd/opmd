<?php
require_once('../../init.php');
header('Content-type: application/json');
if(isset($_GET['reco'])) {
	$inter = new Intervention($_GET['reco']);
	if($inter->exists()) {
		$id = $inter->getId();
		$inter->setFin(0);
		$inter->setStatus(3); // status vl_reco
		$inter->setEtat(2); // etat reco
		$veh = $inter->getVehicules();
		if( !empty($veh) ) {
			foreach( $veh as $v ) {
				$v->setEtat(3);
				$v->setIntervention($id);
				$v->commit();
			}
		}
		$inter->setMateriels(array());
    Materiel::updateInter(null, $inter->getId());
		$inter->setDifferee(0);
    $inter->setPresenceExterne(array());
		$inter->commit();
	}
	echo json_encode(array("retour" => "ok"));
}

	
?>
