<?php
require_once('../../init.php');

/* 
Rappel etat 
0 : aucun
1 : En cours
2 : Reconnaissance
3 : Differee
4 : Terminer
5 : Annuler
 */


if(isset($_POST['type'])) {
	
	$type = $_POST['type'];
	$tabIds = $_POST['id'];
	$code = null;
	
	if ( $type == 'cour' ) {
		foreach($tabIds as $id){
			$code .= "<button type='button' class='btn btn-warning espace btInter' onclick='splash_recap(".$id.")'>".$id."</button>";
		}
		echo $code;
	}
	
	if ( $type == 'perm' ) {
		$perms = Permanence::getCurrentPermanence();
		if ( $perms->getId() != -1 ){
			$code .= "<button type='button' class='btn btn-danger espace btInter' onclick='consult_permanence(".$perms->getId().")'>Permanence en cours N° ".$perms->getId()."</button>";
		}
		foreach($tabIds as $id){
			if ( $perms->getId() != $id ){
				$code .= "<button type='button' class='btn btn-danger espace btInter' onclick='consult_permanence(".$id.")'>".$id."</button>";
			} 
		}
		echo $code;
	}
	
	if ( $type == 'diff' ) {
		foreach($tabIds as $id){
			$code .= "<button type='button' class='btn btn-yellow espace btInter' onclick='splash_recap(".$id.")'>".$id."</button>";
		}
		echo $code;
	}
	
	if ( $type == 'perso' ) {
		$perso = Personnel::getListPersonnel();
		$nb = 1;
		$tab1 = null;
		$tab2 = null;
		$tab3 = null;
		$s = null;
		$code .= '<br><br>';
		$code .= '<form class="form-horizontal" role="form">';
		$code .= '<div class="form-group">';
		$code .= '<label for="recherche" class="col-sm-4 control-label">Rechercher un personnel</label>';
		$code .= '<div class="col-sm-7">';
		$code .= '<input type="text" class="form-control" id="recherche" placeholder="Entrer le nom d\'un personnel">';
		$code .= '</div>';
		foreach($perso as $p){
			//$code .= '<option value='.$p["id"].'>'.$p["nom"].'</option>';
			$c = '<li class="list-group-item personnel" id="id-'.$p["id"].'">'.$p["nom"].'</li>';
			$s[] = array('id' => $p['id'], 'name' => $p['nom']);
			if( $nb == 1 ){
				$tab1[] = $c;
				$nb++;
			} else if ( $nb == 2 ){
				$tab2[] = $c;
				$nb++;
			} else {
				$tab3[] = $c;
				$nb = 1;
			}
			//$code .= "<button type='button' class='btn btn-success espace' onclick='alert(".$id.")'>".$pers->getNomFormate()."</button>";
		}
		$code .= '</form>';
		$code .= '<br><br><br><br>';
		$code .= '<div class="row">';
		$code .= '<div class="col-sm-4">';
		$code .= '<ul class="list-group">';
		foreach($tab1 as $c1){
			$code .= $c1;
		}
		$code .= '</ul>';
		$code .= '</div>';
		$code .= '<div class="col-sm-4">';
		$code .= '<ul class="list-group">';
		foreach($tab2 as $c2){
			$code .= $c2;
		}
		$code .= '</ul>';
		$code .= '</div>';
		$code .= '<div class="col-sm-4">';
		$code .= '<ul class="list-group">';
		foreach($tab3 as $c3){
			$code .= $c3;
		}
		$code .= '</ul>';
		$code .= '</div>';
		$code .= '</div>';
		$code .= '<script> var p = '. json_encode($s).'; initPerso(p); </script>';
		echo $code;
	}
	
	
	if ( $type == 'reco' ) {
		foreach($tabIds as $id){
			$code .= "<button type='button' class='btn btn-info espace btInter' onclick='splash_recap(".$id.")'>".$id."</button>";
		}
		echo $code;
	}
	
	
}




?>