<?php
header('Content-type: application/json');
require_once('../../init.php');

if ( isset($_GET['type']) ){
	
	if( $_GET['type'] == "verif" ) {
		$codis = Intervention::verifCodis($_GET['id']); 
		$rep = array("reponse" => $codis);
	}

	if( $_GET['type'] == "lier" ) {
		$codis = trim($_GET['codis']);
		$tmp = explode(" ",$codis);
		$liste = null;
		$flag = true;
		foreach($tmp as $t){
			if(!empty($t)){
				$ret = Intervention::verifCodis($t);
				//echo $ret;
				if($ret != "ok"){
					$liste[] = $t;
					$flag = false;
				}
			}
		}
		$res = ($flag != true) ? "erreur" : "ok";
		$rep = array("reponse" => $res, "codis" => $liste);
	}
	
	if( $_GET['type'] == "valid" ) {
		$codis = Intervention::validCodis($_GET['id']);
		$rep = array("reponse" => $codis);
	}
	
	echo json_encode($rep);
}



?>