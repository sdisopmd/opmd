<?php

	if(isset($_POST['titre'])){
		$titre = $_POST['titre'];
	}
	
	if(isset($_POST['texte'])){
		$texte = $_POST['texte'];
	}
	
	if(isset($_POST['date'])){
		$date = $_POST['date'];
	}

?>

<div class='modal fade' id='confirm' tabindex='-1' role='dialog' aria-labelledby='dial-confirm' aria-hidden='true'>
	  <div class='modal-dialog modal-lg'>
	    <div class='modal-content' style='border-radius:20px;'>
	      <div class='modal-header bg-danger' style='border-radius:20px;'>
	        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
	        <h1 class='modal-title text-center' id='dial-confirm'><?= $titre; ?></h1>
	      </div>
	      <div class='modal-body'>
	      	
			<div id='confirmSuppr'>
			
			<form class='form-horizontal' role='form' id='form-confirm' method='POST' onsubmit='return false'>

				<div class='form-group'>
					<div class='col-sm-12'>
						<h3 class='text-center'><?= $texte; ?></h3>
						<?php if( isset($date) ) : ?>
						<h2 class="text-center text-danger"><?= $date; ?></h2>
						<?php endif; ?>
					</div>
				</div>

				<div class='form-group'>
					<div class='col-sm-offset-4 col-sm-2'>
					  <button type='submit' class='btn btn-primary' data-dismiss='modal' id="del-annuler">Annuler</button>
					</div>
					<div class='col-sm-2'>
					  <button type='submit' class='btn btn-danger' id='del-confirm'>Confirmer</button>
					</div>
				</div>

			</form>
				
			</div>
			
	
	      </div>
	    </div>
	  </div>
	</div>
