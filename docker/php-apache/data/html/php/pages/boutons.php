<?php

require_once('../../init.php');

/* 
Rappel etat 
0 : aucun
1 : En cours
2 : Reconnaissance
3 : Differee
4 : Terminer
5 : Annuler
*/


?>

<!-- Intervention en cours -->
<?php if($etat == 1) : ?>
<div class="row">
	<div class="col-sm-2 col-sm-offset-3"><button type="button" onclick="modif(<?= $id; ?>,<?= $etat; ?>);" class="btn btn-success">MODIFIER</button></div>
	<div class="col-sm-2"><button type="button" onclick="diff(<?= $id; ?>,<?= $etat; ?>);" class="btn btn-warning">DIFFERER</button></div>
	<div class="col-sm-2"><button type="button" onclick="term(<?= $id; ?>,<?= $etat; ?>);" class="btn btn-danger">CLOTURE</button></div>
</div>
<?php endif; ?>

<!-- Intervention différée -->
<?php if($etat == 3) : ?>
<div class="row">
	<div class="col-sm-2 col-sm-offset-2"><button type="button" onclick="modif(<?= $id; ?>,<?= $etat; ?>);" class="btn btn-warning">MODIFIER</button></div>
	<div class="col-sm-2"><button type="button" onclick="reco(<?= $id; ?>,<?= $etat; ?>);" class="btn btn-info">VL RECO</button></div>
	<div class="col-sm-2"><button type="button" onclick="annul(<?= $id; ?>,<?= $etat; ?>);" class="btn btn-danger">ANNULER</button></div>
</div>
<?php endif; ?>

<!-- Intervention avec VL reconnaissance -->
<?php if($etat == 2) : ?>
<div class="row">
	<div class="col-sm-2 col-sm-offset-2"><button type="button" onclick="modif(<?= $id; ?>,<?= $etat; ?>);" class="btn btn-info">MODIFIER</button></div>
	<div class="col-sm-2"><button type="button" onclick="diff(<?= $id; ?>,<?= $etat; ?>);" class="btn btn-warning">DIFFERER</button></div>
	<div class="col-sm-2"><button type="button" onclick="annul(<?= $id; ?>,<?= $etat; ?>);" class="btn btn-danger">ANNULER</button></div>
</div>
<?php endif; ?>


