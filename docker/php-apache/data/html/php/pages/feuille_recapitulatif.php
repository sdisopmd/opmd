<?php
require_once('../../init.php');

if ( !isset($_SESSION['users']) ){
	$serveur = "Location: http://".$_SERVER['SERVER_NAME'];
	//echo $serveur;
	header($serveur);
} else {

$date_actu = date("d") . "/" . date("m") . "/" . date("Y");
$heure_actu = date("H") . "H" . date("i");

$compteur = Intervention::compteurInter();
$nb_it_actu = $compteur['enCour'];
$nb_inter_en_reco = $compteur['vlreco'];
$nb_it_diff = $compteur['diff'];
$it_ann = $compteur['annul'];
$it_ter = $compteur['term'];
$total = $compteur['total'];
$total_inter =  $total + $it_ann + $it_ter;
$nb_pe = (Permanence::getCurrentPermanence()->getId() == -1) ? 0 : 1;

$interList = Intervention::listInterventions();
$tabInter = array();
foreach($interList as $intervention){
	$it = new Intervention($intervention->id);
	array_push($tabInter,$it);
}

$centreList = Centre::listCentres();
$tabCentre = array();
foreach($centreList as $centres){
	$center = new Centre($centres->id);
	array_push($tabCentre,$center);
}

$natureList = NatureIntervention::getAllNatures();

$communeList = Commune::getAllCommunes();

$tabMois = array("Janvier" => 1, "Février" => 2, "Mars" => 3, "Avril" => 4, "Mai" => 5, "Juin" => 6, "Juillet" => 7, "Août" => 8, "Septembre" => 9, "Octobre" => 10, "Novembre" => 11, "Décembre" => 12)

?>


<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Feuille récapitulatif</title>

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" type="text/css" href="/libs/bootstrap/css/bootstrap.css" />
		<link rel="stylesheet" type="text/css" href="/libs/bootstrap/css/bootstrap-multixvers.css" />

		<!-- Unixvers style -->
		<link rel="stylesheet" href="/<?= CSS; ?>/multixvers.css">
		
		<link rel="shortcut icon" href="/favicon.ico">
		

	</head>

	<body id="fondSite">

		<div class="container-fluid">

			<!-- titre-->

			<div class="row">
				<div class="col-md-1">
					<img class="decale" src="/<?= IMG; ?>/pucelle.png" alt="logo" style="width:100px;"/>
				</div>
				<div class="col-md-1">

				</div>
				<div class="col-md-8">

					<div class="panel panel-default decale">
						<div class="panel-body">
							<form class="form-horizontal" role="form">
								<div class="form-group">
									<label for="centre" class="col-sm-4 control-label" style="font-size:36px">Centre de : </label>
									<div class="col-sm-6" style="margin-top:18px">
										<select class="form-control" name="centre">
											<?php foreach($tabCentre as $centre) : ?>
										    <option><?= $centre->getLibelle() ; ?></option>
										    <?php endforeach; ?>
										</select>
									</div>
								</div>
							</form>
						</div>
					</div>

				</div>
				<div class="col-md-2"></div>
				
			</div>

			<!-- /titre-->

			<!-- visa cdc-->

			<div class="row">

				<div class="col-md-offset-2 col-md-2">

					<div class="panel panel-info visa">
						<div class="panel-heading">
							<h3 class="panel-title text-center"><b>Visa Chef de Centre</b></h3>
						</div>
						<div class="panel-body" style="height:105px">

						</div>
					</div>

				</div>
				
			</div>

			<!-- /visa cdc-->

			<!-- tableau-->

			<div class="row">
				<div class="col-md-12">
					<div class="table-responsive">
					<table class="table table-bordered table-hover table-striped">
						<thead>
							<tr class="bg-info">
								<th class="text-center">N° INTER</th>
								<th class="text-center" style="width:70px">ETAT</th>
								<th class="text-center">DATE</th>
								<th class="text-center">DEPART</th>
								<th class="text-center">RETOUR</th>
								<th class="text-center">DUREE</th>
								<th class="text-center">NATURE</th>
								<th class="text-center">PRECISION</th>
								<th class="text-center">COMMUNE</th>
								<th class="text-center">ADRESSE</th>
								<!-- <th class="text-center">VEHICULE(s)</th>
								<th class="text-center">EFFECTIF(s)</th> -->
							</tr>
						</thead>

						<tbody>
							
							<?php foreach( $tabInter as $inter) : ?>

							<tr id="inter-<?= $inter->getId(); ?>" onclick="alert(this.id.substr(6));">
								<td class="text-center" style="vertical-align:middle;"> <?= $inter->getId(); ?> </td>
								<td class="text-center" style="vertical-align:middle;"> 
									<?php 
										$nbEtat = $inter->getEtat();
										$status = $inter->recupEtat($nbEtat);
										$class = array("default","success","info","warning","danger","purple","violet");
										echo "<span class='label label-{$class[$nbEtat]}'>{$status}</span>";
									?> 
								</td>
								<td class="text-center" style="vertical-align:middle;"> <?= ($inter->getDateHeure()) ? dateD($inter->getDateHeure()) : "Aucune"; ?> </td>
								<td class="text-center" style="vertical-align:middle;"> <?= ($inter->getDateHeure()) ? dateH2($inter->getDateHeure()) : "Aucun départ"; ?> </td>
								<td class="text-center" style="vertical-align:middle;"> <?= ($inter->getFin()) ? dateH2($inter->getFin()) : "Aucun retour"; ?> </td>
								<td class="text-center" style="vertical-align:middle;"> 
									<?php
										if($inter->getFin()) {
											$datetime1 = new DateTime(dateDH3($inter->getDateHeure()));
											$datetime2 = new DateTime(dateDH3($inter->getFin()));
											$interval = $datetime1->diff($datetime2);
											echo $interval->format('%hh-%imin');
										} else {
											echo "En cour"; 
										} 
									
									?> 
								</td>
								<td class="text-center" style="vertical-align:middle;"> <?= ($inter->getNature()) ? $inter->getNature()->getLibelle() : "Aucune"; ?> </td>
								<td class="text-center" style="vertical-align:middle;"> <?= ($inter->getPrecision()) ? $inter->getPrecision()->getLibelle() : "Aucune"; ?> </td>
								<td class="text-center" style="vertical-align:middle;"> 
								<?php
									if( $inter->getCommune()->getId() != -1) {
										$commune = $inter->getCommune();
										$code = $commune->getCodePostal();
										$nom = $commune->getLibelle();
										echo $code. " ". $nom;
									} else {
										 echo "Aucune"; 
									}
								?> 
								</td>
								<td class="text-center" style="vertical-align:middle;"> 
								<?php 
									if($inter->getAdresse()) {
										$num = ($inter->getNumeroRue()) ? "N° ".$inter->getNumeroRue() : "";
										$rue = ($inter->getAdresse()) ? " ".$inter->getAdresse() : "";
										$appart = ($inter->getAppartement()) ? "<br> Appartement ". $inter->getAppartement() : "";
										$etage = ($inter->getEtage()) ? " - étage : ". $inter->getEtage() : "";
										$ensemble = $num . $rue . $appart . $etage;
										if( $ensemble != "" ) {
											echo $ensemble;
										} else {
											echo "Aucune";
										}
										
									}  else {
										echo "Aucune"; 
									}
								?> 
								</td>
								<!-- <td class="text-center" style="vertical-align:middle;"> 
									<?php 
										if($inter->getVehicules(true)){
											foreach( $inter->getVehicules() as  $v){
												echo $v->getLibelle()." ";
											}
											//debug($inter->getVehicules());
										} else {
											echo "Aucun"; 
										}
									
									?> 
								</td>
								<td class="text-center" style="vertical-align:middle;"> 
									<?php 
								
										if($inter->getPersonnel(true)) {
											foreach($inter->getPersonnel() as $personne){
												//debug($personne);
												$nom = $personne->getNom();
												$grade = $personne->getGrade()->getAbreviation();
												echo "<b>".$grade."</b>"." ". $nom." ";
											}
											
											
										} else {
											echo "Personne"; 
										}
											
								
								?> </td> -->
							</tr>
							
							<?php endforeach; ?>
								
							

						</tbody>

					</table>
					</div>
				</div>
			</div>

			<!-- /tableau-->

		</div>
		
		<!-- Modal -->
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		  <div class="modal-dialog modal-lg">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		        <h4 class="modal-title" id="myModalLabel">FEUILLE DE DEPART</h4>
		      </div>
		      <div class="modal-body">
		      	 
		      </div>
		    </div>
		  </div>
		  
		</div>


		<!-- Bootstrap JavaScript -->
		<script type="text/javascript" src="/<?= JQUERY; ?>/jquery-2.1.0.js"></script>
		<script type="text/javascript" src="/<?= BOOTSTRAP_JS; ?>/bootstrap.js"></script>
		
		<script type="text/javascript" charset="utf-8">
			$(document).ready(function(){
				
				$(".visa").hide();
				
				$("#choix-perm").on('change',function(){
					var idp = $(this).val();
					$.post('/php/requetes/recup_perm.php', { id : idp }).always( function(arg){
						$("#p-deb").empty().html(arg.deb);
						$("#p-fin").empty().html(arg.fin);
						$("#p-time").empty().html(arg.duree);
					});
				});
				
				
				function affInter(id){
					
				}
				
			});
		</script>
		
		
		<style>
		.dropdown-menu > li > a:hover, .dropdown-menu > li > a:focus {
			background-image: -webkit-linear-gradient(top, #F6F697 0%, #F5EE4F 100%);
			background-image: linear-gradient(to bottom, #F6F697 0%, #F5EE4F 100%);
			background-repeat: repeat-x;
			filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffF6F697', endColorstr='#ffF5EE4F', GradientType=0);
			background-color: #F5EE4F;
		}
		</style>

	</body>

</html>

<?php
}
?>