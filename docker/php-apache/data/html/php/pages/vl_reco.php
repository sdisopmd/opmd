<?php

	require_once('../../init.php');

	$vl = Intervention::getInterventionsEnReconnaissance();

	if($vl != null) {
	?>

<div class="table-responsive">
<table class="table table-bordered" border="2" cellspacing="2" cellpadding="2" style="empty-cells:show;" id="table">
	<thead>
		<tr class="bg-info">
			<th class="redtitle text-middle">N° CODIS</th>
			<th class="redtitle text-middle">Date - heure</th>
			<th class="redtitle text-middle">Lieu / Précision</th>
			<th class="redtitle text-middle">Véhicules / Personnel</th>
			<th class="redtitle text-middle"<?php echo " colspan='{$nb_col}'"; ?>>Actions</th>
		</tr>
	</thead>
	
	<tbody>
	<?php
		foreach($vl as $inter) {
		
			$com = $inter->getCommune();
			$pes = $inter->getPresencesExternes();
			$statusTexte = $inter->getStatus()->getCouleurTexte();
			$statusFond = $inter->getStatus()->getCouleurFond();
			$styleInter = "color:{$statusTexte};background-color:{$statusFond};";
			$codisLier = explode(" ",$inter->getCodisLier());
			
	?>
			<tr class="bg-default">
				<td class="text-center text-middle">
					<input type="button" value="<?php echo $inter->getId(); ?>" onclick="splash_recap(<?php echo $inter->getId(); ?>);" style="font-weight:bold;<?= $styleInter; ?>" class="btn espace" />
					<?php if(!empty($codisLier[0])) : ?> 
					<br>
						<?php foreach($codisLier as $num) : ?> 
							<input type="button" value="<?php echo $num; ?>" onclick="" style="font-weight:bold;background-color:black;color:white;" class="btn espace" />
						<?php endforeach; ?>
					<?php endif; ?> 
				</td>
				<td class="text-center text-middle"><?php echo date('d/m/y à Hi', $inter->getDateHeure()); ?></td>
				<td class="text-center text-middle"><?php echo $com->getLibelle().(strlen($inter->getAdresse()) ? " – {$inter->getAdresse()}" : '').'<br />&nbsp;&nbsp;'.$inter->getPrecision()->getLibelle(); ?></td>
				<td class="text-center text-middle"><?php
						$vs = $inter->getVehiculesVisibles();
						for($i=0 ; $i<count($vs) ; $i++) {
							$v = $vs[$i];
							//if($i%4 == 0 && $i != 0) echo '<br /><br style="line-height:2px;" />&nbsp;&nbsp;';
							$etat_v = $v->getEtat();
							$style = "background-color:{$etat_v->getCouleurFond()}; color:{$etat_v->getCouleurTexte()};";
							echo "<div style='{$style}'";
							echo " onclick='affectationVehicule({$v->getId()});' onmouseup='document.oncontextmenu = rightClick;' data='veh-{$v->getId()}'";
							echo " class='arrondi spaceT sizeT'> {$v->getLibelle()} {$v->getCentre()->getAbreviation()}</div>";
						}
			
						$ps = $inter->getPersonnel();
						for($i=0 ; $i<count($ps) ; $i++) {
							if($i == 0) echo "<br style='clear:both'><hr/>";
							$p = $ps[$i];
							//if($i%4 == 0 && $i != 0) echo '<br /><br style="line-height:2px;" />&nbsp;&nbsp;';
							$couleur = $p->getCouleur();
							$texte = $p->getTextCouleur();
							$style = "background-color:{$couleur};color:{$texte}";
							echo "<div style='{$style}'";
							echo "class='arrondi spaceT sizeT'> {$p->getGrade()->getAbreviation()} {$p->getNom()}</div>";
						}
					?>
				</td>
				<td class="text-center text-middle" onclick="radio(<?= $inter->getId(); ?>);">RELÈVE</td>
				<td class="text-center text-middle"><img onclick="print_recap(<?php echo $inter->getId(); ?>);" src="<?= IMG; ?>/print16.png" /><br><br><img data="<?php if( !empty($com->getCodePostal()) ) { echo  $com->getCodePostal(); } else { echo 52000; } ?>" class="mapi" src="<?= IMG; ?>/map24.png" /></td>
			</tr>
<?php
		
		}
	
?>
	</tbody>
</table>
</div>



<?php
	}
?>