
<div class="table-responsive">

<?php

	require_once('../../init.php');

	$pe = Permanence::getCurrentPermanence();
	$debut_form =  dateDH2($pe->getDebut());
	$fin_form = dateDH2($pe->getFin());
	$id = $pe->getId();

	$hs = $pe->getHorairesRenforts();

		if($hs != null) { ?>
		<h3 class='text-center'>PERMANENCE CI N° <b style="color:red"><?= $id; ?></b></h3>
			<table class='table table-bordered table-striped '>
						<thead>
							<tr style='font-weight:bold;' class="bg-info">
								<th class="text-center">PERSONNEL</th>
								<th colspan="2" class="text-center">DÉBUT</th>
								<th colspan="2" class="text-center">FIN</th>
								<th class="text-center">TOTAL</th>
							</tr>
							<tr style='font-weight:bold;' class="bg-info">
								<th>GRADE NON Prénom (CENTRE)</th>
								<th>DATE</th>
								<th>HEURE</th>
								<th>DATE</th>
								<th>HEURE</th>
								<th>DUREE</th>
							</tr>
						</thead>
						
						<tbody>
			
			<?php
			for($i=0 ; $i<count($hs) ; $i++) {
			
				$h = $hs[$i];
				$nom = $h->getPersonnel()->getNomFormate();
				// date-heure arrivée
				$arrivee = ($h->getArrivee() < $pe->getDebut() ? $pe->getDebut() : $h->getArrivee());
				$dateD = dateD2($arrivee);
				$heureD = dateH2($arrivee);
				// date-heure fin
				$depart = ($h->getDepart() > $pe->getFin() ? ($pe->getFin() == null ? $h->getDepart() : $pe->getFin()) : $h->getDepart());
				//$fin = $h->getDepart();//$h->getDepart() != null ? ($dateD == dateD2($depart) ? dateH3($depart) : dateD2H3($depart)) : '';
				$dateF = dateD2($depart);
				$heureF = dateH2($depart); 
				$total = "";
				//debug(array("depart"=>$depart));
				//debug(array("arrivee"=>$arrivee));
				if($h->getDepart() != null ) {
					$datetime1 = new DateTime(dateDH3($arrivee));
					$datetime2 = new DateTime(dateDH3($depart));
					$interval = $datetime1->diff($datetime2);
					$affH = $interval->format('%hh-%imin');
					$total = ( substr($affH,0,2) == "0h" ) ? substr($affH,3) : $affH;
				} else {
					$total = "En cour"; 
				} 
				
				?>
				<tr>
						<td><?= $nom; ?></td>
						<td><?= $dateD; ?></td>
						<td><?= $heureD; ?></td>
						<td><?= $dateF; ?></td>
						<td><?= $heureF; ?></td>
						<td><?= $total; ?></td>
					</tr>
	<?php } ?>
				</tbody>
			</table>
	
		
	<?php } ?>
	</div>
