<?php
require_once('../../init.php');
header('Content-type: application/json');

if(isset($_POST['type'])) {
	
	$type = $_POST['type'];
	$all = null;
	
	if ( $type == 'cour' ) {
		$inter = Intervention::getAllInterByEtat(1);
		createListId($inter,$type);
	}
	
	if ( $type == 'perm' ) {
		$perms = Permanence::getAllPermanences();
		createListId($perms,$type);
	}
	
	if ( $type == 'diff' ) {
		$inter = Intervention::getAllInterByEtat(3);
		createListId($inter,$type);
	}
	
	if ( $type == 'perso' ) {
		$perso = Personnel::getAllPersonnel();
		createListId($perso,$type);
	}
	
	
	if ( $type == 'reco' ) {
		$inter = Intervention::getAllInterByEtat(2);
		createListId($inter,$type);
	}
	
}

function createListId($objet,$type){
	
	$all = null;
	if ( $objet != null ) {
		foreach( $objet as $obj ) {
			$all[] = $obj->getId();
		}
		$retour = json_encode(array( 'type' => $type ,'id' => $all));
	} else {
		$retour = json_encode(array( 'type' => $type ,'id' => "aucun"));
	}
	echo $retour;
}


?>