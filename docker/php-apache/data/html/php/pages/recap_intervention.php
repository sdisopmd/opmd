<?php
require_once('../../init.php');

$inter = new Intervention(@$_GET['id']);

if($inter->getId() != -1) {
	$infos = array();
	$infos[] = "Intervention {$inter->getId()}";
	$infos[] = date('\L\e d/m/y à Hi', $inter->getDateHeure());
	$infos[] = trim("{$inter->getPrenomAppelant()} {$inter->getNomAppelant()}");
	$infos[] = trim("{$inter->getNumeroRue()} {$inter->getAdresse()}");
	$infos[] = trim((strlen($inter->getEtage()) ? "Etage {$inter->getEtage()}" : '').' '.(strlen($inter->getAppartement()) ? "Appt {$inter->getAppartement()}" : ''));
	$commune = $inter->getCommune();
	$infos[] = trim("{$commune->getCodePostal()} {$commune->getLibelle()}");
	$infos[] = (strlen($inter->getTelephoneAppelant()) ? "Tél. : {$inter->getTelephoneAppelant()}" : '');
	$infos[] = (strlen($inter->getComplement()) ? "Comp. d'info. : {$inter->getComplement()}" : '');
	$infos[] = "Pour : {$inter->getPrecision()->getLibelle()}";
	$infos[] = (strlen($inter->getMessageRadio()) ? 'Messages : <div style="display:inline-block; vertical-align:top;">'.str_replace("\n", '<br />', $inter->getMessageRadio()).'</div>' : '');
	
	foreach($infos as $info) {
		if(strlen($info)) {
			echo $info.'<br />';
		}
	}
}

?>