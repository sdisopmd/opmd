<?php
require_once('../../init.php');
createTable();

function createTable(){
	
	echo '<div class="table-responsive">';
		echo '<table class="table table-bordered">';
			echo '<thead>';
				echo '<tr class="bg-info">';
					echo '<th class="redtitle text-middle">N° CODIS</th>';
					echo '<th class="redtitle text-middle">Date - heure</th>';
					echo '<th class="redtitle text-middle">Lieu / Précision</th>';
				echo '</tr>';
			echo '</thead>';
		
			echo '<tbody>';

				$inters = Intervention::getAllInterventionsDifferees();
				contentTable($inters);
				
			echo '</tbody>';
		
		echo '</table>';
	
	echo '</div>';

}

function contentTable($interventions){
	
		if( $interventions != null ){
			foreach($interventions as $inter) {
				$statusTexte = $inter->getStatus()->getCouleurTexte();
				$statusFond = $inter->getStatus()->getCouleurFond();
				$styleInter = "color:$statusTexte;background-color:$statusFond;";
			    $codisLier = explode(" ",$inter->getCodisLier());
			
				echo '<tr class="bg-default">';
					echo '<td class="text-center text-middle">';
						echo '<input type="button" value="'.$inter->getId().'" onclick="splash_recap('.$inter->getId().');" style="font-weight:bold;'.$styleInter.'" class="btn espace" />';
						if(!empty($codisLier[0])) {
							echo '<br>';
							foreach($codisLier as $num) {
								echo '<input type="button" value="'.$num.'" onclick="" style="font-weight:bold;background-color:black;color:white;" class="btn espace" />';
							}
						}
					echo '</td>';
					echo '<td class="text-center text-middle">'.date('d/m/y à Hi', $inter->getDateHeure()).'</td>';
					echo '<td class="text-center text-middle">';
					
						$libelleR = $inter->getAdresse();
						$libelleC = $inter->getCommune()->getLibelle();
						$libelleP = $inter->getPrecision()->getLibelle();
						if( strlen($libelleR) ){
							echo $libelleC. " - ".$libelleR;
						} else {
							echo $libelleC;
						}
						echo '<br />&nbsp;&nbsp;'.$libelleP;
					
					echo '</td>';
				echo '</tr>';
			}
		}
}

?>


