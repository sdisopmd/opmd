<?php
require_once('../../init.php');

if ( !isset($_SESSION['users']) ){
	$serveur = "Location: http://".$_SERVER['SERVER_NAME'];
	header($serveur);
} else {

	if ( isset($_POST['id']) && $_POST['id'] != -1 ) {
		// cas ou on affiche une demande
		$id = $_POST['id'];
		$s = new Situation($id);
		$date = $s->getCreer();
		$centre = new Centre($s->getCentre());
		$tabCentre = array($centre);
		$data = $s->getData();
		$inters = $data->getInters();
		$moyens = $data->getMoyens();
		$personnes = $data->getPersonnes();
		$animaux = $data->getAnnimaux();
		$sites = $data->getSites();
		$nb_it_actu = $inters[0];
		$nb_it_diff = $inters[1];
		$nb_it_ann = $inters[2];
		$nb_it_ter = $inters[3];
		$off = $moyens[0];
		$soff = $moyens[1];
		$hdr = $moyens[2];
		$sssm = $moyens[3];
		$vehicules = $moyens[4];
		$p_sauv = $personnes[0];
		$p_secu = $personnes[1];
		$p_prec = $personnes[2];
		$a_sauv = $animaux[0];
		$a_secu = $animaux[1];
		$a_prec = $animaux[2];
	} else {
		// cas normal
		$date = Situation::getDate();
		$off = Personnel::getNbOfficiersCaserne();
		$soff = Personnel::getNbSousOfficiersCaserne();
		$hdr = Personnel::getNbHommesDuRangCaserne();
		$sssm = "";
		$vehicules = "";

		$compteur = Intervention::compteurInter();
		$nb_it_actu = $compteur['enCour'];
		$nb_inter_en_reco = $compteur['vlreco'];
		$nb_it_diff = $compteur['diff'];
		$nb_it_ann = $compteur['annul'];
		$nb_it_ter = $compteur['term'];
		$total = $compteur['total'];
		$total_inter =  $total + $nb_it_ann + $nb_it_ter;
		$nb_pe = (Permanence::getCurrentPermanence()->getId() == -1) ? 0 : 1;

		$p_sauv = "";
		$p_secu = "";
		$p_prec = "";
		$a_sauv = "";
		$a_secu = "";
		$a_prec = "";

		$sites = null;

		$centreList = Centre::listCentres();
		$tabCentre = array();
		foreach($centreList as $centres){
			$center = new Centre($centres->id);
			array_push($tabCentre,$center);
		}
		
	}


?>

<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>POINT DE SITUATION</title>

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" type="text/css" href="/libs/bootstrap/css/bootstrap.css" />
		<link rel="stylesheet" type="text/css" href="/libs/bootstrap/css/bootstrap-multixvers.css" />

		<!-- Unixvers style -->
		<link rel="stylesheet" href="/<?= CSS; ?>/multixvers.css">
		
		<link rel="shortcut icon" href="/favicon.ico">

		<style>
			body {
				overflow-x: hidden;
			}
		</style>

	</head>

	<body id="fondSite">

		<form class="form-horizontal" role="form" method="POST">
	
		<!-- titre -->
		<div class="row" style="margin-top:10px;margin-bottom:10px;">
			<div class="col-md-1 col-md-offset-1">
				<img src="/<?= IMG; ?>/pucelle.png" alt="Logo" style="width:100px;"/>
			</div>
			<div class="col-md-6">
				<h1 class="text-center">POINT DE SITUATION</h1>
			</div>

			<div class="col-md-4">
				<h3>
					N° <span id="id">0</span> FAIT le <span id="date"><?= $date; ?></span>
				</h3>
			</div>

		</div>
		<!-- /titre -->

		<!-- interventions -->
		<div class="row">
			<div class="col-md-1">

			</div>
			<div class="col-md-5">

				<table class="table table-bordered" style="width:600px">
					<thead>
						<tr class="bg-danger">
							<th class="text-center" colspan="4">INTERVENTIONS</th>
						</tr>
					
						<tr class="bg-info">
							<th class="text-center">En cours</th>
							<th class="text-center">Différé</th>
							<th class="text-center">Annulées</th>
							<th class="text-center">Terminées</th>
						</tr>
					</thead>

					<tbody>

						<tr>
							<td class="text-center inter-actu"><?= $nb_it_actu ?></td>
							<td class="text-center inter-diff"><?= $nb_it_diff ?></td>
							<td class="text-center inter-annul"><?= $nb_it_ann ?></td>
							<td class="text-center inter-term"><?= $nb_it_ter ?></td>
						</tr>

					</tbody>

				</table>
			</div>

			<div class="col-md-6">
				
					<div class="form-group">
						<label for="centre" class="col-sm-4 control-label" style="font-size:36px">Centre de : </label>
						<div class="col-sm-4" style="margin-top:18px">
							<select class="form-control" name="centre">
								<?php foreach($tabCentre as $centre) : ?>
							    <option value="<?= $centre->getId();?>"><?= $centre->getLibelle() ; ?></option>
							    <?php endforeach; ?>
							</select>
						</div>
						<input type="text" class="col-sm-4" hidden/>
					</div>
				
			</div>

		</div>
		<!-- /interventions -->

		<!-- MOYENS MOBILISES -->
		<div class="row">
			<div class="col-md-1">

			</div>
			<div class="col-md-10">
				
				<table class="table table-bordered">
					<thead>
						
						<tr class="bg-danger">
							<th class="text-center" colspan="5">MOYENS MOBILISES</th>
						</tr>
						<tr class="bg-info">
							<th class="text-center">OFFICIERS</th>
							<th class="text-center">SOUS OFFICIERS</th>
							<th class="text-center">CAPORAUX / SAPEURS</th>
							<th class="text-center">SSSM</th>
							<th class="text-center">Véhicules(type & nombre)</th>
						</tr>
					</thead>

					<tbody>

						<tr>
							<td class="text-center off"><b><?= $off; ?></b></td>
							<td class="text-center soff"><b><?= $soff; ?></b></td>
							<td class="text-center hdr"><b><?= $hdr; ?></b></td>
							<td class="text-center"><b><input type="text" name="sssm" class="text-center" value="<?= $sssm ?>"></b></td>
							<td class="text-center"><b><textarea rows="3" name="vehicules" wrap="hard" style="width:90%"><?= $vehicules ?></textarea></b></td>
						</tr>

					</tbody>

				</table>

			</div>
			<div class="col-md-1">

			</div>
		</div>
		<!-- / MOYENS MOBILISES -->

		<!-- INTERVENTIONS AU PROFIT DE PERSONNE -->
		<div class="row">
			<div class="col-md-1">

			</div>
			<div class="col-md-4">
				
				<table class="table table-bordered">
					<thead>
						
						<tr class="bg-danger">
							<th class="text-center" colspan="2">INTERVENTIONS AU PROFIT DE PERSONNE</th>
						</tr>
						<tr class="bg-info">
							<th class="text-center">Sauvetage(s)</th>
							<th class="text-center">Mise(s) en sécurité</th>
						</tr>
					</thead>

					<tbody>

						<tr>
							<td class="text-center"><input type="text" name="personne-sauvetage" class="text-center" value="<?= $p_sauv ?>"></td>
							<td class="text-center"><input type="text" name="personne-securite" class="text-center" value="<?= $p_secu ?>"></td>
						</tr>
						
						<tr class="text-center">
							<td colspan="2">A préciser</td>
						</tr>
						<tr class="text-center">
							<td colspan="2"><textarea rows="3" name="personne-precision" wrap="hard" style="width:90%"><?= $p_prec ?></textarea></td>
						</tr>

					</tbody>

				</table>
			</div>
			<div class="col-md-2">

			</div>
			<div class="col-md-4">
				
				<table class="table table-bordered">
					<thead>
						
						<tr class="bg-danger">
							<th class="text-center" colspan="2">INTERVENTION AU PROFIT D'ANNIMAUX</th>
						</tr>
						<tr class="bg-info">
							<th class="text-center">Sauvetage(s)</th>
							<th class="text-center">Mise(s) en sécurité</th>
							
						</tr>
					</thead>

					<tbody>

						<tr>
							<td class="text-center"><input type="text" name="animaux-sauvetage" class="text-center" value="<?= $a_sauv ?>"></td>
							<td class="text-center"><input type="text" name="animaux-securite" class="text-center" value="<?= $a_secu ?>"></td>
						</tr>
						
						<tr class="text-center">
							<td colspan="2">A préciser (chiens/chevaux...)</td>
						</tr>
						<tr class="text-center">
							<td colspan="2"><textarea rows="3" name="animaux-precision" wrap="hard" style="width:90%"><?= $a_prec ?></textarea></td>
						</tr>
					</tbody>

				</table>
			</div>
			<div class="col-md-1">

			</div>
		</div>

		<!-- /INTERVENTIONS AU PROFIT DE PERSONNE -->

		<!-- INTERVENTIONS AU PROFIT DE SITES SENSIBLES: ERP / ENTREPRISE IMPORTANTE / ER .... -->
		<div class="row">
			<div class="col-md-1">

			</div>
			<div class="col-md-10">
				<div class="table-responsive">
				<table class="table table-bordered">
					<thead>
						
						<tr class="bg-danger">
							<th class="text-center" colspan="3">INTERVENTIONS AU PROFIT DE SITES SENSIBLES: ERP / ENTREPRISE IMPORTANTE / ER <span class="pull-right" style="cursor:pointer" id="add-ligne"><span class="glyphicon glyphicon-plus"></span></span></th>
						</tr>
						<tr class="bg-info">
							<th class="text-center">Nom de l'établissement / Nature</th>
							<th class="text-center">Dégâts particuliers</th>
							<th class="text-center">Chômage technique (NB)</th>
						</tr>
					</thead>

					<tbody id="ligne">
						<?php if($sites == null) : ?>
						<tr>
							<td class="text-center"><textarea rows="3" name="nature[]" wrap="hard" style="width:90%"></textarea></td>
							<td class="text-center"><textarea rows="3" name="degat[]" wrap="hard" style="width:90%"></textarea></td>
							<td class="text-center"><textarea rows="3" name="chomage[]" wrap="hard" style="width:90%"></textarea></td>
						</tr>
						<?php else : ?>
							<?php foreach($sites as $s) : ?>
							<tr>
								<td class="text-center"><textarea rows="3" name="nature[]" wrap="hard" style="width:90%"><?= ( isset($s[0]) ) ? $s[0] : "" ?></textarea></td>
								<td class="text-center"><textarea rows="3" name="degat[]" wrap="hard" style="width:90%"><?= ( isset($s[1]) ) ? $s[1] : "" ?></textarea></td>
								<td class="text-center"><textarea rows="3" name="chomage[]" wrap="hard" style="width:90%"><?= ( isset($s[2]) ) ? $s[2] : "" ?></textarea></td>
							</tr>
							<?php endforeach; ?>
						<?php endif; ?>
					</tbody>

				</table>
				</div>
			</div>
			<div class="col-md-1">

			</div>
		</div>
		<!-- / INTERVENTIONS AU PROFIT DE SITES SENSIBLES: ERP / ENTREPRISE IMPORTANTE / ER -->
		
		<hr style="width:80%">
		
		<div class="form-group">
			<div class="col-sm-12 text-center">
				<button type="button" class="btn btn-success" id="save">Enregistrer</button>
				<button type="button" class="btn btn-info" id="search">Rechercher</button>
			</div>
		</div>
		
		</form>

		<div id="bloc-modal"></div>

		<!-- Bootstrap JavaScript -->
		<script type="text/javascript" src="/<?= JQUERY; ?>/jquery-2.1.0.js"></script>
		<script type="text/javascript" src="/<?= BOOTSTRAP_JS; ?>/bootstrap.js"></script>
		<script type="text/javascript" src="/<?= GRITTER_JS; ?>/gritter.js"></script>
		<script type="text/javascript" src="/<?= JS; ?>/modals.js"></script>
		<script type="text/javascript" src="/<?= JS; ?>/notif.js"></script>
		<script type="text/javascript" src="/<?= JS; ?>/utils.js"></script>

		<link rel="stylesheet" type="text/css" href="<?= URL.GRITTER_CSS; ?>/gritter.css"/>

		<script type="text/javascript">
		
			$(document).ready(function(e){
			
				$("#add-ligne").on('click', function(){
						
						var code = '<tr>';
							code += '<td class="text-center"><textarea rows="3" cols="73" name="nature[]" wrap="hard"></textarea></td>';
							code += '<td class="text-center"><textarea rows="3" cols="72" name="degat[]" wrap="hard"></textarea></td>';
							code += '<td class="text-center"><textarea rows="3" cols="72" name="chomage[]" wrap="hard"></textarea></td>';
						code += '</tr>';
						
						$('#ligne').append(code);
					
				});
			
				$("#save").on('click', function(){
				
					var data = recupData();
					$.post('/php/requetes/add_situation.php',data).always(function(reponse){
						if(reponse.msg == "ok"){
							notif("Le point de situation a bien été enregistré","success",5000);
						} else if(reponse.msg =="no"){
							notif("Le point de situation ne peut pas être enregistré avec des données vide","danger",5000);
						} else {
							notif("Une erreur est survenue lors du traitement","danger",5000);
						}
					});
				
				});

				$("#search").on('click', function(){
					viderModal();
					creerModal("situation", "RECHERCHE D'UN POINT DE SITUATION", "situation-search", '/php/formulaires/recherche_point_de_situation.php', "get", 'bg-info', "");
					//$('#' + idModal).modal("show");
				});
			
			});

			
			function recupData(){
			
				var tabNatures = $('textarea[name="nature[]"]').each(function(){ $(this).val()});
				var natures = null;
				var i;
				if(tabNatures.length > 1){
					for( i = 0; i < tabNatures.length; i++ ){
						natures[i] = $(tabNatures[i]).val();
					}
				}

				var tabDegats = $('textarea[name="degat[]"]').each(function(){ $(this).val()});
				var degats = null;
				var i;
				if(tabDegats.length > 1){
					for( i = 0; i < tabDegats.length; i++ ){
						degats[i] = $(tabDegats[i]).val();
					}
				}

				var tabChomages = $('textarea[name="chomage[]"]').each(function(){ $(this).val()});
				var chomages = null;
				var i;
				if(tabChomages.length > 1){
					for( i = 0; i < tabChomages.length; i++ ){
						chomages[i] = $(tabChomages[i]).val();
					}
				}

				var data = {
					id : ($('#id').text() == 0) ? -1 : $('#id').text(),
					date : $('#date').text(),
					centre : $('select[name=centre]').val(),
					iCour : $('.inter-actu').text(),
					iDiff : $('.inter-diff').text(),
					iAnnul : $('.inter-annul').text(),
					iTerm : $('.inter-term').text(),
					off : $('.off').text(),
					soff : $('.soff').text(),
					hdr : $('.hdr').text(),
					sssm : ($('input[name=sssm]').val() != "") ? $('input[name=sssm]').val() : null,
					vehicule : ($('textarea[name=vehicules]').val() != "") ? $('textarea[name=vehicules]').val() : null,
					pSauv : ($('input[name=personne-sauvetage]').val() != "") ? $('input[name=personne-sauvetage]').val() : null,
					pSecu : ($('input[name=personne-securite]').val() != "") ? $('input[name=personne-securite]').val() : null,
					pInfo : ($('textarea[name=personne-precision]').val() != "") ? $('textarea[name=personne-precision]').val() : null,
					aSauv : ($('input[name=animaux-sauvetage]').val() != "") ? $('input[name=animaux-sauvetage]').val() : null,
					aSecu : ($('input[name=animaux-securite]').val() != "") ? $('input[name=animaux-securite]').val() : null,
					aInfo : ($('textarea[name=animaux-precision]').val() != "") ? $('textarea[name=animaux-precision]').val() : null,
					nature : natures,
					degat : degats,
					chomage : chomages
				};
				
				var objet = new Object();
				objet.id = data.id;
				objet.date = data.date;
				objet.centre = data.centre;
				objet.tabInters = [data.iCour,data.iDiff,data.iAnnul,data.iTerm];
				objet.tabMoyens = [data.off,data.soff,data.hdr,data.sssm,data.vehicule];
				objet.tabPersonnes = [data.pSauv,data.pSecu,data.pInfo];
				objet.tabAnnimaux = [data.aSauv,data.aSecu,data.aInfo];
				objet.tabSites = [data.nature,data.degat,data.chomage];
				
				msg(objet);
				return objet;
			
			}
		
		</script>
	</body>

</html>

<?php
}
?>