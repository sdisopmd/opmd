
<?php
require_once('../../init.php');
$in = "";
$ico = "down";
if( isset($_GET['ouvert']) ){
	$in = ($_GET['ouvert'] == "oui") ? "in" : "";
	$ico = ($_GET['ouvert'] == "oui") ? "up" : "down";
}
	$materielL = Materiel::getListMateriels(false);
	echo "<div class='panel panel-danger'>";
      echo "<div class='panel-heading'>";
        echo "<h3 class='panel-title'><span class='badge-trans pull-left'><span class='glyphicon glyphicon-chevron-".$ico."'></span></span>MATERIELS<span class=\"badge-trans pull-right\"><a href=\"#\" onclick='addMateriels();'><span class=\"glyphicon glyphicon-plus\"></span></a></span></h3>";
      echo "</div>";
      echo "<div class='panel-body collapse ".$in."'>";
			if($materielL != null){
				foreach( $materielL as $m){
					if($m->getVisible() == 1) {
						$etat_m = $m->getEtat();
						$style = "background-color:{$etat_m->getCouleurFond()}; color:{$etat_m->getCouleurTexte()}";
						echo "<button type='button' data='mat-{$m->getId()}' class='btn special' style='{$style};height:34px;' onmouseup='document.oncontextmenu = rightClick;' onclick='modMateriels({$m->getId()});'> {$m->getLibelle()} {$m->getCentre()->getAbreviation()} <b class='badge-white'>{$m->getDispo()}</b></button>";
					}
				}
			}
      echo "</div>";
    echo "</div>";
    
?>


    