
<?php
require_once('../../init.php');
$in = "";
$ico = "down";
if( isset($_GET['ouvert']) ){
	$in = ($_GET['ouvert'] == "oui") ? "in" : "";
	$ico = ($_GET['ouvert'] == "oui") ? "up" : "down";
}
	$vehiculeL = Vehicule::getAllVehicules();
	echo "<div class='panel panel-danger'>";
      echo "<div class='panel-heading'>";
        echo "<h3 class='panel-title'><span class='badge-trans pull-left'><span class='glyphicon glyphicon-chevron-".$ico."'></span></span>AUTRE VEHICULES<span class=\"badge-trans pull-right\"><a href=\"#\" onclick='addVehicules();'><span class=\"glyphicon glyphicon-plus\"></span></a></span></h3>";
      echo "</div>";
      echo "<div class='panel-body collapse ".$in."'>";
			if($vehiculeL != null){
				foreach( $vehiculeL as $v){
					if($v->getVisible() == 1 && $v->getCentre(true) > 1) {
						$etat_v = $v->getEtat();
						$style = "background-color:{$etat_v->getCouleurFond()}; color:{$etat_v->getCouleurTexte()}";
						echo "<button type='button' class='btn special' style='{$style};width:122px;height:34px;' onmouseup='document.oncontextmenu=rightClick'; onclick='modVehicules({$v->getId()})'; data='veh-{$v->getId()}'> {$v->getLibelle()} {$v->getCentre()->getAbreviation()} </button>";
					}
				}
			}
      echo "</div>";
    echo "</div>";
	
?>


    