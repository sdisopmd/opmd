


<?php
// Info l'Etat correspond 0:Aucun, 1: En cour, 2: Reconnaissance, 3: Différée et 4 : Terminer, 5 : Annuler
// Le Status correspond à l'état du véhicule soit :
// 1:En inter (orange), 3: VL reco (cyan), 5: Indisponible (noi) et 10 : Cloture inter (vert) 


// pour une inter en cour etat à 1 et status à 1
// pour une inter en reco etat à 2 et status à 3
// pour une inter en diff etat à 3 et status à 1
// pour une inter en term etat à 4 et status à 10
// pour une inter en annul etat à 5 et status à 10
require_once('../../init.php');

$inter = new Intervention(@$_GET['change']);
$etat = $inter->getEtat();
$date =  dateD(time());
$heure = dateH(time());
$adresse = "";
$commune = 0;
$cp = "";

if($inter->exists()){
	$date = dateD($inter->getDateHeure());
	$heure = dateH($inter->getDateHeure());
	$adresse = $inter->getAdresse();
	$commune = $inter->getCommune()->getLibelle();
	$cp = $inter->getCommune()->getCodePostal();
}
$heure = strval($heure);


include_once('../interventions/fonction.php');

if ( $etat == 0 ) {
	include_once('../interventions/vue_depart.php');
}

if ( $etat == 1 ) {
	include_once('../interventions/vue_depart.php');
}

if ( $etat == 2 ) {
	include_once('../interventions/vue_vlreco.php');
	afficheMenu($inter, $etat);
}

if ( $etat == 3 ) {
	include_once('../interventions/vue_diff.php');
	afficheMenu($inter, $etat);
}
	
?>


<div class="row">
	<div class="col-sm-12"><hr></div>
</div>


 <div class="form-group">
 	<div class="row">
	
		<!-- Intervention en création -->
		<?php if($etat == 0) : ?>
		<div class="row">
			<div class="col-sm-2 col-sm-offset-3"><button type="button" id="inter-dep" class="btn btn-success">DEPART</button></div>
			<div class="col-sm-2"><button type="button" id="vlreco" class="btn btn-info">VL RECO</button></div>
			<div class="col-sm-2"><button type="button" id="inter-diff" class="btn btn-warning">DIFFERER</button></div>
		</div>
		<?php endif; ?>
	
		<!-- Intervention en cours -->
		<?php if($etat == 1) : ?>
		<div class="row">
			<div class="col-sm-2 col-sm-offset-3"><button type="button" id="inter-dep" class="btn btn-success">MODIFIER</button></div>
			<div class="col-sm-2"><button type="button" id="inter-diff" class="btn btn-warning">DIFFERER</button></div>
			<div class="col-sm-2"><button type="button" id="inter-term" class="btn btn-danger">CLOTURE</button></div>
		</div>
		<?php endif; ?>
		
		<!-- Intervention différée -->
		<?php if($etat == 3) : ?>
		<div class="row">
			<div class="col-sm-2 col-sm-offset-2"><button type="button" id="inter-diff" class="btn btn-warning">MODIFIER</button></div>
			<div class="col-sm-2"><button type="button" id="inter-dep" class="btn btn-success">DEPART</button></div>
			<div class="col-sm-2"><button type="button" id="vlreco" class="btn btn-info">VL RECO</button></div>
			<div class="col-sm-2"><button type="button" id="inter-annuler" class="btn btn-danger">ANNULER</button></div>
		</div>
		<?php endif; ?>
		
		<!-- Intervention avec VL reconnaissance -->
		<?php if($etat == 2) : ?>
		<div class="row">
			<div class="col-sm-2 col-sm-offset-3"><button type="button" id="vlreco" class="btn btn-info">MODIFIER</button></div>
			<div class="col-sm-2"><button type="button" id="inter-diff" class="btn btn-warning">DIFFERER</button></div>
			<div class="col-sm-2"><button type="button" id="inter-annuler" class="btn btn-danger">ANNULER</button></div>
		</div>
		<?php endif; ?>
	
		</div>
	</div>
</form>
		
<!-- Unixvers Menu style -->
<script type="text/javascript" src="/assets/js/intervention.js"></script>
<script type="text/javascript" src="/libs/menu/menu.js"></script>