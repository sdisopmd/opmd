<?php
require_once('../../init.php');

$date_actu = date("d") . "/" . date("m") . "/" . date("Y");
$heure_actu = date("H") . "H" . date("i");

$compteur = Intervention::compteurInter();
$nb_it_actu = $compteur['enCour'];
$nb_inter_en_reco = $compteur['vlreco'];
$nb_it_diff = $compteur['diff'];
$nb_inter_liee = $compteur['liee'];
$it_ann = $compteur['annul'];
$it_ter = $compteur['term'];
$total = $compteur['total'];
$total_inter =  $total + $it_ann + $it_ter;
$nb_pe = (Permanence::getCurrentPermanence()->getId() == -1) ? 0 : 1;

$interList = Intervention::listInterventions();
$tabInter = array();
foreach($interList as $intervention){
	$it = new Intervention($intervention->id);
	array_push($tabInter,$it);
}

$centreList = Centre::listCentres();
$tabCentre = array();
foreach($centreList as $centres){
	$center = new Centre($centres->id);
	array_push($tabCentre,$center);
}

$permList = Permanence::getAllPermanences();
$tabPerm = array();
if ( $permList != null ) {
	foreach($permList as $p){
		$perm = new Permanence($p->getId());
		array_push($tabPerm,$perm);
	}
}

$perm_en_cour = (Permanence::getCurrentPermanence()->getId() == -1 ) ? new Permanence(1) : Permanence::getCurrentPermanence();
$id_perm = 1;
$deb_perm = "";
$fin_perm = "";
$duree = "";
if ( $perm_en_cour != null ) {
	$id_perm = ($perm_en_cour->getId() != -1) ? $perm_en_cour->getId() : 0;
	$deb_perm = $perm_en_cour->getDebut();
	$fin_perm = $perm_en_cour->getFin();
	
	if ( $fin_perm != null ) {
		$datetime1 = new DateTime(dateDH3($perm_en_cour->getDebut()));
		$datetime2 = new DateTime(dateDH3($perm_en_cour->getFin()));
		$duree = $datetime1->diff($datetime2);
		$duree = $duree->format('%hh-%imin');
	}
	
}


?>


<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Feuille récapitulatif</title>

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" type="text/css" href="/libs/bootstrap/css/bootstrap.css" />
		<link rel="stylesheet" type="text/css" href="/libs/bootstrap/css/bootstrap-unixvers.css" />

		<!-- Unixvers style -->
		<link rel="stylesheet" href="/<?= CSS; ?>/unixvers.css">
		
		<link rel="shortcut icon" href="/favicon.ico">
		

	</head>

	<body id="fondSite">

		<div class="container-fluid">

			<!-- titre-->

			<div class="row">
				<div class="col-md-1">
					<img class="decale" src="/<?= IMG; ?>/pucelle.png" alt="logo" />
				</div>
				<div class="col-md-1">

				</div>
				<div class="col-md-8">

					<div class="panel panel-default decale">
						<div class="panel-body">
							<form class="form-horizontal" role="form">
								<div class="form-group">
									<label for="centre" class="col-sm-4 control-label" style="font-size:36px">Centre de : </label>
									<div class="col-sm-6" style="margin-top:18px">
										<select class="form-control" name="centre">
											<?php foreach($tabCentre as $centre) : ?>
										    <option><?= $centre->getLibelle() ; ?></option>
										    <?php endforeach; ?>
										</select>
									</div>
								</div>
							</form>
						</div>
					</div>

				</div>
				<div class="col-md-2"></div>
				
			</div>

			<!-- /titre-->

			<!-- visa cdc-->

			<div class="row">
				<div class="col-md-2">

				</div>
				<div class="col-md-2">

					<div class="panel panel-info visa">
						<div class="panel-heading">
							<h3 class="panel-title text-center"><b>Visa Chef de Centre</b></h3>
						</div>
						<div class="panel-body" style="height:105px">

						</div>
					</div>

				</div>
				<div class="col-md-2">

				</div>
				<div class="col-md-6">
					<table class="table table-bordered" style="width:600px">

						<tr class="bg-info">
							<th class="text-center" rowspan="2" style="vertical-align:middle;">Permanence ci N° 
		
								<select style="display:inline" id="choix-perm">
									<?php if ( $id_perm != 0 ) : ?>
										<option><?= $id_perm ; ?></option>
									<?php endif; ?>
									<?php foreach($tabPerm as $perm) : ?>
										<?php if ( $perm->getId() != $id_perm ) : ?>
										<option value="<?= $perm->getId() ; ?>"><?= $perm->getId() ; ?></option>
										<?php endif; ?>
								    <?php endforeach; ?>
								</select>
	
							</th>
							<th class="text-center">DEBUT</th>
							<th class="text-center">FIN</th>
							<th class="text-center">DUREE</th>
						</tr>

						<tr>
						
							<td class="text-center" id="p-deb">
							<?php 
							if ( $id_perm != 0) {
								echo "<b>". dateD($deb_perm) ." - ". dateH2($deb_perm)."</b>";
							} 
							?>
							</td>
							
							<td class="text-center" id="p-fin">
							<?php 
							if ( $id_perm != 0 && $perm_en_cour->getFin() != null) {
								echo "<b>". dateD($fin_perm) ." - ". dateH2($fin_perm)."</b>";
							} 
							?>
							</td>
							
							<td class="text-center" id="p-time">
							<?php 
							if ( $duree != "") {
								echo "<b>". $duree ."</b>";
							} 
							?>
							</td>
							
						</tr>

					</table>
				</div>
			</div>

			<!-- /visa cdc-->

			<!-- tableau-->

			<div class="row">
				<div class="col-md-12">
					<div class="table-responsive">
					<table class="table table-bordered table-hover table-striped">
						<thead>
							<tr class="bg-info">
								<th class="text-center">N° INTER</th>
								<th class="text-center" style="width:70px">ETAT</th>
								<th class="text-center">DATE</th>
								<th class="text-center">DEPART</th>
								<th class="text-center">RETOUR</th>
								<th class="text-center">DUREE</th>
								<th class="text-center">NATURE</th>
								<th class="text-center">PRECISION</th>
								<th class="text-center">COMMUNE</th>
								<th class="text-center">ADRESSE</th>
								<!-- <th class="text-center">VEHICULE(s)</th>
								<th class="text-center">EFFECTIF(s)</th> -->
							</tr>
						</thead>

						<tbody>
							
							<?php foreach( $tabInter as $inter) : ?>

							<tr id="inter-<?= $inter->getId(); ?>" onclick="alert(this.id.substr(6));">
								<td class="text-center" style="vertical-align:middle;"> <?= $inter->getId(); ?> </td>
								<td class="text-center" style="vertical-align:middle;"> 
									<?php 
										$nbEtat = $inter->getEtat();
										$status = $inter->recupEtat($nbEtat);
										$class = array("default","success","info","warning","danger","purple","violet");
										echo "<span class='label label-{$class[$nbEtat]}'>{$status}</span>";
									?> 
								</td>
								<td class="text-center" style="vertical-align:middle;"> <?= ($inter->getDateHeure()) ? dateD($inter->getDateHeure()) : "Aucune"; ?> </td>
								<td class="text-center" style="vertical-align:middle;"> <?= ($inter->getDateHeure()) ? dateH2($inter->getDateHeure()) : "Aucun départ"; ?> </td>
								<td class="text-center" style="vertical-align:middle;"> <?= ($inter->getFin()) ? dateH2($inter->getFin()) : "Aucun retour"; ?> </td>
								<td class="text-center" style="vertical-align:middle;"> 
									<?php
										if($inter->getFin()) {
											$datetime1 = new DateTime(dateDH3($inter->getDateHeure()));
											$datetime2 = new DateTime(dateDH3($inter->getFin()));
											$interval = $datetime1->diff($datetime2);
											echo $interval->format('%hh-%imin');
										} else {
											echo "En cour"; 
										} 
									
									?> 
								</td>
								<td class="text-center" style="vertical-align:middle;"> <?= ($inter->getPrecision()) ? $inter->getPrecision()->getNature()->getLibelle() : "Aucune"; ?> </td>
								<td class="text-center" style="vertical-align:middle;"> <?= ($inter->getPrecision()) ? $inter->getPrecision()->getLibelle() : "Aucune"; ?> </td>
								<td class="text-center" style="vertical-align:middle;"> 
								<?php
									if( $inter->getAdresse()->getCommune()->getId() != -1) {
										$commune = $inter->getAdresse()->getCommune();
										$code = $commune->getCodePostal();
										$nom = $commune->getLibelle();
										echo $code. " ". $nom;
									} else {
										 echo "Aucune"; 
									}
								?> 
								</td>
								<td class="text-center" style="vertical-align:middle;"> 
								<?php 
									if($inter->getAdresse()) {
										$adresse = new Adresse($inter->getAdresse(true));
										$num = ($inter->getNumeroRue()) ? "N° ".$inter->getNumeroRue() : "";
										$rue = ($inter->getAdresse()->getRue()->getLibelle()) ? " ".$inter->getAdresse()->getRue()->getLibelle() : "";
										$appart = ($inter->getAppartement()) ? "<br> Appartement ". $inter->getAppartement() : "";
										$etage = ($inter->getEtage()) ? " - étage : ". $inter->getEtage() : "";
										$ensemble = $num . $rue . $appart . $etage;
										if( $ensemble != "" ) {
											echo $ensemble;
										} else {
											echo "Aucune";
										}
										
									}  else {
										echo "Aucune"; 
									}
								?> 
								</td>
								<!-- <td class="text-center" style="vertical-align:middle;"> 
									<?php 
										if($inter->getVehicules(true)){
											foreach( $inter->getVehicules() as  $v){
												echo $v->getLibelle()." ";
											}
											//debug($inter->getVehicules());
										} else {
											echo "Aucun"; 
										}
									
									?> 
								</td>
								<td class="text-center" style="vertical-align:middle;"> 
									<?php 
								
										if($inter->getPersonnel(true)) {
											foreach($inter->getPersonnel() as $personne){
												//debug($personne);
												$nom = $personne->getNom();
												$grade = $personne->getGrade()->getAbreviation();
												echo "<b>".$grade."</b>"." ". $nom." ";
											}
											
											
										} else {
											echo "Personne"; 
										}
											
								
								?> </td> -->
							</tr>
							
							<?php endforeach; ?>
								
							

						</tbody>

					</table>
					</div>
				</div>
			</div>

			<!-- /tableau-->

		</div>
		
		<!-- Modal -->
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		  <div class="modal-dialog modal-lg">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		        <h4 class="modal-title" id="myModalLabel">FEUILLE DE DEPART</h4>
		      </div>
		      <div class="modal-body">
		      	 
		      </div>
		    </div>
		  </div>
		  
		</div>


		<!-- Bootstrap JavaScript -->
		<script type="text/javascript" src="/<?= JQUERY; ?>/jquery-2.1.0.js"></script>
		<script type="text/javascript" src="/<?= BOOTSTRAP_JS; ?>/bootstrap.js"></script>
		
		<script type="text/javascript" charset="utf-8">
			$(document).ready(function(){
				
				$(".visa").hide();
				
				$("#choix-perm").on('change',function(){
					var idp = $(this).val();
					$.post('/php/requetes/recup_perm.php', { id : idp }).always( function(arg){
						$("#p-deb").empty().html(arg.deb);
						$("#p-fin").empty().html(arg.fin);
						$("#p-time").empty().html(arg.duree);
					});
				});
				
				
				function affInter(id){
					
				}
				
			});
		</script>

	</body>

</html>

