<?php
require_once('../../init.php');

if( isset($_POST['type']) && !empty($_POST['type']) ) {
	$type = $_POST['type'];
	
	switch($type) {
	
		case "vehicule" : {
			$liste = Vehicule::getAllVehicules();
			$titre = "UN VEHICULE";
		}
		break;
		
		case "materiel" : {
			$liste = Materiel::getAllMateriels();
			$titre = "UN MATERIEL";
		}
		break;
		
		case "groupe" : {
			$liste = Groupes::getAllGroupes();
			$titre = "UN GROUPE";
		}
		break;
		
		case "centre" : {
			$liste = Centre::getAllCentres();
			$titre = "UN CENTRE";
		}
		break;
		
		case "commune" : {
			$liste = Commune::getAllCommunes(true);
			$titre = "UNE COMMUNE";
		}
		break;

		case "nature" : {
			$liste = NatureIntervention::getAllNatures();
			$titre = "UNE NATURE";
		}
		break;

		case "precision" : {
			$liste = PrecisionIntervention::getAllPrecisions();
			$titre = "UNE PRECISION";
		}
		break;
		
		case "intervention" : {
			$liste = Intervention::getAllInterventions();
			$titre = "UNE INTERVENTION";
		}
		break;
		
	}
	
	creerModalChoix($type, $titre, $liste, $type);

}


 //modal pour le choix 
function creerModalChoix($idModal = "modal", $titre = "", $liste = null, $type = null){

	echo "<div class='modal fade' id='{$idModal}' tabindex='-1' role='dialog' aria-labelledby='dial-{$idModal}' aria-hidden='true'>";
	  echo "<div class='modal-dialog modal-lg'>";
	    echo "<div class='modal-content' style='border-radius:20px;'>";
	      echo "<div class='modal-header bg-warning' style='border-radius:20px;'>";
	        echo "<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>";
	        echo "<h1 class='modal-title text-center' id='dial-{$idModal}'>SELECTIONNER {$titre}</h1>";
	      echo "</div>";
	      echo "<div class='modal-body'>";
	      	
			echo '<form class="form-horizontal" role="form" name="selectChoix" method="POST" action="'.$_SERVER["PHP_SELF"].'">';

				echo '<div class="row">';
					echo '<div class="col-md-12">';
				
						echo '<div class="form-group">';
								echo '<div class="col-sm-2">';
										echo '<label for="id" class="col-sm-4 control-label">'.strtoupper($type).'</label>';
								echo "</div>";
								echo '<div class="col-sm-10">';
									if($type == "intervention"){
										echo '<select name="id" class="form-control input-sm" data="inter">';
									} else {
										echo '<select name="id" class="form-control input-sm">';
									}
									echo "<option value='-1'></option>\n";
									if($type == "erreur"){
										foreach( $liste as $elem ) {
											echo "<option value=\"{$elem->getId()}\">Erreur #{$elem->getId()}</option>\n";
										}
									} else {
										foreach( $liste as $elem ) {
											$centre = ($type == "vehicule" || $type == "materiel") ? " - ".$elem->getCentre()->getAbreviation() : "";
											echo "<option value=\"{$elem->getId()}\">{$elem->getLibelle()} {$centre}</option>\n";
										}
									}
									
									
									echo '</select>';
			  
								echo "</div>";
							echo "</div>";
				
					echo "</div>";
				echo "</div>";
				
				echo "<input type='text' hidden name='element' value='".$type."'>";
				
				if( $type != "intervention" ) {
				
				echo '<div class="row">';
					echo '<div class="col-md-12">';
				
						echo '<div class="form-group">';
							echo '<div class="col-sm-offset-2 col-sm-3">';
							  echo '<button type="submit" name="add" class="btn btn-success">Ajouter</button>';
							echo '</div>';
							
							echo '<div class="col-sm-3">';
							  echo '<button type="submit" name="mod" class="btn btn-warning">Modifier</button>';
							echo '</div>';
							
							echo '<div class="col-sm-3">';
							  echo '<button type="submit" name="del" class="btn btn-danger">Supprimer</button>';
							echo '</div>';
						
						echo "</div>";
				
					echo "</div>";
				echo "</div>";
				
				}
						
			echo "</form>";
	
	      echo "</div>";
	    echo "</div>";
	  echo "</div>";
	echo "</div>";
	
	?>
	
	<script>
	
		$("#<?= $idModal; ?>").modal("show");
		
		$("button[name=add]").on("click",function(e){
			e.preventDefault();
			var form = new Object();
			form.id = $('select[name=id]').val();
			form.action = "add";
			form.element = $('input[name=element]').val();
			if(fullDebug) { msg(form) };
			
			
			$("#<?= $idModal; ?>").modal("hide");
			$("#<?= $idModal; ?>").empty();
			$("#<?= $idModal; ?>").remove();
			
			ajout(form.element);
		});
		
		$("button[name=mod]").on("click",function(e){
			e.preventDefault();
			var form = new Object();
			form.id = $('select[name=id]').val();
			form.action = "mod";
			form.element = $('input[name=element]').val();
			if(fullDebug) { msg(form) };
			
			if( form.id == -1){
				notif("Vous devez sélectionner un élément pour pouvoir le modifier","warning","","");
			} else {
				$("#<?= $idModal; ?>").modal("hide");
				$("#<?= $idModal; ?>").empty();
				$("#<?= $idModal; ?>").remove();
				
				modif(form.id,form.element);
			}
			
			
		});
		
		$("button[name=del]").on("click",function(e){
			e.preventDefault();
			var form = new Object();
			form.id = $('select[name=id]').val();
			form.action = "del";
			form.element = $('input[name=element]').val();
			if(fullDebug) { msg(form) };
			
			if( form.id == -1){
				notif("Vous devez sélectionner un élément pour pouvoir le supprimer","warning","","");
			} else {
				$("#<?= $idModal; ?>").modal("hide");
				$("#<?= $idModal; ?>").empty();
				$("#<?= $idModal; ?>").remove();
				
				supprim(form.id,form.element);
			}
			
		});
		
		
		$('select[data=inter]').on('change',function(e){ 
			var idInter = $(this).val(); 
			$("#<?= $idModal; ?>").modal("hide");
			var titre = "RECAPITULATIF DE L'INTERVENTION N° "+idInter;
			 creerModal("recap", titre, "recap-inter", "/php/pages/recap_intervention.php?id="+idInter, "",'bg-info','');
		});
		
		function ajout(element){
			
			if(element == "vehicule"){
				addVehicules();
			}
			
			if(element == "materiel"){
				addMateriels();
			}
			
			if(element == "groupe"){
				addGroupes();
			}
			
			if(element == "centre"){
				addCentres();
			}
			
			if(element == "commune"){
				addCommunes();
			}

			if(element == "nature"){
				addNatures();
			}

			if(element == "precision"){
				addPrecisions();
			}
			
			if(fullDebug) { msg("enregistrement : " + element) };
			
		}
		
		function modif(id,element){
			
			if(element == "vehicule"){
				modVehicules(id);
			}
			
			if(element == "materiel"){
				modMateriels(id);
			}
			
			if(element == "groupe"){
				modGroupes(id);
			}
			
			if(element == "centre"){
				modCentres(id);
			}
			
			if(element == "commune"){
				modCommunes(id);
			}

			if(element == "nature"){
				modNatures(id);
			}

			if(element == "precision"){
				modPrecisions(id);
			}
			
			if(fullDebug) { msg("modification : " + element) };
			
		}
		
		function supprim(id,element){
		
			if(element == "vehicule"){
				delVehicules(id,element);
			}
			
			if(element == "materiel"){
				delMateriels(id,element);
			}
			
			if(element == "groupe"){
				delGroupes(id,element);
			}
			
			if(element == "centre"){
				delCentres(id,element);
			}
			
			if(element == "commune"){
				delCommunes(id,element);
			}
			
			if(element == "nature"){
				delNatures(id,element);
			}

			if(element == "precision"){
				delPrecisions(id,element);
			}

			if(fullDebug) { msg("suppression : " + element) };
			
		}
		
		
		
	</script>
	
	<?php
	
}

?>