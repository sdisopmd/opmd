<?php
require_once('../../init.php');
	$inter = new Intervention(@$_GET['id']);
	if($inter->getId() == -1) {
		echo '<script>notif("Paramètre incorrect détecté","error","",""); viderModal();</script>';
	} else {
		$id = $inter->getId();
		$etat = $inter->getEtat();
	}
?>

	<!--<body style="background:white;" <?php //if(isset($_GET['print'])) echo 'onload="window.print(); window.close();"'; ?>>-->
	
		<div class="row">
			<div class="col-sm-12">
			
			
	
		<div class="panel panel-danger">
		  <!-- Default panel contents -->
		  <div class="panel-heading text-center"><b style="font-size:18px;color:black">Fiche Intervention N° <span class="badge-danger" style="font-size:22px"><?= $inter->getId(); ?><span></b></div>

		  <!-- Table -->
		  <table class="table table-striped">

			<?php
				$i = 0;
				
				$infos[$i][0] = "<b>DEPART</b>";
				$infos[$i++][1] = date('\L\e d/m/y à Hi', $inter->getDateHeure());
				
				$infos[$i][0] = '<b>COMMUNE</b>';
				$commune = $inter->getCommune();
				$infos[$i++][1] = "{$commune->getLibelle()} ({$commune->getCodePostal()})";
				
				$infos[$i][0] = '<b>ADRESSE</b>';
				$infos[$i++][1] = trim("{$inter->getNumeroRue()} {$inter->getAdresse()} ".
										(strlen($inter->getEtage()) ? "Etage {$inter->getEtage()}" : '').' '.
										(strlen($inter->getAppartement()) ? "Appt {$inter->getAppartement()}" : ''));
				
				$infos[$i][0] = '<b>NATURE</b>';
				$infos[$i++][1] = $inter->getNature()->getLibelle();
				
				$infos[$i][0] = '<b>PRECISION</b>';
				$infos[$i++][1] = $inter->getPrecision()->getLibelle();
				
				$infos[$i][0] = '<b>APPELANT</b>';
				$infos[$i++][1] = trim("{$inter->getPrenomAppelant()} {$inter->getNomAppelant()}");
				
				$infos[$i][0] = '<b>TELEPHONE</b>';
				$infos[$i++][1] = $inter->getTelephoneAppelant();
				
				$infos[$i][0] = "<b>COMPL. D'INFO</b>";
				$infos[$i++][1] = $inter->getComplement();
				
				$vs = $inter->getVehicules();
				$ms = $inter->getAllMateriels();
				$pe = $inter->getPresencesExternes();
				$pers = $inter->getPersonnel();
				
				$infos[$i][0] = "<b>DISPOSITIF AU DEPART</b>" ;
				$elem = "";
				if($vs != null) for($j=0 ; $j<count($vs) ; $j++) {
					$v = $vs[$j];
					$elem .= "<span class='label label-danger space'>{$v->getLibelle()} {$v->getCentre()->getAbreviation()}</span>";
				}
				$infos[$i++][1] = $elem;
				
				foreach($infos as $info) {
					$sep = ($info[0] == "&nbsp;") ? '' : ':';
					echo "<tr>
							<td style='vertical-align:top;'>{$info[0]}</td>
							<td style='vertical-align:top;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{$sep}&nbsp;&nbsp;</td>
							<td style='vertical-align:top;'>{$info[1]}</td>
						</tr>";
				}
			?>
					</table>
				</div>
		
			</div>
		</div> <!-- /panel -->
		
		<div class="row">
		
			<div class="col-sm-4">
			
				<div class="panel panel-danger">
				  <div class="panel-heading text-center"><b style="font-size:18px;color:black">VEHICULES</b><span class="badge badge-info pull-right" style="font-size:22px"><?= count($vs); ?></span></div>
				  <div class="panel-body clear-both">
					<ul class="list-group  clear-both">
					<?php
						if($vs != null) foreach($vs as $v) {
							echo "<li class='label label-danger space size'>{$v->getLibelle()} {$v->getCentre()->getAbreviation()}<br />";
							$ps = $v->getPersonnel();
							$tab = null;
							if($ps != null) foreach($ps as $p) {
								$tab .= "<b class='space'>{$p->getNomFormate()}</b>";
							}
							echo $tab."</li>";
						}
					?>
					</ul>
				  </div>
				</div>
				
			</div>
			
			
			<div class="col-sm-4">
			
				<div class="panel panel-warning">
				  <div class="panel-heading text-center"><b style="font-size:18px;color:black">MATERIELS</b><span class="badge badge-info pull-right" style="font-size:22px"><?= count($ms); ?></span></div>
				  <div class="panel-body clear-both">
					<ul class="list-group  clear-both">
					<?php
						if($ms != null) foreach($ms as $m) {
							$nombre = ($m->getNbByInter($id) != 0 ) ? $m->getNbByInter($id) : "";
							echo "<li class='label label-warning space size'>{$m->getLibelle()} {$m->getCentre()->getAbreviation()} {$nombre}<br>";
							echo "</li>";
						}
					?>
					</ul>
				  </div>
				</div>
				
			</div>
			
			
			<div class="col-sm-4">
			
				<div class="panel panel-success">
				  <div class="panel-heading text-center"><b style="font-size:18px;color:black">PERSONNEL</b><span class="badge badge-info pull-right" style="font-size:22px"><?= count($pers); ?></span></div>
				  <div class="panel-body">
					<ul class="list-group">
					<?php
						if($pers != null) foreach($pers as $p) {
							echo "<li class='label label-success space size'>{$p->getNomFormate()}</li>";
						}
					?>
					</ul>
				  </div>
				</div>
				
			</div>
			
		</div>
		
		<div class="row">
			<div class="col-sm-12">
			
				<div class="panel panel-info">
				  <div class="panel-heading text-center"><b style="font-size:18px;color:black">PRESENCE EXTERNE</b><span class="badge badge-info pull-right" style="font-size:22px"><?= count($pe); ?></span></div>
				  <div class="panel-body">
					<ul class="list-group">
					<?php
						if($pe != null) foreach($pe as $p) {
							$nb = ( PresenceExterne::getQuantite($inter->getId(),$p->getId()) != 0 ) ? PresenceExterne::getQuantite($inter->getId(),$p->getId()) : "";
							echo "<li class='label label-info space size'>{$p->getLibelle()} {$nb}</li>";
						}
					?>
					</ul>
				  </div>
				</div>
		
			</div>
		</div>
		

		<?php include_once('boutons.php'); ?>

	

