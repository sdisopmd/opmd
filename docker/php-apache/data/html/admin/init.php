<?php
session_start();

/*
* Fichier de chargement de tous les fichiers du projet  
* Fichier crée le 06/04/2014
* Auteur : Denis Sanson alias Multixvers
*/
 
$root = "/";
define("DS",$root);
$root = substr($_SERVER['DOCUMENT_ROOT'],0,-4)."src/";
set_include_path(get_include_path() . PATH_SEPARATOR . $root);
$local = true;


// Sert pour se connecter à la BDD
define('DB_LOCAL',$local);

// sert au débug
define("DEBUG", $local);

// niveau source src
define("SRC",$root);

// niveau web
define('WEB',$_SERVER['DOCUMENT_ROOT']);

// niveau config
define("CONFIG",$root.DS."config".DS);

// nom du serveur
define('SERVER_NAME',$_SERVER['SERVER_NAME']);

// chargement des chemins
include_once(CONFIG.'path_admin.php');

// chargement des paramètres
include_once(CONFIG.'params.php');

// chargement configuration pour la base de donnée
include_once(CONFIG.'db_config.php');

// chargement des fonctionnalité des dates
include_once(CONFIG.'dates.php');

// chargement des classes
include_once(CONFIG.'loader.php');

?>

