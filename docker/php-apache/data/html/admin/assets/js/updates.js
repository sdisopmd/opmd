/*
 * Fonction des mises à jours partie administration
 * Fichier crée le 01/05/2014
 * Auteur : Denis Sanson alias Multixvers
 */

/*-------------------- FONCTIONS DE SURVEILLANCE INTELLIGENTE --------------------*/

function updateIntelligent() {
    setTimeout(function() { update() }, 1500);
}

function update() {
    update_users();
    update_dashboard();
}

/*-------------------- FONCTIONS DE MISE À JOUR --------------------*/
function update_users() {
    $('#users').load('/php/admin/pages/user.php');
}

function update_dashboard() {
    $('#dashboard').load('/php/admin/pages/dashboard.php');
}