/*
 * Fonction de validation des formulaires d'administration
 * Fichier crée le 01/05/2014
 * Auteur : Denis Sanson alias Multixvers
 */


/*-------------------- VALIDATION DES FORM D'ADMINISTRATION--------------------*/


function valid_user(form, data) {

    if (!$.trim(data.nom).length) {
        error(form, "Nom vide");
        return false;
    }

    if (!$.trim(data.prenom).length) {
        error(form, "Prénom vide");
        return false;
    }

    updateIntelligent();
    if (fullDebug) { msg(data) };
    return true;
}