/*
 * Fonction des boîtes modal du menu d'administration via Bootstrap
 * Fichier crée le 01/05/2014
 * Auteur : Denis Sanson alias Multixvers
 */


/*-------------------- FONCTIONS DES BOITES MODAL BOOTSTRAP DU MENU D'ADMINISTRATION --------------------*/

// UTILISATEUR

function users() {
    $.post('../admin/pages/modal_choix.php', { type: "user" }, function(arg) { $("body").append(arg) });
}

function viewUsers() {
    $("#content").load('../admin/membres/users/gestion.php');
}

function addUsers() {
    creerModal("add-user", "AJOUTER UN UTILISATEUR", "add-u", '../admin/membres/users/register.php?id=-1&mode=add', "get", 'bg-success', "");
}

function modUsers(u_id) {
    creerModal("mod-user", "MODIFIER UN UTILISATEUR", "mod-u", '../admin/membres/users/register.php?id=' + u_id + '&mode=mod', "get", 'bg-warning', "");
}

function delUsers(u_id, type) {
    modalSuppr("del-user", "SUPPRIMER UN UTILISATEUR", "del-u", 'bg-danger', "de l'utilisateur", type, u_id);
}

function upinfo() {
    $.post('../admin/requetes/infodev.php', { info: true }).always(function(arg) {
        if (arg.retour == "ok") {
            notif("Information pour le développement est mit à jours", "info", "", "");
        }
    });
}


// Recherche Codis
function modalCodis() {
    creerModal("codis-number", "RECHERCHER UNE INTERVENTION", "search-c", '/php/formulaires/search-codis.php', "get", 'bg-success', "");
}