
<?php
	require_once('../../init.php');
	header('Content-type: application/json');
	if(isset($_POST['login']) && isset($_POST['pass'])) {
		$login = $_POST['login'];
		$pass = $_POST['pass'];
		$retour = User::login($login,$pass);
		if ( $retour == 'erreur' ) {
			$txt = "Erreur vous pouvez pas vous connecter car vos identifiants sont incorrect";
			$rep = array("retour" => $txt);
			echo json_encode($rep);
		} else {
			$u = new User($retour);
			$_SESSION['users']['id'] = $u->getId();
			$_SESSION['users']['nom'] = $u->getNom();
			$_SESSION['users']['prenom'] = $u->getPrenom();
			$_SESSION['users']['active'] = $u->getActive();
			$_SESSION['users']['groupe_id'] = $u->getGroupe();
			$_SESSION['users']['login'] = $u->getConnecter();
			$_SESSION['users']['info'] = $u->getInfo();
			$rep = array("retour" => "ok");
			echo json_encode($rep);
		}
	}
	
	

?>