<?php

	require_once('../../init.php');
	header('Content-type: application/json');
	if(isset($_POST['id'])) {
		$id = $_POST['id'];
		$u = new User($id);
		$u->logout();
		$ret = array("retour"=>"ok");
		echo json_encode($ret);
	} else {
		$ret = array("retour"=>"erreur");
		echo json_encode($ret);
	}



?>