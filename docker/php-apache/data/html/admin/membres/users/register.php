<?php

	require_once('../../init.php');

	if(isset($_POST['id']))
	{
		$id = $_POST['id'];
		$nom = ucfirst(strtolower($_POST['nom']));
		$prenom = ucfirst(strtolower($_POST['prenom']));
		$login = $_POST['login'];
		$pass = $_POST['pass'];
		$groupe = $_POST['groupe'];
		$active = $_POST['active'];
		$user = new User($id);
		$user->setNom($nom);
		$user->setPrenom($prenom);
		$user->setLogin($login);
		$user->setPass($pass);
		$user->setGroupe($groupe);
		$user->setActive($active);
		$user->commit();
		exit;
	} 
	
	$u = new User(@$_GET['id']);

?>


<form class="form-horizontal" role="form" id="form-connect">

	<div class="form-group">
		<div class="col-sm-12">
			<div id="error" />

			<input type="hidden" name="id" value="<?php echo $u->getId(); ?>" />
			<input type="hidden" name="del" />
		</div>
	</div>
  
  <div class="form-group has-feedback">
    <label for="nom" class="col-sm-2 control-label">Nom</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" name="nom" placeholder="Votre nom" value="<?= $u->getNom();?>">
      <span class="glyphicon form-control-feedback"></span>
      <span class="error"></span>
    </div>
  </div>
  
  <div class="form-group has-feedback">
    <label for="prenom" class="col-sm-2 control-label">Prénom</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" name="prenom" placeholder="Votre prénom" value="<?= $u->getPrenom();?>">
      <span class="glyphicon form-control-feedback"></span>
      <span class="error"></span>
    </div>
  </div>
  
  <div class="form-group has-feedback">
    <label for="login" class="col-sm-2 control-label">Identifiant</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" name="login" placeholder="Votre identifiant de connection" value="<?= $u->getLogin();?>">
      <span class="glyphicon form-control-feedback"></span>
      <span class="error"></span>
    </div>
  </div>
  
  <div class="form-group has-feedback">
    <label for="pass" class="col-sm-2 control-label">Mot de passe</label>
    <div class="col-sm-10">
      <input type="password" class="form-control" name="pass" placeholder="Votre mot de passe" value="<?= $u->getPass();?>">
      <span class="glyphicon form-control-feedback"></span>
      <span class="error"></span>
    </div>
  </div>
  
  <div class="form-group has-feedback">
    <label for="pass2" class="col-sm-2 control-label">Confirmer le mot de passe</label>
    <div class="col-sm-10">
      <input type="password" class="form-control" name="pass2" placeholder="Confirmer votre mot de passe" value="<?= $u->getPass();?>">
      <span class="glyphicon form-control-feedback"></span>
      <span class="error"></span>
    </div>
  </div>
  
  <div class="form-group has-feedback">
    <label for="groupe" class="col-sm-2 control-label">Groupe</label>
    <div class="col-sm-10">
      <select name="groupe" class="form-control">
			<option value="-1">Sélectionner un groupe</option>
			<?php
				$gs = Group::getAllGroups();
				if($gs != null) foreach($gs as $g) {
					if($g->getId() == $u->getGroupe()){
						echo "<option value='{$g->getId()}' selected='selected'>{$g->getNom()}</option>";
					} else {
						echo "<option value='{$g->getId()}'>{$g->getNom()}</option>";
					}
				}
			?>
		</select>
      <span class="glyphicon form-control-feedback"></span>
      <span class="error"></span>
    </div>
  </div>
  
  <div class="form-group has-feedback">
    <label for="actif" class="col-sm-2 control-label">Activer</label>
    <div class="col-sm-10">
      <label class="radio-inline">
				<input type="radio" name="actif" value="1" onclick="activate(1);" <?= ($u->getActive() == 1) ? "checked" : ""; ?>> OUI
			</label>
			<label class="radio-inline">
				<input type="radio" name="actif" value="0" onclick="activate(0);" <?= ($u->getActive() == 0) ? "checked" : ""; ?>> NON
			</label>
      <span class="glyphicon form-control-feedback"></span>
      <span class="error"></span>
    </div>
  </div>

	<hr>
	
	<?php if(isset($_GET['mode']) && $_GET['mode'] == 'add') : ?>
	  
	  <div class="form-group">
	    <div class="col-sm-offset-3 col-sm-2">
	      <button type="button" class="btn btn-info" data-dismiss="modal">Fermer</button>
	    </div>
	    <div class="col-sm-offset-2 col-sm-2">
	      <button type="button" class="btn btn-success" id="addUser">Ajouter</button>
	    </div>
	  </div>
	  

	
<?php elseif(isset($_GET['mode']) && $_GET['mode'] == 'mod') : ?>
	
	<div class="form-group">
	    <div class="col-sm-offset-2 col-sm-2">
	      <button type="button" class="btn btn-info" data-dismiss="modal">Fermer</button>
	    </div>
	    <div class="col-sm-2">
	      <button type="button" class="btn btn-success" id="saveUser">Sauvegarder</button>
	    </div>
	    <div class="col-sm-6">
	      <button type="button" class="btn btn-danger" id="delUser">Supprimer</button>
	    </div>
	</div>
	
<?php endif; ?>
  
</form>

<script type="text/javascript" src="<?= JS."/md5.js"; ?>"></script>
<script>

	var activer = <?= ($u->getActive()) ? $u->getActive() : 1; ?>;
	
	function activate(val){
		activer = val;
	}
  
  $("input[name=nom]").focusout(function() {
      var nom = $(this).val();
      validInput(nom,"text",$(this),3,25,"alpha");
  });
  
  $("input[name=prenom]").focusout(function() {
      var prenom = $(this).val();
      validInput(prenom,"text",$(this),3,25,"alpha");
  });
  
  $("input[name=login]").focusout(function() {
      var login = $(this).val();
      validInput(login,"text",$(this),3,25,"alpha");
			verifLoginExist();
  });
  
  $("input[name=pass]").focusout(function() {
      var pass = $(this).val();
      validInput(pass,"password",$(this),5,50,"password");
  });
  
  $("input[name=pass2]").focusout(function() {
      var pass2 = $(this).val();
			var pass = $("input[name=pass]").val();
			if(pass2 != pass){
				errorForms('input[name=pass2]', "Les mots de passe ne correspond pas", 'error');
			} else {
				validInput(pass2,"password",$(this),5,50,"password");
			}
      
  });
	
	$("select[name=groupe]").change(function() {
      var groupe = $(this).val();
      validSelect(groupe,"text",$(this),1,5);
  });

function recupDatas(){

		var data = {
			id : $("input[name=id]").val(),
			nom : $("input[name=nom]").val(),
			prenom : $("input[name=prenom]").val(),
			login : $("input[name=login]").val(),
			pass : $("input[name=pass]").val(),
			pass2 : $("input[name=pass2]").val(),
			groupe : $("select[name=groupe]").val(),
			active : activer
		}
		
		return data;
}  


$("#addUser").click(function(){
		
	var data = recupDatas();
	var retour = validSubscribe(data);
	if(retour){
		data.pass = md5(data.pass);
		$.post("/admin/membres/users/register.php",data)
			.always(function(arg) {
				var n = arg.search("Notice");
				if( n > 0 ) {
					arg = "";
				}

				if( arg.trim() != ""){
					viderModal();
					$("#bloc-error").append(arg);
					notif("Erreur lors de l'ajout d'un utilisateur","danger","","");
					
				} else {
					viderModal();
					viewUsers();
					notif("Votre utilisateur à bien été ajouter à la liste","success","","");
				}
				
			});
	}
});

$("#saveUser").click(function(){

	if ( $("input[name=id]").val() == 1 ){
		notif("Vous ne pouvez pas modifier ce compte utilisateur car il est protégé","danger","","");
	} else {

		var data = recupDatas();
		var retour = validSubscribe(data);
		if(retour){
			data.pass = (data.pass.length > 20) ? data.pass : md5(data.pass);
			$.post("/admin/membres/users/register.php",data)
				.always(function(arg) {
					var n = arg.search("Notice");
					if( n > 0 ) {
						arg = "";
					}

					if( arg.trim() != ""){
						viderModal();
						$("#bloc-error").append(arg);
						notif("Erreur lors de la modification d'un utilisateur","danger","","");
						
					} else {
						viderModal();
						viewUsers();
						notif("Votre utilisateur à bien été modifié et mis à jour dans la liste","success","","");
					}
					
				});
		}
	
	}
});

$("#delUser").click(function(){
	if ( $("input[name=id]").val() == 1 ){
		notif("Vous ne pouvez pas supprimer ce compte utilisateur car il est protégé","danger","","");
	} else {
		delUsers($("input[name=id]").val(), "utilisateur");
	}
});

// validation du formulaire d'inscription
function validSubscribe(data){

	var context;

	if(data.nom.trim().length != 0){
		context = $("input[name=nom]");
		validInput(data.nom,"text",context,3,25,"alpha");
	} else {
		context = "input[name=nom]";
		errorForms(context, "Le champ nom est obligatoire pour valider le formulaire", "error");
		return false;
	}
	
	if(data.prenom.trim().length != 0){
		context = $("input[name=prenom]");
		validInput(data.prenom,"text",context,3,25,"alpha");
	} else {
		context = "input[name=prenom]";
		errorForms(context, "Le champ prénom est obligatoire pour valider le formulaire", "error");
		return false;
	}
	
	if(data.login.trim().length != 0){
		context = $("input[name=login]");
		validInput(data.login,"text",context,3,25,"alpha")
	} else {
		context = "input[name=login]";
		errorForms(context, "Le champ login est obligatoire pour valider le formulaire", "error");
		return false;
	}
	
	if(data.pass.trim().length != 0){
		context = $("input[name=pass]");
		validInput(data.pass,"text",context,5,50,"password")
	} else {
		context = "input[name=pass]";
		errorForms(context, "Le champ mot de passe est obligatoire pour valider le formulaire", "error");
		return false;
	}
	
	if(data.pass2.trim().length != 0){
		context = $("input[name=pass2]");
		validInput(data.pass2,"text",context,5,50,"password")
	} else {
		context = "input[name=pass2]";
		errorForms(context, "Les mots de passe ne correspondent pour valider le formulaire", "error");
		return false;
	}
	
	if(data.pass2 != data.pass){
		errorForms('input[name=pass2]', "Les mots de passe ne correspond pas", 'error');
		return false;
	} 
	
	if(data.groupe != -1){
		context = $("select[name=groupe]");
		validSelect(data.pass,"text",context,1,5);
	} else {
		context = "select[name=groupe]";
		errorForms(context, "La sélection d'un groupe est obligatoire pour valider le formulaire", "error");
		return false;
	}
	
	if ( fullDebug ) { msg(data); }
	return true;
	
}

function verifLoginExist(){
	var login = $("input[name=login]").val();
	$.post("/php/requetes/verif_unique.php",{type : "login", valeur : login})
		.always(function(arg) {
			if( arg.trim() == "erreur"){
				notif("Attention ! le login existe déjà, merci d'en saisir un autre","warning","","");
				$("input[name=login]").val('');
				context = "input[name=login]";
				errorForms(context, "Le champ login est obligatoire pour valider le formulaire", "error");
			} else {
				if( arg.trim() != "ok"){
					viderModal();
					$("#bloc-error").append(arg);
					$("input[name=login]").val('');
					context = "input[name=login]";
					errorForms(context, "Le champ login est obligatoire pour valider le formulaire", "error");
				}
			}
		});
}
</script>