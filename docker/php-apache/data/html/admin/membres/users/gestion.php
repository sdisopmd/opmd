<?php

	require_once('../../init.php');

	$users = User::getAllUsers(); 

?>


	<div class="row">
		<div class="col-sm-12">
		
		<div class="panel panel-default">
		
			<div class="panel-heading">
				<h1>GESTION DES UTILISATEURS 
				<button class="btn btn-success pull-right" onclick="addUsers();">Ajouter 
				<span class="glyphicon glyphicon-plus"></span>
				</button>
				</h1> 
			</div>

			
			<table class="table table-bordered table-striped table-hover">
			  <thead>
				<tr class="bg-info">
				  <th class="text-center">ID</th>
				  <th class="text-center">NOM</th>
				  <th class="text-center">PRENOM</th>
				  <th class="text-center">LOGIN</th>
				  <th class="text-center">GROUPE</th>
				  <th class="text-center">ACTIVER</th>
				  <th class="text-center">CONNECTER</th>
				  <th class="text-center">ACTIONS</th>
				</tr>
			  </thead>
			  <tbody>
			  <?php if( $users != null ) : ?>
				  <?php foreach($users as $u) : ?>
					<?php $group = new Group($u->getGroupe()); ?>
					<tr class="text-center">
					  <td><?= $u->getId();?></td>
					  <td><?= $u->getNom();?></td>
					  <td><?= $u->getPrenom();?></td>
					  <td><?= $u->getLogin();?></td>
					  <td><?= $group->getNom();?></td>
					  <td><?= ($u->getActive() == 1 ) ? "<b class='badge-success'>OUI</b>" : "<b class='badge-danger'>NON</b>";?></td>
					  <td><?= ($u->getConnecter() == 1 ) ? "<b class='badge-success'>OUI</b>" : "<b class='badge-danger'>NON</b>";?></td>
					  <td>
							<?php if($u->getId() == 1) : ?>
						  <b>PROTECTION</b> 
							<?php else : ?>
							<button class="btn btn-warning" onclick="modUsers(<?= $u->getId();?>);"><span class="glyphicon glyphicon-edit"></span></button>  - 
						  <button class="btn btn-danger" onclick="delUsers(<?= $u->getId();?>,'utilisateur');"><span class="glyphicon glyphicon-remove"></span></button> 
							<?php endif; ?>
					  </td>
					</tr>
				  <?php endforeach; ?>
			  <?php endif; ?>
			  </tbody>
			</table>
			
		</div>
		
		</div>
	</div>





