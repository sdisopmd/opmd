
<?php
	require_once('../../init.php');
	
	if(isset($_POST['login']) && isset($_POST['pass'])) {
		$login = $_POST['login'];
		$pass = $_POST['pass'];
		$retour = User::login($login,$pass);
		if ( $retour == 'erreur' ) {
			echo "<h3 class='text-danger text-center'>Erreur vous pouvez pas vous connecter car vos identifiants sont incorrect</h3>"; 
		} else {
			$u = new User($retour);
			$_SESSION['users']['id'] = $u->getId();
			$_SESSION['users']['nom'] = $u->getNom();
			$_SESSION['users']['prenom'] = $u->getPrenom();
			$_SESSION['users']['active'] = $u->getActive();
			$_SESSION['users']['groupe_id'] = $u->getGroupe();
			//debug($_SESSION);
		}
	}

?>



	<form class="form-horizontal" role="form" id="form-login" method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>">
		
	  <div class="form-group">
	    <label for="login" class="col-sm-2 control-label">UTILISATEUR</label>
	    <div class="col-sm-6">
	      <input type="text" name="login" maxlength="50" class="form-control">
	    </div>
	  </div>
	  
	  <div class="form-group">
	    <label for="pass" class="col-sm-2 control-label">MOT DE PASSE</label>
	    <div class="col-sm-6">
	    	<input type="password" name="pass" maxlength="50" class="form-control">
	    </div>
	  </div>
	  	  

	  <div class="form-group">
	    <div class="col-sm-offset-6 col-sm-2">
	      <button type="submit" class="btn btn-success">Se connecter</button>
	    </div>
	  </div>
	  
	</form>
	








