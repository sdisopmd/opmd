<?php
/*
* Fichier de chargement des scripts JS
* Fichier crée le 06/04/2014
* Auteur : Denis Sanson alias Multixvers
*/

	$scriptsLibs = array(
		BOOTSTRAP_JS.DS."bootstrap.js",
		GRITTER_JS.DS."gritter.js",
		COLOR.DS."pick-a-color.js",
		COLOR.DS."tinycolor.js",
		LOADER.DS."spin.js"
	);
	
	$scriptsApplis = array(
		JS.DS."utils.js",
		JS.DS."modals.js",
		JS.DS."developpement.js",
		JS.DS."select.js",
		JS.DS."depart.js",
		JS.DS."notif.js",
		JS.DS."menus.js",
		JS.DS."regexp.js",
		JS.DS."validates.js",
		JS.DS."validateur.js",
		JS.DS."boutons.js",
		ADMIN_JS.DS."menu.js",
		ADMIN_JS.DS."validates.js",
		ADMIN_JS.DS."updates.js",
		ADMIN_JS.DS."main.js"
	);

?>

<?php foreach($scriptsLibs as $lib) : ?>
	<script type="text/javascript" src="<?= URL.$lib; ?>"></script>
<?php endforeach; ?>

<?php foreach($scriptsApplis as $app) : ?>
	<script type="text/javascript" src="<?= URL.$app; ?>"></script>
<?php endforeach; ?>
