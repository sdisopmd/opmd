<?php

/*
* Fichier de chargement des styles CSS
* Fichier crée le 06/04/2014
* Auteur : Denis Sanson alias Multixvers
*/



	$stylesLibs = array(
		BOOTSTRAP_CSS.DS."bootstrap.css",
		BOOTSTRAP_CSS.DS."bootstrap-multixvers.css",
		GRITTER_CSS.DS."gritter.css",
		COLOR.DS."pick-a-color.css",
		MENU.DS."menu.css"
	);
	
	$stylesApplis = array(
		CSS.DS."multixvers.css"
	);

?>


<?php foreach($stylesLibs as $lib) : ?>
	<link rel="stylesheet" type="text/css" href="<?= URL.$lib; ?>"/>
<?php endforeach; ?>

<?php foreach($stylesApplis as $app) : ?>
	<link rel="stylesheet" type="text/css" href="<?= URL.$app; ?>"/>
<?php endforeach; ?>