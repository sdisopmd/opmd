<div id="menus">
	<nav class="navbar navbar-default" role="navigation" id="myMenu">
	  <div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
		  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		  </button>
		  <a class="navbar-brand" href="/"><img src="<?= IMG; ?>/pucelle.png" alt="Logo" width="41px" height="45px" style="margin-top:-10px"></a>
		</div>
	
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="menu">
			
		  <ul class="nav navbar-nav" role="menu">
			<li id="gestion-site"><a href="#"><b>APPLICATION</b></a></li>
			<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><b>GESTIONS</b> <b class="caret"></b></a>
				<ul class="dropdown-menu">
					<li id="gestion-vehicules"><a href="#"><b>Véhicules</b></a></li>
					<li id="gestion-materiels"><a href="#"><b>Matériels</b></a></li>
					<li id="gestion-centres"><a href="#"><b>Centres</b></a></li>
					<li id="gestion-groupes"><a href="#"><b>Groupes</b></a></li>
					<li id="gestion-communes"><a href="#"><b>Commune</b></a></li>
					<li id="gestion-natures"><a href="#"><b>Natures</b></a></li>
					<li id="gestion-precisions"><a href="#"><b>Précision</b></a></li>
					<li id="gestion-affectation"><a href="#"><b>Feuille de garde</b></a></li>
				</ul>
			</li>
			<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><b>CONSULTATIONS</b> <b class="caret"></b></a>
				<ul class="dropdown-menu">
					<li id="gestion-consulter"><a href="#"><b>Numéro</b></a></li>
					<li id="gestion-permanence"><a href="#"><b>Permanence</b></a></li>
					<li id="gestion-consignes"><a href="#"><b>Consignes</b></a></li>
					<li id="gestion-situation"><a href="#" onclick="window.open('<?= PAGES; ?>/point_de_situation.php');"><b>Point de situation</b></a></li>
					<li id="gestion-recap"><a href="#" onclick="window.open('<?= PAGES; ?>/feuille_recapitulatif.php');"><b>Récapitulatif</b></a></li>
					<li id="gestion-depart"><a href="#" /><b>Feuille de départ</b></a></li>
				</ul>
			</li>
			<li id="admin-user"><a href="#"><b>UTILISATEURS</b></a></li>
			
			<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><b style="color:purple">OPERATION <span class="glyphicon glyphicon-wrench"></span></b> <b class="caret"></b></a>
				<ul class="dropdown-menu">
					<li id="modal-vidange"><a href="#" /><b style="color:blue">VIDANGE <span class="glyphicon glyphicon-trash"></span></b></a></li>
					<li id="gestion-export"><a href="#" /><b style="color:green">SAUVEGARDE <span class="glyphicon glyphicon-cloud-download"></span></b></a></li>
					<li id="gestion-import"><a href="#" /><b style="color:red">RESTAURATION <span class="glyphicon glyphicon-cloud-upload"></span></b></a></li>
				</ul>
			</li>
			<li id="modal-search-codis"><a href="#" /><b style="color:green"><span class="glyphicon glyphicon-search"></span> N° CODIS</b></a></li>
			
			<?php 
				$u2 = new User(2);
				if(User::isLogin() != null){
					$u = User::isLogin();
					$u1 = $u->getId();
				}
			?>
			
		  </ul>
		  <ul class="nav navbar-nav pull-right" role="menu">
			<?php if(User::isLogin() != null) : ?>
			<li><a href="#" id="deco" data="<?=  User::isLogin()->getId();?>"><b><?= User::isAuth()->getNom()." : ".User::isLogin()->getNomFormat(); ?></b><b class="badge-success" style="margin-left:10px"> Déconnexion</b></a></li>
			<?php endif; ?>
			<li><a href="#"><b id="horloge">12h00:00</b></a></li>
		  </ul>
			<?php if(User::isLogin()->getId() == 1) : ?>
			<p id="block-msgs"><b id="messagerie"><span class="glyphicon glyphicon-comment"></span><span id="nb-msgs">10</span></b></a></p>
			<?php endif; ?>
		</div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>
	
</div>