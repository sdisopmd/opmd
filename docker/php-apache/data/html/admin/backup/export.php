<?php
require_once('../init.php');

if ( !isset($_SESSION['users']) ){
	$serveur = "Location: http://".$_SERVER['SERVER_NAME'];
	//echo $serveur;
	header($serveur);
} else {



?>


<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>EXPORTATION</title>
		
		<?php
			$stylesLibs = array(
				BOOTSTRAP_CSS.DS."bootstrap.css",
				BOOTSTRAP_CSS.DS."bootstrap-unixvers.css",
				GRITTER_CSS.DS."gritter.css"
			);
			
			$stylesApplis = array(
				CSS.DS."unixvers.css"
			);
		?>


		<?php foreach($stylesLibs as $lib) : ?>
			<link rel="stylesheet" type="text/css" href="<?= URL.$lib; ?>"/>
		<?php endforeach; ?>

		<?php foreach($stylesApplis as $app) : ?>
			<link rel="stylesheet" type="text/css" href="<?= URL.$app; ?>"/>
		<?php endforeach; ?>
		
		<link rel="shortcut icon" href="/favicon.ico">
		<script type="text/javascript" src="/libs/jquery/jquery-2.1.0.js"></script>
	</head>
	
	<body id="page-export">
	
	
	<div id="bloc-export">
	
		<div id="export">
		
		<div id="titre-export" class="bg-success">
			<h1 class="text-center">EXPORTATION DE LA BASE DE DONNEE</h1>
		</div>
		
		
		<div class="panel panel-info">
			<div class="panel-heading">Processus d'exportation</div>
			<div class="panel-body">
				
				<ul class="list-group">
					<li class="list-group-item"><span class="gauche"><b>Etape 1 : Vérification des droits....</b></span><span class="centre"><b>|</b></span><span class="droite"><b></b><span class="ico"></span></span></li>
					
					<li class="list-group-item"><span class="gauche"><b>Etape 2 : Connexion à la base de donnée....</b></span><span class="centre"><b>|</b></span><span class="droite"><b></b><span class="ico"></span></span></li>
					
					<li class="list-group-item"><span class="gauche"><b>Etape 3 : Exportation des données en cours....</b></span><span class="centre"><b>|</b></span><span class="droite"><b></b><span class="ico"></span></span></li>
					
					<li class="list-group-item"><span class="gauche"><b>Etape 4 : La sauvegarde a été effectué le DATE HEURE</b></span><span class="centre"><b>|</b></span><span class="droite"><b></b><span class="ico"></span></span></li>
					
					<li class="list-group-item"><span class="gauche"><b>Etape 5 : Déplacement du fichier en vue du téléchargement....</b></span><span class="centre"><b>|</b></span><span class="droite"><b></b><span class="ico"></span></span></li>
					
					<li class="list-group-item"><span><b>Etape 6 : <a type="button" class="btn btn-success" id="download">Télécharger la sauvegarde <span class="fichier"></span></a></b></span></li>
					
					<li class="list-group-item"><span class="gauche"><b>Etape 7 : Suppression du fichier temporaire....</b></span><span class="centre"><b>|</b></span><span class="droite"><b></b><span class="ico"></span></span></li>
					
					<li class="list-group-item"><span class="gauche"><b>Etape 8 : Opération terminé actualisation automatique dans </b></span><span class="centre"><b>|</b></span><span class="droite"><b></b><span class="ico"></span></span></li>
					
				</ul>
			</div>
		</div>
		

		
		</div>
	
	</div>

	<div id="bloc-error"></div>
	
	<?php
		$scriptsLibs = array(
			BOOTSTRAP_JS.DS."bootstrap.js",
			GRITTER_JS.DS."gritter.js",
			LOADER.DS."spin.js"
		);
		
		$scriptsApplis = array(
			JS.DS."utils.js",
			JS.DS."notif.js"
		);

	?>

	<?php foreach($scriptsLibs as $lib) : ?>
		<script type="text/javascript" src="<?= URL.$lib; ?>"></script>
	<?php endforeach; ?>

	<?php foreach($scriptsApplis as $app) : ?>
		<script type="text/javascript" src="<?= URL.$app; ?>"></script>
	<?php endforeach; ?>
	
	<script>
	
	
		var ok = "<span class='glyphicon glyphicon-ok pull-right'></span>";
		var no = "<span class='glyphicon glyphicon-remove pull-right'></span>";

		$(document).ready(function(e){
			
			$('li').hide();
			
			verifUser()
			
		});
		
		// verifie si l'utilisateur et au moins un administrateur
		function verifUser(){
				$('li').eq(0).show();
				$.post('/admin/backup/exportDB.php',{ save:true, auth:true}).always(function(arg){
						if(arg.rep == "ok"){
							$('.droite b').eq(0).text(arg.groupe+" : "+arg.name+" ").css({color:"green"});
							$('.ico').eq(0).css({color:"green"}).html(ok);
							$('li').eq(1).show();
							setTimeout( function(){ exportDeLaBase(); }, 500);
						} else {
							notif("Vous n'avez pas le droit d'effectuer cette opération","danger","","");
						}
				});
		}
		
		// exportation du fichier sql ( date heure nom )
		function exportDeLaBase(){
			$('li').eq(2).show();
				$('.droite b').eq(1).text("Vous etes bien connecté à la base de donnée").css({color:"green"});
				$('.ico').eq(1).css({color:"green"}).html(ok);
				$.post('/admin/backup/exportDB.php',{ save:true, export:true}).always(function(arg){
						if(arg.rep == "ok"){
							var name = arg.name;
							var jour = arg.jour;
							var heure = arg.heure;
							var fichier = arg.fichier;
							var dossier = arg.dossier;
							$('.droite b').eq(2).text("Le fichier "+fichier+" vient d'être exporté").css({color:"green"});
							$('.ico').eq(2).css({color:"green"}).html(ok);
							$('li').eq(3).show();
							setTimeout( function(){ sauvegarde(fichier,jour,heure); }, 500);
						} else {
							notif("Vous n'avez pas le droit d'effectuer cette opération","danger","","");
						}
				});
		}
		
		// déplacement du fichier en vue du téléchargement ( date heure nom )
		function sauvegarde(fichier,jour,heure){
				$('li').eq(4).show();
				$('.droite b').eq(3).text("La sauvegarde à été effectuée le "+jour+ " à "+ heure).css({color:"green"});
				$('.ico').eq(3).css({color:"green"}).html(ok);
				setTimeout( function(){ deplacement(fichier,jour,heure); }, 500);
		}
		
		// déplacement du fichier en vue du téléchargement ( date heure nom )
		function deplacement(fichier,date,heure){
				$('.droite b').eq(4).text("Le fichier est en cours de déplacement").css({color:"green"});
				$('.ico').eq(4).css({color:"green"}).html(ok);
				$.post('/admin/backup/exportDB.php',{ save:true, move:true, file:fichier}).always(function(arg){
						if(arg.rep == "ok"){
							var url = arg.url;
							setTimeout( function(){ telechargement(fichier,url); }, 1000);
						} else {
							notif("Vous n'avez pas le droit d'effectuer cette opération","danger","","");
						}
				});
				
		}
		
		// téléchargement du fichier ( date heure nom )
		function telechargement(fichier,url){
			$('.fichier').text(fichier);
			$('#download').attr('href',url);
			$('li').eq(5).show();
			$('#download').on('click',function(){
					$('li').eq(6).show();
					setTimeout( function(){ nettoyage(fichier); }, 3000);
			});
		}
		
		// suppression du fichier temporaire ( date heure nom )
		function nettoyage(fichier){
				$('.droite b').eq(5).text("Nettoyage du fichier sur le serveur en cours...").css({color:"green"});
				$('.ico').eq(5).css({color:"green"}).html(ok);
				$.post('/admin/backup/exportDB.php',{ save:true, delete:true, file:fichier}).always(function(arg){
						if(arg.rep == "ok"){
							var url = arg.url;
							$('li').eq(7).show();
							setTimeout( function(){ actualisation3(url); }, 500);
						} else {
							notif("Vous n'avez pas le droit d'effectuer cette opération","danger","","");
						}
				});
		}
		
		// retour a l'application 3
		function actualisation3(url){
				$('.droite b').eq(6).text("3").css({color:"green"});
				$('.ico').eq(6).css({color:"green"}).html(ok);
				setTimeout( function(){ actualisation2(url); }, 1000);
		}
		
		// retour a l'application 2
		function actualisation2(url){
				$('.droite b').eq(6).text("2").css({color:"green"});
				$('.ico').eq(6).css({color:"green"}).html(ok);
				setTimeout( function(){ actualisation1(url); }, 1000);
		}
		
		// retour a l'application 1
		function actualisation1(url){
				$('.droite b').eq(6).text("1").css({color:"green"});
				$('.ico').eq(6).css({color:"green"}).html(ok);
				setTimeout( function(){ actualisation(url); }, 1000);
		}
		
		// retour a l'application
		function actualisation(url){
				location.assign(url);
		}
		
	</script>
	
	<style>
		
		.gauche {
			display:inline-block;
			width:50%;
		}
		
		.centre {
			display:inline-block;
			width:1%;
			text-align:center;
		}
		
		.droite {
			display:inline-block;
			margin-left:30px;
			width:44%;
		}
		
		#download {
			display:inline-block;
			width:40%;
			margin-left:25%;
		}
	
	</style>
	
	</body>
	
</html>

<?php
}
?>