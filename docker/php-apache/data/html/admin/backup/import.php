<?php
require_once('../init.php');

if ( !isset($_SESSION['users']) ){
	$serveur = "Location: http://".$_SERVER['SERVER_NAME'];
	//echo $serveur;
	header($serveur);
} else {



?>


<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>IMPORTATION</title>
		
		<?php
			$stylesLibs = array(
				BOOTSTRAP_CSS.DS."bootstrap.css",
				BOOTSTRAP_CSS.DS."bootstrap-unixvers.css",
				GRITTER_CSS.DS."gritter.css"
			);
			
			$stylesApplis = array(
				CSS.DS."unixvers.css"
			);
		?>


		<?php foreach($stylesLibs as $lib) : ?>
			<link rel="stylesheet" type="text/css" href="<?= URL.$lib; ?>"/>
		<?php endforeach; ?>

		<?php foreach($stylesApplis as $app) : ?>
			<link rel="stylesheet" type="text/css" href="<?= URL.$app; ?>"/>
		<?php endforeach; ?>
		
		<link rel="shortcut icon" href="/favicon.ico">
		<script type="text/javascript" src="/libs/jquery/jquery-2.1.0.js"></script>
	</head>
	
	<body id="page-import">
	
	
	<div id="bloc-import">
	
		<div id="import">
		
		<div id="titre-import" class="bg-success">
			<h1 class="text-center">IMPORTATION DE LA BASE DE DONNEE</h1>
		</div>
		
		<div class="panel panel-info">
			<div class="panel-heading">Processus d'importation</div>
			<div class="panel-body">
				
				<ul class="list-group">
					<li class="list-group-item"><span class="gauche"><b>Etape 1 : Vérification des droits....</b></span><span class="centre"><b>|</b></span><span class="droite"><b></b><span class="ico"></span></span></li>
					
					<li class="list-group-item"><span class="gauche"><b>Etape 2 : Connexion à la base de donnée....</b></span><span class="centre"><b>|</b></span><span class="droite"><b></b><span class="ico"></span></span></li>
					
					<li class="list-group-item"><span class="gauche"><b>Etape 3 : Importation du fichier de sauvegarde</b></span><span class="centre"><b>|</b></span><span class="droite"><input id="fileupload" type="file" name="files[]" data-url="php/"><b></b><span class="ico"></span></span></li>
					
					<li class="list-group-item"><span class="gauche"><b>Etape 4 : Déplacement du fichier en vue de la restauration</b></span><span class="centre"><b>|</b></span><span class="droite"><b></b><span class="ico"></span></span></li>
					
					<li class="list-group-item"><span class="gauche"><b>Etape 5 : Effacement de la base de donnée</b></span><span class="centre"><b>|</b></span><span class="droite"><b></b><span class="ico"></span></span></li>
					
					<li class="list-group-item"><span class="gauche"><b>Etape 6 : Restauration des données en date du <span class="date"></span></b></span><span class="centre"><b>|</b></span><span class="droite"><b></b><span class="ico"></span></span></li>
					
					<li class="list-group-item"><span class="gauche"><b>Etape 7 : Suppression des fichiers temporaires....</b></span><span class="centre"><b>|</b></span><span class="droite"><b></b><span class="ico"></span></span></li>
					
					<li class="list-group-item"><span class="gauche"><b>Etape 8 : Opération terminé actualisation automatique dans </b></span><span class="centre"><b>|</b></span><span class="droite"><b></b><span class="ico"></span></span></li>
					
				</ul>
				
			</div>
		</div>
		
		
		</div>
	
	</div>

	<div id="bloc-error"></div>
	
	<?php
		$scriptsLibs = array(
			BOOTSTRAP_JS.DS."bootstrap.js",
			GRITTER_JS.DS."gritter.js",
			LOADER.DS."spin.js"
		);
		
		$scriptsApplis = array(
			JS.DS."utils.js",
			JS.DS."notif.js"
		);

	?>

	<?php foreach($scriptsLibs as $lib) : ?>
		<script type="text/javascript" src="<?= URL.$lib; ?>"></script>
	<?php endforeach; ?>

	<?php foreach($scriptsApplis as $app) : ?>
		<script type="text/javascript" src="<?= URL.$app; ?>"></script>
	<?php endforeach; ?>
	
	<script src="/libs/jquery/jquery.upload.js"></script>
	<script src="/libs/jquery/jquery.iframe-transport.js"></script>
	<script src="/libs/jquery/jquery.fileupload.js"></script>
	
	<script>

		var ok = "<span class='glyphicon glyphicon-ok pull-right'></span>";
		var no = "<span class='glyphicon glyphicon-remove pull-right'></span>";
		var fichierdb = "";
		
		$(document).ready(function(e){
			
			$('li').hide();
			
			verifUser();
			
			$('#fileupload').fileupload({
        dataType: 'json',
        done: function (e, data) {
            $.each(data.result.files, function (index, file) {
								fichierdb = file.name;
								telechargement(fichierdb);
            });
        }
			});
			
		});
		
		// verifie si l'utilisateur et au moins un administrateur
		function verifUser(){
				$('li').eq(0).show();
				$.post('/admin/backup/importDB.php',{ restaure:true, auth:true}).always(function(arg){
						if(arg.rep == "ok"){
							$('.droite b').eq(0).text(arg.groupe+" : "+arg.name+" ").css({color:"green"});
							$('.ico').eq(0).css({color:"green"}).html(ok);
							$('li').eq(1).show();
							setTimeout( function(){ importBase(); }, 500);
						} else {
							notif("Vous n'avez pas le droit d'effectuer cette opération","danger","","");
						}
				});
		}
		
		// importation du fichier sql ( date heure nom )
		function importBase(){
				$('li').eq(2).show();
				$('.droite b').eq(1).text("Vous etes bien connecté à la base de donnée").css({color:"green"});
				$('.ico').eq(1).css({color:"green"}).html(ok);
		}
		
		// upload du fichier sql (date heure user )
		function telechargement(fichier){
				$('#fileupload').remove();
				$('.droite b').eq(2).text("Le fichier : "+fichier+" est téléchargé").css({color:"green"});
				$('.ico').eq(2).css({color:"green"}).html(ok);
				$('li').eq(3).show();
				setTimeout( function(){ deplacement(fichier); }, 500);
		}
		
		// deplacement du fichier pour importation (date heure user )
		function deplacement(fichier){
				$.post('/admin/backup/importDB.php',{ restaure:true, move:true, file:fichier}).always(function(arg){
						if(arg.rep == "ok"){
							var jour = arg.jour;
							var heure = arg.heure;
							$('.droite b').eq(3).text("Le fichier est prêt pour la restauration").css({color:"green"});
							$('.ico').eq(3).css({color:"green"}).html(ok);
							$('.date').text(jour+" à "+heure);
							$('li').eq(4).show();
							setTimeout( function(){ effacement(fichier); }, 1000);
						} else {
							notif("Vous n'avez pas le droit d'effectuer cette opération","danger","","");
						}
				});
		}
		
		// effacement de la base de donnée
		function effacement(fichier){
				$.post('/admin/backup/importDB.php',{ restaure:true, vider:true}).always(function(arg){
						if(arg.rep == "ok"){
							var nom = arg.name;
							$('.droite b').eq(4).text("La base de donnée est vidé").css({color:"green"});
							$('.ico').eq(4).css({color:"green"}).html(ok);
							setTimeout( function(){ $('li').eq(5).show(); }, 1000);
							setTimeout( function(){ importDeLaBase(fichier,nom); }, 2000);
						} else {
							notif("Vous n'avez pas le droit d'effectuer cette opération","danger","","");
						}
				});
		}
		
		// importation des données depuis le fichier (date heure user )
		function importDeLaBase(fichier,nom){
				$.post('/admin/backup/importDB.php',{ restaure:true, insert:true, file:fichier, name:nom}).always(function(arg){
						if(arg.rep == "ok"){
							$('.droite b').eq(5).text("Toutes les données sont restaurées").css({color:"green"});
							$('.ico').eq(5).css({color:"green"}).html(ok);
							setTimeout( function(){ $('li').eq(6).show(); }, 1000);
							setTimeout( function(){ nettoyage(fichier); }, 3000);
						} else {
							notif("Vous n'avez pas le droit d'effectuer cette opération","danger","","");
						}
				});
		}
		
		// suppression du fichier temporaire ( date heure nom )
		function nettoyage(fichier){
				$('.droite b').eq(6).text("Nettoyage des fichiers sur le serveur en cours...").css({color:"green"});
				$('.ico').eq(6).css({color:"green"}).html(ok);
				$.post('/admin/backup/importDB.php',{ restaure:true, delete:true, file:fichier}).always(function(arg){
						if(arg.rep == "ok"){
							var url = arg.url;
							$('li').eq(7).show();
							setTimeout( function(){ actualisation3(url); }, 500);
						} else {
							notif("Vous n'avez pas le droit d'effectuer cette opération","danger","","");
						}
				});
		}
		
		// retour a l'application 3
		function actualisation3(url){
				$('.droite b').eq(7).text("3").css({color:"green"});
				$('.ico').eq(7).css({color:"green"}).html(ok);
				setTimeout( function(){ actualisation2(url); }, 1000);
		}
		
		// retour a l'application 2
		function actualisation2(url){
				$('.droite b').eq(7).text("2").css({color:"green"});
				$('.ico').eq(7).css({color:"green"}).html(ok);
				setTimeout( function(){ actualisation1(url); }, 1000);
		}
		
		// retour a l'application 1
		function actualisation1(url){
				$('.droite b').eq(7).text("1").css({color:"green"});
				$('.ico').eq(7).css({color:"green"}).html(ok);
				setTimeout( function(){ actualisation(url); }, 1000);
		}
		
		// retour a l'application
		function actualisation(url){
				location.assign(url);
		}
		
	</script>
	
	<style>
		
		.gauche {
			display:inline-block;
			width:50%;
		}
		
		.centre {
			display:inline-block;
			width:1%;
			text-align:center;
		}
		
		.droite {
			display:inline-block;
			margin-left:30px;
			width:44%;
		}
		
		.gauchesp {
			display:inline-block;
			width:35%;
		}
		
		.centresp {
			display:inline-block;
			width:1%;
			text-align:center;
		}
		
		.droitesp {
			display:inline-block;
			margin-left:30px;
			width:61%;
		}
		
		.droitesp button {
			display:inline-block;
			margin-left:30px;
		}
		
		.droitesp input {
			display:inline-block;
			width:80%;
		}
		
		#download {
			display:inline-block;
			width:40%;
			margin-left:25%;
		}
	
	</style>
	
	</body>
	
</html>

<?php
}
?>