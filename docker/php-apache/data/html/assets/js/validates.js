/*
 * Fonction de validation des formulaires
 * Fichier crée le 06/04/2014
 * Auteur : Denis Sanson alias Multixvers
 */

// Variable global
var overlap;

/*-------------------- VALIDATION DES FORM --------------------*/

function error(context, msg) {
    if ($("#error", context).html() != "") {
        $("#error", context).animate({ opacity: '1', height: '80px' }, 0);
        $("#error", context).show();
    }
    $("#error", context).html(msg);
    $("#error", context).addClass("ui-state-error ui-corner-all");
    $("#error", context).css("text-align", "center");
    $("#error", context).css("margin-bottom", "2px");
    $("#error", context).css("z-index", "-5");
    $("#error", context).delay(3000).animate({ opacity: '0', height: '0px' }, 1000).hide(0);
    notif(msg, "danger", "", "");

}

function valid_heure(heure) {
    var temp = new Date();
    if (heure == "") {
        return false;
    }
    if (heure.length == 4) {
        var h = heure.substr(0, 2);
        var m = heure.substr(2, 2);
        temp.setHours(h, m);
        if ((temp.getHours() < 0 || temp.getHours() > 23) && (temp.getMinutes() < 0 || temp.getMinutes() > 59)) {
            return false;
        } else {
            return true;
        }
    } else {
        return false;
    }
}

function valid_personnel(form, data) {

    if (!$.trim(data.nom).length) {
        error(form, "Nom vide");
        return false;
    }

    if (!$.trim(data.prenom).length) {
        error(form, "Prénom vide");
        return false;
    }

    updateIntelligent();
    if (fullDebug) { msg(data) };
    return true;
}

function valid_vehicule(form, data) {

    if (!$.trim(data.libelle).length) {
        error(form, "Libellé vide");
        return false;
    }

    if (data.centre == -1) {
        error(form, "Aucun centre sélectionné");
        return false;
    }

    if (data.groupe == -1) {
        error(form, "Aucun groupe sélectionné");
        return false;
    }

    if (fullDebug) { msg(data) };
    return true;
}

function valid_materiel(form, data) {

    if (!$.trim(data.libelle).length) {
        error(form, "Libellé vide");
        return false;
    }

    if (data.centre == -1) {
        error(form, "Aucun centre sélectionné");
        return false;
    }

    updateIntelligent();
    update_menu();
    if (fullDebug) { msg(data) };
    return true;
}

function valid_nature(form, data) {

    if (!$.trim(data.libelle).length) {
        error(form, "Libellé vide");
        return false;
    }

    if (fullDebug) { msg(data) };
    return true;
}

function valid_precision(form, data) {

    if (!$.trim(data.libelle).length) {
        error(form, "Libellé vide");
        return false;
    }

    if (data.nature == -1) {
        error(form, "Aucune nature sélectionné");
        return false;
    }

    updateIntelligent();
    if (fullDebug) { msg(data) };
    return true;
}

function valid_groupe(form, data) {

    if (!$.trim(data.libelle).length) {
        error(form, "Libellé vide");
        return false;
    }

    if (data.position == -1) {
        error(form, "Position vide");
        return false;
    }

    updateIntelligent();
    update_menu();
    if (fullDebug) { msg(data) };
    return true;
}

function valid_centre(form, data) {

    if (!$.trim(data.libelle).length) {
        error(form, "Libellé vide");
        return false;
    }

    if (data.centre == -1) {
        error(form, "Aucun centre sélectionné");
        return false;
    }

    updateIntelligent();
    if (fullDebug) { msg(data) };
    return true;
}

function valid_commune(form, data) {

    if (!$.trim(data.libelle).length) {
        error(form, "Libellé vide");
        return false;
    }

    if (!data.cp.length) {
        error(form, "Code postal vide");
        return false;
    }

    updateIntelligent();
    if (fullDebug) { msg(data) };
    return true;
}

function valid_permanence(form, data) {

    if (!$.trim(data.nature).length) {
        error(form, "Nature vide");
        return false;
    }

    if (data.id == 0 || !isFinite(data.id)) {
        error(form, "Numéro de permanence invalide");
        return false;
    }

    if (!(dt = isDatetimeValid(data.debut))) {
        error(form, "Date-heure de début incorrecte");
        return false;
    } else {
        overlap = false;
        // ajax synchrone pour que le getScript finisse de s'exécuter avant de passer à la suite
        jQuery.ajaxSetup({ async: false });
        $.getScript("/php/requetes/is_time_overlapping_perm.php?dt=" + dt);
        jQuery.ajaxSetup({ async: true });
        if (overlap) {
            error(form, "Date-heure de début chevauche une permanence existante");
            return false;
        }
    }

    if (data.fin == "") {
        data.fin = 0;
    }

    if (data.fin > 0) {
        if (!(dt2 = isDatetimeValid(data.fin))) {
            error(form, "Date-heure de fin incorrecte");
            return false;
        } else {
            overlap = false;
            // ajax synchrone pour que le getScript finisse de s'exécuter avant de passer à la suite
            jQuery.ajaxSetup({ async: false });
            $.getScript("/php/requetes/is_time_overlapping_perm.php?dt=" + dt2);
            jQuery.ajaxSetup({ async: true });
            if (overlap) {
                error(form, "Date-heure de fin chevauche une permanence existante");
                return false;
            } else if (dt > dt2) {
                error(form, "Date-heure de fin antérieure à date-heure de début");
                return false;
            }
        }
    }

    updateIntelligent();
    if (fullDebug) { msg(data) };
    return true;
}

function validPermanence(form) {
    try {
        var r = true;

        if (form.id.value.length == 0 || !isFinite(form.id.value)) {
            r = false;
            alert("Numéro de permanence invalide.");
        }

        if (!(dt = isDatetimeValid(form.debut.value))) {
            r = false;
            alert("Date-heure de début incorrecte.");
        } else {
            overlap = false;
            // ajax synchrone pour que le getScript finisse de s'exécuter avant de passer à la suite
            jQuery.ajaxSetup({ async: false });
            $.getScript("/php/requetes/is_time_overlapping_perm.php?dt=" + dt);
            jQuery.ajaxSetup({ async: true });
            if (overlap) {
                r = false;
                alert("Date-heure de début chevauche une permanence existante.");
            }
        }

        if (form.fin.value.length > 0) {
            if (!(dt2 = isDatetimeValid(form.fin.value))) {
                r = false;
                alert("Date-heure de fin incorrecte.");
            } else {
                overlap = false;
                // ajax synchrone pour que le getScript finisse de s'exécuter avant de passer à la suite
                jQuery.ajaxSetup({ async: false });
                $.getScript("/php/requetes/is_time_overlapping_perm.php?dt=" + dt2);
                jQuery.ajaxSetup({ async: true });
                if (overlap) {
                    r = false;
                    alert("Date-heure de fin chevauche une permanence existante.");
                } else if (dt > dt2) {
                    r = false;
                    alert("Date-heure de fin antérieure à date-heure de début.");
                }
            }
        }
        updateIntelligent();
        if (fullDebug) { msg(data) };
        return r;
    } catch (e) {
        if (fullDebug) { msg(e) };
        return false;
    }
}

function validCompteRendu(form) {
    try {
        var r = true;

        if (form.personnel.selectedIndex == 0) {
            r = false;
            alert("Sélectionner l'auteur du compte rendu.");
        }
        updateIntelligent();
        if (fullDebug) { msg(data) };
        return r;
    } catch (e) {
        if (fullDebug) { msg(e) };
        return false;
    }
}