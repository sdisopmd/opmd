/*
 * Fonction des notification
 * Fichier crée le 07/04/2014
 * Auteur : Denis Sanson alias Multixvers
 */


/*-------------------- FONCTIONS DES NOTIFICATIONS --------------------*/

function notif(texte, type, temps, options) {

    if (options == "") {
        options = false;
    }

    if (temps == "") {
        temps = 3000;
    }

    if (type == "info") {
        titre = "INFORMATION : ";
        icone = "info-sign";
        classe = "notif-info";
    }

    if (type == "success") {
        titre = "REUSSITE : ";
        icone = "ok-sign";
        classe = "notif-success";
    }

    if (type == "warning") {
        titre = "ATTENTION : ";
        icone = "warning-sign";
        classe = "notif-warning";
    }

    if (type == "danger") {
        titre = "ERREUR : ";
        icone = "exclamation-sign";
        classe = "notif-danger";
    }

    $.gritter.add({
        title: titre,
        text: texte,
        icon: icone,
        // (bool | optional) if you want it to fade out on its own or just sit there
        sticky: options,
        // (int | optional) the time you want it to be alive for before fading out
        time: temps,
        class_name: classe
    });
}