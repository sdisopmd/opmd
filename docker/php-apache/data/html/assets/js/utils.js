/*
 * Fonction utilitaires gestion de certaine fonctionnalité 
 * Fichier crée le 06/04/2014
 * Auteur : Denis Sanson alias Multixvers
 */

// Variable global
var debug = true;

/*-------------------- GESTION DU DEBOGGAGE --------------------*/

// function de debug
function msg(data) {
    console.log(data);
}

/*-------------------- GESTION DU TEMPS --------------------*/

function formatDate(formatDate, formatString) {
    if (formatDate instanceof Date) {
        var yyyy = formatDate.getFullYear();
        var yy = yyyy.toString().substring(2);
        var m = formatDate.getMonth() + 1;
        var mm = m < 10 ? "0" + m : m;
        var d = formatDate.getDate();
        var dd = d < 10 ? "0" + d : d;

        var h = formatDate.getHours();
        var hh = h < 10 ? "0" + h : h;
        var n = formatDate.getMinutes();
        var nn = n < 10 ? "0" + n : n;
        var s = formatDate.getSeconds();
        var ss = s < 10 ? "0" + s : s;

        formatString = formatString.replace(/yyyy/i, yyyy);
        formatString = formatString.replace(/yy/i, yy);
        formatString = formatString.replace(/mm/i, mm);
        formatString = formatString.replace(/m/i, m);
        formatString = formatString.replace(/dd/i, dd);
        formatString = formatString.replace(/d/i, d);
        formatString = formatString.replace(/hh/i, hh);
        formatString = formatString.replace(/h/i, h);
        formatString = formatString.replace(/nn/i, nn);
        formatString = formatString.replace(/n/i, n);
        formatString = formatString.replace(/ss/i, ss);
        formatString = formatString.replace(/s/i, s);

        return formatString;
    } else {
        return "";
    }
}


function isDatetimeValid(s) {
    var t = 0;
    var dtPattern = new RegExp("^[0-9]{2}[/][0-9]{2}[/][0-9]{4} [0-9]{2}[:][0-9]{2}$");
    if (dtPattern.test(s)) {
        var tab = s.split(' ');
        var tabD = tab[0].split('/');
        var tabH = tab[1].split(':');
        var d = new Date(tabD[2], tabD[1] - 1, tabD[0], tabH[0], tabH[1]);
        if (d.getDate() == tabD[0] && d.getMonth() == tabD[1] - 1 && d.getFullYear() == tabD[2] && d.getHours() == tabH[0] && d.getMinutes() == tabH[1]) {
            t = d.getTime() / 1000;
        }
    }
    return t;
}

function reformDate(date) {

    var test = date.search('-');
    if (test > 1) {
        var y = date.substr(0, 4);
        var m = date.substr(5, 2);
        var d = date.substr(8, 2);
        var date = d + "/" + m + "/" + y;
    }
    return date;
}

function affiche_heure() {
    var d = new Date();
    var heure = d.getHours();
    var minute = (d.getMinutes() < 10) ? "0" + d.getMinutes() : d.getMinutes();
    var seconde = (d.getSeconds() < 10) ? "0" + d.getSeconds() : d.getSeconds();
    var temps = heure + "h" + minute + ":" + seconde;
    $('#horloge').html(temps);
}


function setDHnow(elem) {
    $(elem).val('');
    $(elem).val(formatDate(new Date(), "dd/mm/yyyy hh:nn"));
}

function setDnow(elem) {
    $(elem).val('');
    $(elem).val(formatDate(new Date(), "dd/mm/yyyy"));
}

function setHnow(elem) {
    $(elem).val('');
    $(elem).val(formatDate(new Date(), "hhnn"));
}

/*-------------------- GESTION DU TEXTE --------------------*/

function trim(s) {
    return s.replace(/^\s+/g, '').replace(/\s+$/g, '');
}