/*
 * Patterns des Expressions rationnel
 * Fichier crée le 14/04/2014
 * Auteur : Denis Sanson alias Multixvers
 */

/* ---------------- PATTERN DES REGEXP ----------------------- */

// pour les caractères alphabétiques uniquement (majuscule et minuscule) "sauf les spéciaux"
const regexAlpha = /[^a-zA-ZÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ-\s\']/g

// pour les caractères alphabétiques uniquement (majuscule) "sauf les spéciaux"
const regexAlphaMaj = /[^A-ZÀÁÂÃÄÅÒÓÔÕÖØÈÉÊËÇÌÍÎÏÙÚÛÜÑ-\s\']/g

// pour les caractères alphabétiques uniquement (minuscule) "sauf les spéciaux"
const regexAlphaMin = /[^a-zàáâãäåòóôõöøèéêëçìíîïùúûüÿñ-\s\']/g

// pour les caractères numérique uniquement "sauf les spéciaux"
const regexNum = /[^0-9]/g

// pour les caractères alphanumérique (majuscule et minuscule) "sauf les spéciaux"
const regexAlphaNum = /[^a-zA-Z0-9ÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ-\s\']/g

// pour les caractères alphanumérique (majuscule) "sauf les spéciaux"
const regexAlphaNumMaj = /[^A-Z0-9ÀÁÂÃÄÅÒÓÔÕÖØÈÉÊËÇÌÍÎÏÙÚÛÜÑ-\s\']/g

// pour les caractères alphanumérique (minuscule) "sauf les spéciaux"
const regexAlphaNumMin = /[^a-z0-9àáâãäåòóôõöøèéêëçìíîïùúûüÿñ-\s\']/g

// validateur selon RFC2822
const regexMail = new RegExp('^[0-9a-z._-]+@{1}[0-9a-z.-]{2,}[.]{1}[a-z]{2,5}$', 'i');

// pour les caractères alphabétique, numérique et quelques caractères spéciaux ( & ~ # ^ @ = $ * ! : ; , ? . / § - + ° )
const regexPassword = /[^a-zA-Z0-9ÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ-\s\'\&\~\#\^\@\=\$\*\!\:\;\,\§\/\.\?\-\+\°]/g
