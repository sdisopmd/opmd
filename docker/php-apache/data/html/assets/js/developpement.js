/*
 * Fichier d'application principal
 * Fichier crée le 07/04/2014
 * Auteur : Denis Sanson alias Multixvers
 */

// BASE

function vidanger() {
    creerModal("vider-bdd", "VIDANGER LA BASE DE DONNEE", "vbdd", '/php/developpement/vidange.php', "get", 'bg-danger', "");
}

function exportDB() {
    window.open('/admin/backup/export.php');
}

function importDB() {
    window.open('/admin/backup/import.php');
}