/*
 * Fonction des boutons pour la gestion des intervention
 * Fichier crée le 12/05/2014
 * Auteur : Denis Sanson alias Multixvers
 */


/*-------------------- FONCTIONS DES BOUTON DE GESTION D'INTERVENTION --------------------*/

function dep(id, etat) {
   alert("ICI A VOIR !!! bouton.js");
    viderModal();
    $.get('/php/requetes/depart_intervention.php?dep=' + id).always(function(arg) {
        if (arg.retour == "ok") {
            updateIntelligent();
            notif("Votre intervention a bien été enregistrer", "success", "", "");
            viderModal();
        }
    });
}

function reco(id, etat) {
    viderModal();
    $.get('/php/requetes/vl_reco_intervention.php?reco=' + id).always(function(arg) {
        if (arg.retour == "ok") {
            updateIntelligent();
            notif("Votre intervention a bien été enregistrer", "success", "", "");
            viderModal();
        }
    });
}

function diff(id, etat) {
    viderModal();
    $.get('/php/requetes/diff_intervention.php?diff=' + id).always(function(arg) {
        if (arg.retour == "ok") {
            updateIntelligent();
            notif("Votre intervention a bien été enregistrer", "success", "", "");
            viderModal();
        }
    });
}

function term(id, etat) {
    viderModal();
    creerModal("cloturer", "CLOTURER UNE INTERVENTION", "valid-cloture", '/php/requetes/cloture.php?id=' + id, "get", 'bg-danger', "");
    updateIntelligent();
}

function annul(id, etat) {
    viderModal();
    creerModal("annuler", "ANNULER UNE INTERVENTION", "valid-annuler", '/php/requetes/annuler.php?id=' + id, "get", 'bg-danger', "");
    updateIntelligent();
}

function modif(id, etat) {
    viderModal();
    var tabEtat = ["UNE CREATION D'INTERVENTION", "UNE INTERVENTION EN COURS", "UN VEHICULE DE RECONNAISSANCE", "UNE INTERVENTION EN DIFEREE"];
    var tabClass = ["primary", "success", "info", "warning"]
    if (etat != 6) {
        var titre = "MODIFIER " + tabEtat[etat];
        creerModalDiff("depart", titre, "feuille_depart", "/php/pages/feuille_depart.php?change=" + id, "get", "bg-" + tabClass[etat], true);
    }
    if (etat == 6) {
        var titre = "MODIFIER UNE INTERVENTION LIEE";
        creerModalDiff("depart", titre, "feuille_depart", "/php/pages/feuille_depart.php?change=" + id, "get", "bg-warning", true);
    }
    updateIntelligent();
}