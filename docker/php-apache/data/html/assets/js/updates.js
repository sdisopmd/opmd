/*
 * Fonction des mises à jours du site
 * Fichier crée le 06/04/2014
 * Auteur : Denis Sanson alias Multixvers
 */

/*-------------------- FONCTIONS DE SURVEILLANCE INTELLIGENTE --------------------*/

function updateIntelligent() {
    setTimeout(function() { update() }, 1500);
}

function update() {
    update_vehicules();
    update_materiels();
    update_autreVehicules();
    //update_menus_vehicules();
    update_personnel();
    update_cartouche();
    //update_compte_rendu();
    update_differees();
    //update_menu();

}

/*-------------------- FONCTIONS DE MISE À JOUR --------------------*/
function update_vehicules() {
    var ouvert = ($("#vehicules .collapse").hasClass('in')) ? "oui" : "non";
    $('#vehicules').load('/php/pages/vehicules.php?ouvert=' + ouvert);
}

function update_materiels() {
    var ouvert = ($("#materiels .collapse").hasClass('in')) ? "oui" : "non";
    $('#materiels').load('/php/pages/materiels.php?ouvert=' + ouvert);
}

function update_autreVehicules() {
    var ouvert = ($("#autre-vehicules .collapse").hasClass('in')) ? "oui" : "non";
    $('#autre-vehicules').load('/php/pages/autre_vehicules.php?ouvert=' + ouvert);
}

function update_personnel() {
    $('#personnel').load('/php/pages/personnel.php');
}

function update_cartouche() {
    var ref = $("#myTabs .active").find('a').attr('href');
    if (ref != undefined) {
        var onglet = ref.replace("#", "");
        $('#cartouche').load('/php/pages/cartouche.php?onglet=' + onglet);
    } else {
        $('#cartouche').load('/php/pages/cartouche.php');
    }
}

function update_differees() {
    $('#differees').load('/php/pages/differees.php');
}

function update_compte_rendu() {
    $('#compte_rendu').load('/php/pages/compte_rendu.php');
}

function update_recap(id) {
    $('#recap').load('/php/pages/recap_intervention.php?id=' + id);
}

function update_menu() {
    modalDepart("/php/pages/feuille_depart.php", "get", "", false)
}