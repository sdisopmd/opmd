/*
 * Fonction pour la feuille de départ des interventions
 * fonctionnalité et validation du formulaire incluse 
 * Fichier crée le 15/04/2014
 * Auteur : Denis Sanson alias Multixvers
 */

// Variable global
var temporisateur = 2000;
var augmentateur = 2000;

/*------------------- GESTIONNAIRE DES BOUTONS --------------------*/

$("#inter-dep").on('click', function(e) {
    e.preventDefault();
    var cible = $(this).attr('id');
    setTimeout(function() { recupInfoForm(cible) }, 1000);
    temporisateur = 1000;
});

$("#vlreco").on('click', function(e) {
    e.preventDefault();
    var cible = $(this).attr('id');
    setTimeout(function() { recupInfoForm(cible) }, 1000);
    temporisateur = 1000;
});

$("#inter-diff").on('click', function(e) {
    e.preventDefault();
    var cible = $(this).attr('id');
    setTimeout(function() { recupInfoForm(cible) }, 1000);
    temporisateur = 1000;
});

$("#inter-term").on('click', function(e) {
    e.preventDefault();
    var cible = $(this).attr('id');
    recupInfoForm(cible);
    temporisateur = 1000;
});

$("#inter-annuler").on('click', function(e) {
    e.preventDefault();
    var cible = $(this).attr('id');
    recupInfoForm(cible);
    temporisateur = 1000;
});


/*------------------- GESTIONNAIRE DES REQUETES --------------------*/

function verifCodis(id) {
    temporisateur = 1000;
    var response = true;
    $.get("/php/requetes/codis.php?id=" + id + '&type=verif').always(function(arg) {
        if (arg.reponse == "erreur") {
            response = false;
            addError("id", "codis", "Vous ne pouvez pas utiliser ce numéro CODIS car il est déjà utilisé", "input");
        } else {
            delError("id", "codis", "input");
            $('input[name=id]').addClass('exist');
        }
        if (fullDebug) { msg(arg) };
    });
    return response;
}

function verifCodisLier(codis) {
    temporisateur = 1000;
    var id = $('input[name=id]').val();
    $.get("/php/requetes/codis.php?codis=" + codis + '&type=lier').always(function(arg) {
        if (arg.reponse == "erreur") {
            var nb = arg.codis.length;
            var txt;
            if (nb > 1) {
                txt = "Les numéros codis suivant sont déjà utilisé : " + arg.codis.join(", ");
            } else {
                txt = "Le numéro codis suivant est déjà utilisé : " + arg.codis.join(", ");
            }
            addError("codis_lier", "codis_lier", txt, "input");
        } else {
            if (codis.indexOf(id) != -1) {
                addError("codis_lier", "codis_lier", "Le numéro codis liée " + id + " est déjà utilisé dans le champ codis ci-dessus", "input");
            } else {
                delError("codis_lier", "codis_lier", "input");
                $('input[name=codis_lier]').addClass('exist');
            }
        }
        if (fullDebug) { msg(arg) };
    });
    return codis;
}

/*-------------------- VALIDATION DU FORMULAIRE --------------------*/

function recupInfoForm(cible) {

    var Depart = new Object();

    Depart.date = $('input[name=date]').val();
    Depart.heure = $('input[name=heure]').val();
    Depart.id = $('input[name=id]').val();
    Depart.codisLier = verifCodisLier($('input[name=codis_lier]').val());
    Depart.nom = $('input[name=nom]').val();
    Depart.prenom = $('input[name=prenom]').val();
    Depart.adresse = $('input[name=adresse]').val();
    Depart.commune = $('select[name=commune]').val();
    Depart.num_rue = $('input[name=num_rue]').val();
    Depart.etage = $('input[name=etage]').val();
    Depart.appt = $('input[name=appt]').val();
    Depart.tel = $('input[name=telephone]').val();
    Depart.compl = $('textarea[name=complement]').val();
    Depart.nature = $('select[name=nature]').val();
    Depart.precision = $('select[name=precision]').val();
    Depart.etat = $('input[name=etat]').attr('data');

    var tabVehicule = $('input[name="vehicules[]"]').each(function() { $(this).val() });
    var vehicules = new Array();
    var i;
    for (i = 0; i < tabVehicule.length; i++) {
        vehicules[i] = $(tabVehicule[i]).val();
    }
    Depart.vehicule = (vehicules.length == 0) ? null : vehicules;

    var tabMateriel = $('input[name="materiels[]"]').each(function() { $(this).val() });
    var tabMaterielDispo = $('input[name="materiels[]"]').each(function() { $(this) });
    var materiels = new Array();
    var materielsDispo = new Array();
    var a;
    for (a = 0; a < tabMateriel.length; a++) {
        materiels[a] = $(tabMateriel[a]).val();
        materielsDispo[a] = $(tabMaterielDispo[a]).attr('data');
    }
    var Materiel = new Object();
    Materiel.id = (materiels.length == 0) ? null : materiels;
    Materiel.utiliser = (materielsDispo.length == 0) ? null : materielsDispo;
    Depart.materiel = Materiel;

    var tabPerso = $('input[name="presencesexternes[]"]').each(function() { $(this).val() });
    var tabPersoNumber = $('input[name="presencesexternes[]"]').each(function() { $(this) });
    var perso = new Array();
    var persoNumber = new Array();
    var b
    for (b = 0; b < tabPerso.length; b++) {
        perso[b] = $(tabPerso[b]).val();
        persoNumber[b] = $(tabPersoNumber[b]).attr('data');
    }
    var Perso = new Object();
    Perso.id = (perso.length == 0) ? null : perso;
    Perso.quantite = (persoNumber.length == 0) ? null : persoNumber;
    Depart.presencesexternes = Perso;

    if (cible == "inter-dep") {
        validForm(Depart);
    } else if (cible == "inter-diff") {
        validFormDiff(Depart);
    } else if (cible == "vlreco") {
        validFormReco(Depart);
    } else if (cible == "inter-term") {
        validFormTerm(Depart);
    } else if (cible == "inter-annuler") {
        validFormAnnuler(Depart);
    }
    if (fullDebug) { msg(Depart) };
}



// partie interventions
function validForm(form) {

    var r = true;

    var datePattern = new RegExp("^[0-9]{2}[/][0-9]{2}[/][0-9]{4}$");
    var formDate = form.date
    if (!datePattern.test(formDate)) {
        formDate = reformDate(formDate);
        if (!datePattern.test(formDate)) {
            r = false;
            addError("date", "date", "Merci d'entrer une date valide", "input");
        } else {
            delError("date", "date", "input");
        }
    } else {
        delError("date", "date", "input");
    }

    if (valid_heure(form.heure) == false) {
        r = false;
        addError("heure", "heure", "Merci d'entrer une heure valide au format HHMM", "input");
    } else {
        delError("heure", "heure", "input");
    }


    if (!isFinite(form.id) || form.id < 1) {
        r = false;
        addError("id", "codis", "Merci d'entrer un numéro codis valide", "input");
    } else {
        delError("id", "codis", "input");
    }


    if (form.adresse == "") {
        r = false;
        addError("adresse", "adresse", "Merci d'entrer une adresse valide", "input");
    } else {
        delError("adresse", "adresse", "input");
    }


    if (form.commune == "0") {
        r = false;
        addError("commune", "commune", "Merci de sélectionner une commune valide", "select");
    } else {
        delError("commune", "commune", "select");
    }


    if (form.cp == "") {
        r = false;
        addError("cp", "cp", "Merci d'entrer un code postal valide", "input");
    } else {
        delError("cp", "cp", "input");
    }

    if (form.nature == undefined) {
        r = false;
        addError("nature", "nature", "Merci de sélectionner une nature valide", "select");
    } else {
        delError("nature", "nature", "select");
    }

    if (form.precision == undefined) {
        r = false;
        addError("precision", "precision", "Merci de sélectionner une précision valide", "select");
    } else {
        delError("precision", "precision", "select");
    }

    if (form.vehicule == "") {
        r = false;
        addError("liste_vehicules", "vehicule", "Merci de choisir au moins un véhicule", "div");
    } else {
        delError("liste_vehicules", "vehicule", "div");
    }

    form.etatUpdate = 1;
    form.differee = 0;

    if (fullDebug) { msg(form) };

    if (r == true) {
        viderModal();
        $.post("/php/interventions/sauvegarde.php", form).always(function(arg) {
            if (fullDebug) { msg(arg) };
            if (arg.retour == "ok") {
                notif("Votre intervention a bien été enregistrée", "success", "", "");
                updateIntelligent();
            } else {
                notif("Une erreur est survenue lors de l'enregistrement celui-ci n'est pas pris en compte", "danger", "", "");
            }

        });
    }
}

// partie reconnaissance
function validFormReco(form) {

    var r = true;

    var datePattern = new RegExp("^[0-9]{2}[/][0-9]{2}[/][0-9]{4}$");
    var formDate = form.date
    if (!datePattern.test(formDate)) {
        formDate = reformDate(formDate);
        if (!datePattern.test(formDate)) {
            r = false;
            addError("date", "date", "Merci d'entrer une date valide", "input");
        } else {
            delError("date", "date", "input");
        }
    } else {
        delError("date", "date", "input");
    }

    if (valid_heure(form.heure) == false) {
        r = false;
        addError("heure", "heure", "Merci d'entrer une heure valide au format HHMM", "input");
    } else {
        delError("heure", "heure", "input");
    }


    if (!isFinite(form.id) || form.id < 1) {
        r = false;
        addError("id", "codis", "Merci d'entrer un numéro codis valide", "input");
    } else {
        delError("id", "codis", "input");
    }


    if (form.adresse == "") {
        r = false;
        addError("adresse", "adresse", "Merci d'entrer une adresse valide", "input");
    } else {
        delError("adresse", "adresse", "input");
    }


    if (form.commune == "0") {
        r = false;
        addError("commune", "commune", "Merci de sélectionner une commune valide", "select");
    } else {
        delError("commune", "commune", "select");
    }


    if (form.cp == "") {
        r = false;
        addError("cp", "cp", "Merci d'entrer un code postal valide", "input");
    } else {
        delError("cp", "cp", "input");
    }

    if (form.nature == undefined) {
        r = false;
        addError("nature", "nature", "Merci de sélectionner une nature valide", "select");
    } else {
        delError("nature", "nature", "select");
    }

    if (form.precision == undefined) {
        r = false;
        addError("precision", "precision", "Merci de sélectionner une précision valide", "select");
    } else {
        delError("precision", "precision", "select");
    }

    if (form.vehicule == "") {
        r = false;
        addError("liste_vehicules", "vehicule", "Merci de choisir au moins un véhicule", "div");
    } else {
        delError("liste_vehicules", "vehicule", "div");
    }

    form.reconnaissance = form.id;
    form.differee = 0;
    form.etatUpdate = 2;

    if (fullDebug) { msg(form) };

    if (r == true) {
        viderModal();
        $.post("/php/interventions/sauvegarde.php", form).always(function(arg) {
            if (fullDebug) { msg(arg) };
            if (arg.retour == "ok") {
                notif("Votre intervention a bien été enregistrée", "success", "", "");
                updateIntelligent();
            } else {
                notif("Une erreur est survenue lors de l'enregistrement celui-ci n'est pas pris en compte", "danger", "", "");
            }

        });
    }
}

// partie intervention différée
function validFormDiff(form) {

    var r = true;

    var datePattern = new RegExp("^[0-9]{2}[/][0-9]{2}[/][0-9]{4}$");
    var formDate = form.date
    if (!datePattern.test(formDate)) {
        formDate = reformDate(formDate);
        if (!datePattern.test(formDate)) {
            r = false;
            addError("date", "date", "Merci d'entrer une date valide", "input");
        } else {
            delError("date", "date", "input");
        }
    } else {
        delError("date", "date", "input");
    }

    if (valid_heure(form.heure) == false) {
        r = false;
        addError("heure", "heure", "Merci d'entrer une heure valide au format HHMM", "input");
    } else {
        delError("heure", "heure", "input");
    }

    if (!isFinite(form.id) || form.id < 1) {
        r = false;
        addError("id", "codis", "Merci d'entrer un numéro codis valide", "input");
    } else {
        delError("id", "codis", "input");
    }

    if (form.adresse == "") {
        r = false;
        addError("adresse", "adresse", "Merci d'entrer une adresse valide", "input");
    } else {
        delError("adresse", "adresse", "input");
    }


    if (form.commune == "0") {
        r = false;
        addError("commune", "commune", "Merci de sélectionner une commune valide", "select");
    } else {
        delError("commune", "commune", "select");
    }


    if (form.cp == "") {
        r = false;
        addError("cp", "cp", "Merci d'entrer un code postal valide", "input");
    } else {
        delError("cp", "cp", "input");
    }


    if (form.nature == undefined) {
        r = false;
        addError("nature", "nature", "Merci de sélectionner une nature valide", "select");
    } else {
        delError("nature", "nature", "select");
    }

    if (form.precision == undefined) {
        r = false;
        addError("precision", "precision", "Merci de sélectionner une précision valide", "select");
    } else {
        delError("precision", "precision", "select");
    }

    form.differee = 1;
    form.etatUpdate = 3;

    if (fullDebug) { msg(form) };

    if (r == true) {
        viderModal();
        $.post("/php/interventions/sauvegarde.php", form).always(function(arg) {
            if (fullDebug) { msg(arg) };
            if (arg.retour == "ok") {
                notif("Votre intervention a bien été enregistrée", "success", "", "");
                updateIntelligent();
            } else {
                notif("Une erreur est survenue lors de l'enregistrement celui-ci n'est pas pris en compte", "danger", "", "");
            }

        });
    }
}

// partie cloture
function validFormTerm(form) {
    viderModal();
    creerModal("cloturer", "CLOTURER UNE INTERVENTION", "valid-cloture", '/php/requetes/cloture.php?id=' + form.id, "get", 'bg-danger', "");
    if (fullDebug) { msg(form) };
}

// partie cloture
function validFormAnnuler(form) {
    viderModal();
    creerModal("annul", "ANNULER UNE INTERVENTION", "valid-annul", '/php/requetes/annuler.php?id=' + form.id, "get", 'bg-danger', "");
    if (fullDebug) { msg(form) };
}

/*--------------- GESTIONNAIRE D'ERREUR DU FORMULAIRE --------------*/

function addError(champ, id, msg, input) {
    if (input == "input") {
        $('input[name=' + champ + ']').parent().addClass('has-error');
    } else if (input == "select") {
        $('select[name=' + champ + ']').parent().addClass('has-error');
    } else if (input == "textarea") {
        $('textarea[name=' + champ + ']').parent().addClass('has-error');
    } else {
        $('#' + champ).addClass('has-error');
    }
    $('#error-' + id + '').text(msg).addClass("text-danger");
    notif(msg, "danger", temporisateur, "");
    temporisateur = temporisateur + augmentateur;
}

function delError(champ, id, input) {
    if (input == "input") {
        $('input[name=' + champ + ']').parent().removeClass('has-error');
    } else if (input == "select") {
        $('select[name=' + champ + ']').parent().removeClass('has-error');
    } else if (input == "textarea") {
        $('textarea[name=' + champ + ']').parent().removeClass('has-error');
    } else {
        $('#' + champ).removeClass('has-error');
    }
    $('#error-' + id + '').text("").removeClass("text-danger");
}