# Application OPMD - OPérations Multilple et Diverses

## L'application et constituée de 2 parties :

	- Administration (pour gérer l'ensemble de l'application)
	- Partie principal après authentification

### Organisation de la structure :

**src :**
- **backup :** Contient les sauvegardes SQL faite manuellement avec le nom de l'utilisateur
- **classes :** Contient toutes les Class Objet POO de l'application
- **config :** Contient tous les fichiers servant à la configuration de l'application
- **session :** Contient les fichiers de session utilisateur

**html :**

- **admin :** Contient un ensemble de sous dossiers permetant de gérer l'administration
- **assets :** Contient les dossiers **css**, **js**, et **img** utilisé par l'application et crée par Multixvers
- **libs :** Contient un ensemble de librairies tierces et aussi quelques une crée par Multixvers
- **php :** Contient un ensemble de sous dossiers permetant d'afficher l'application

**init.php :** Fichier d'initialisation du projet
**index.php :** Point d'entré de l'application soit affichage de la landing page
**humans.txt :** description du créateur
**favicon.ico :** Image pour les onglets des navigateurs
**LICENCE.txt :** Projet sous licence MIT crée par Denis Sanson sous le nom Multixvers
**README.md :** Fichier de description pour le dépôt OPMD (dépôt privé herbeger sur les serveur de Multixvers, pour le versionnage de l'application

### Lancement du projet 

*Prérequis avant d'installer docker sur Windows :* 

**Ouvrez PowerShell en tant qu’administrateur et exécutez :** 

`dism.exe /online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all /norestart`

`dism.exe /online /enable-feature /featurename:VirtualMachinePlatform /all /norestart`

Puis installer le noyau linux via ce lien : https://wslstorestorage.blob.core.windows.net/wslblob/wsl_update_x64.msi

Puis ouvrir Windows Store et dans la barre de rechercher entré : **Debian** puis cliquer sur **installer**

Toujours dans le Windows Store rechercher : **Terminal puis l'installer**

Ouvrir Debian et *attendre* que ça demande un nom d'utilisateur et ensuite un mot de passe

Enfin installer **docker**

_Prérequis avoir **docker** d'installer sur son ordinateur_


### Docker et docker compose

- Ouvrir un terminal à l'emplacement du fichier docker-compose.yml
- ***Exécuté la commande pour démarrer*** :

`docker-compose up -d`

- Puis ouvrir le navigateur est entrer l'adresse : http://localhost
- Login : opmd
- Password : opmd

Pour accéder à PhpMyAdmin 
- Puis ouvrir le navigateur est entrer l'adresse : http://localhost:81
- Login : opmd
- Password : opmd

***Exécuter la commande pour tout arrêter*** :

`docker-compose down -d`

**OU** 

**Utiliser en double cliquant sur :**

_**opmd start  (pour démarrer)**_

_**ompd stop (pour arrêter)**_

